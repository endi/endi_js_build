/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/price_study/components/App.js":
/*!*******************************************!*\
  !*** ./src/price_study/components/App.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Router_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Router.js */ "./src/price_study/components/Router.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../views/RootComponent.js */ "./src/price_study/views/RootComponent.js");
/* harmony import */ var _Controller_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Controller.js */ "./src/price_study/components/Controller.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");






var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().Application.extend({
  channelName: 'app',
  radioEvents: {
    "navigate": 'onNavigate',
    "show:modal": "onShowModal",
    "product:delete": "onModelDelete",
    "discount:delete": "onModelDelete",
    "product:duplicate": "onProductDuplicate"
  },
  region: '#js-main-area',
  onBeforeStart: function onBeforeStart(app, options) {
    console.log("AppClass.onBeforeStart");
    this.rootView = new _views_RootComponent_js__WEBPACK_IMPORTED_MODULE_3__.default();
    this.controller = new _Controller_js__WEBPACK_IMPORTED_MODULE_4__.default({
      rootView: this.rootView
    });
    this.router = new _Router_js__WEBPACK_IMPORTED_MODULE_1__.default({
      controller: this.controller
    });
    console.log("AppClass.onBeforeStart finished");
  },
  onStart: function onStart(app, options) {
    this.showView(this.rootView);
    console.log("Starting the history");
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.hideLoader)();
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.start();
  },
  onNavigate: function onNavigate(route_name, parameters) {
    console.log("App.onNavigate");
    var dest_route = route_name;

    if (!_.isUndefined(parameters)) {
      dest_route += "/" + parameters;
    }

    window.location.hash = dest_route;
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.loadUrl(dest_route);
  },
  onShowModal: function onShowModal(view) {
    this.controller.showModal(view);
  },
  onModelDelete: function onModelDelete(view) {
    this.controller.modelDelete(view);
  },
  onProductDuplicate: function onProductDuplicate(view) {
    this.controller.productDuplicate(view);
  }
});
var App = new AppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (App);

/***/ }),

/***/ "./src/price_study/components/Controller.js":
/*!**************************************************!*\
  !*** ./src/price_study/components/Controller.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _models_ProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/ProductModel.js */ "./src/price_study/models/ProductModel.js");
/* harmony import */ var _models_WorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/WorkModel.js */ "./src/price_study/models/WorkModel.js");
/* harmony import */ var _models_DiscountModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/DiscountModel.js */ "./src/price_study/models/DiscountModel.js");





var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    console.log("Controller.initialize");
    this.rootView = options['rootView'];
    this.productDefaults = this.config.request('get:options', 'defaults');
  },
  index: function index() {
    console.log("Controller.index");
    this.rootView.index();
  },
  showModal: function showModal(view) {
    this.rootView.showModal(view);
  },
  // Product related views
  addProduct: function addProduct() {
    var collection = this.facade.request('get:collection', 'products');
    var defaults = {
      order: collection.getMaxOrder() + 1
    };
    Object.assign(defaults, this.productDefaults);
    var model = new _models_ProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default(defaults);
    this.rootView.showAddProductForm(model, collection);
  },
  editProduct: function editProduct(modelId) {
    var collection = this.facade.request('get:collection', 'products');
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditProductForm.bind(this.rootView));
  },
  productDuplicate: function productDuplicate(childView) {
    console.log("Controller.productDuplicate");
    var request = childView.model.duplicate();
    var this_ = this;
    request.done(function (model) {
      var route;

      if (model.get('type_') == 'price_study_work') {
        route = "/works/";
        this_.rootView.showEditWorkForm(model);
      } else {
        route = "/products";
        this_.rootView.showEditProductForm(model);
      }

      var dest_route = route + model.get('id');
      window.location.hash = dest_route;
    });
  },
  // Work related views
  addWork: function addWork() {
    var collection = this.facade.request('get:collection', 'products');
    var defaults = {
      order: collection.getMaxOrder() + 1
    };
    var model = new _models_WorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default(defaults);
    this.rootView.showAddWorkForm(model, collection);
  },
  editWork: function editWork(modelId) {
    var collection = this.facade.request('get:collection', 'products');
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditWorkForm.bind(this.rootView));
  },
  addDiscount: function addDiscount() {
    var collection = this.facade.request('get:collection', 'discounts');
    var model = new _models_DiscountModel_js__WEBPACK_IMPORTED_MODULE_3__.default({
      order: collection.getMaxOrder() + 1,
      type_: 'amount'
    });
    this.rootView.showAddDiscountForm(model, collection);
  },
  editDiscount: function editDiscount(modelId) {
    var collection = this.facade.request('get:collection', 'discounts');
    var request = collection.fetch();
    request = request.then(function () {
      var model = collection.get(modelId);
      return model;
    });
    request.then(this.rootView.showEditDiscountForm.bind(this.rootView));
  },
  // Common model views
  _onModelDeleteSuccess: function _onModelDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
    this.rootView.index();
  },
  _onModelDeleteError: function _onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  modelDelete: function modelDelete(childView) {
    console.log("Controller.modelDelete");
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cet élément ?");

    if (result) {
      childView.model.destroy({
        success: this._onModelDeleteSuccess.bind(this),
        error: this._onModelDeleteError.bind(this),
        wait: true
      });
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/price_study/components/Facade.js":
/*!**********************************************!*\
  !*** ./src/price_study/components/Facade.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _models_PriceStudyModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/PriceStudyModel.js */ "./src/price_study/models/PriceStudyModel.js");
/* harmony import */ var _models_ProductCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/ProductCollection.js */ "./src/price_study/models/ProductCollection.js");
/* harmony import */ var _models_DiscountCollection_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/DiscountCollection.js */ "./src/price_study/models/DiscountCollection.js");
/* harmony import */ var _base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../base/components/FacadeModelApiMixin.js */ "./src/base/components/FacadeModelApiMixin.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
Global Api, handling all the model and collection fetch

facade = Radio.channel('facade');
facade.request('get:collection', 'sale_products');
*/








var FacadeClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().Object.extend(_base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_6__.default).extend({
  radioEvents: {
    'changed:product': 'onChangedProduct',
    'changed:discount': 'onChangedDiscount'
  },
  radioRequests: {
    'get:model': 'getModelRequest',
    'get:collection': 'getCollectionRequest',
    'load:collection': 'loadCollection',
    'insert:from:catalog': 'insertProductFromCatalog',
    'workitem:from:catalog': 'insertWorkitemFromCatalog',
    'is:valid': "isDatasValid"
  },
  initialize: function initialize(options) {
    this.models = {};
    this.models['price_study'] = new _models_PriceStudyModel_js__WEBPACK_IMPORTED_MODULE_3__.default({});
    this.collections = {};
    var collection;
    collection = new _models_ProductCollection_js__WEBPACK_IMPORTED_MODULE_4__.default();
    this.collections['products'] = collection;
    collection = new _models_DiscountCollection_js__WEBPACK_IMPORTED_MODULE_5__.default();
    this.collections['discounts'] = collection;
  },
  setup: function setup(options) {
    this.setModelUrl('price_study', options['context_url']);
    this.setCollectionUrl('products', options['product_collection_url']);
    this.setCollectionUrl('discounts', options['discount_collection_url']);
  },
  _afterLoadFromCatalog: function _afterLoadFromCatalog(collection, models) {
    console.log("Fetching collection");
    var serverRequest = collection.fetch();
    this.models.price_study.fetch();
    return serverRequest.then(function () {
      console.log('Returnin models');
      console.log(models);
      return models;
    });
  },
  _insertFromCatalog: function _insertFromCatalog(collection, sale_products) {
    var _this = this;

    /*
     Return a Promise resolving the model insertion
    */
    console.log("_insertFromCatalog");
    var sale_product_ids = sale_products.map(function (item) {
      return item.id;
    });

    var url = _.result(collection, 'url'); // Lance la fonction ou récupère l'attribut


    url += "?action=load_from_catalog";
    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.ajax_call)(url, {
      sale_product_ids: sale_product_ids
    }, 'POST');
    return serverRequest.then(function (models) {
      return _this._afterLoadFromCatalog(collection, models);
    });
  },
  insertProductFromCatalog: function insertProductFromCatalog(sale_products) {
    var collection = this.collections['products'];
    console.log(collection);
    console.log(sale_products);
    return this._insertFromCatalog(collection, sale_products);
  },
  insertWorkitemFromCatalog: function insertWorkitemFromCatalog(collection, sale_products) {
    /*
     Return a Promise resolving the model insertion
    */
    console.log(collection);
    console.log(sale_products);
    return this._insertFromCatalog(collection, sale_products);
  },
  start: function start() {
    /*
     * Fires initial One Page application Load
     */
    console.log("Facade.start : Loading current models");
    var modelRequest = this.loadModel('price_study');
    var collectionRequest = this.loadCollection('products');
    var discountColectionRequest = this.loadCollection('discounts');
    return $.when(modelRequest, collectionRequest, discountColectionRequest);
  },
  onChangedProduct: function onChangedProduct() {
    console.log("Product datas chanegd");
    this.models.price_study.fetch();
    this.collections.discounts.fetch();
  },
  onChangedDiscount: function onChangedDiscount() {
    console.log("A discount has been saved");
    this.models.price_study.fetch();
  },
  isDatasValid: function isDatasValid() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    channel.trigger('bind:validation');
    var result = this.collections.products.validateModels();
    channel.trigger('unbind:validation');
    return result;
  }
});
var Facade = new FacadeClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Facade);

/***/ }),

/***/ "./src/price_study/components/Router.js":
/*!**********************************************!*\
  !*** ./src/price_study/components/Router.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");

var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {
    'index': 'index',
    '': 'index',
    'addproduct': 'addProduct',
    'products/:id': 'editProduct',
    'addwork': "addWork",
    "works/:id": "editWork",
    'adddiscount': "addDiscount",
    "discounts/:id": "editDiscount"
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/price_study/components/ToolbarApp.js":
/*!**************************************************!*\
  !*** ./src/price_study/components/ToolbarApp.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _common_components_ValidationLimitToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/components/ValidationLimitToolbarAppClass */ "./src/common/components/ValidationLimitToolbarAppClass.js");
/* harmony import */ var _views_PriceStudyResume__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../views/PriceStudyResume */ "./src/price_study/views/PriceStudyResume.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);



var ToolbarAppClass = _common_components_ValidationLimitToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  getResumeView: function getResumeView(actions) {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    var model = facade.request('get:model', 'price_study');
    return new _views_PriceStudyResume__WEBPACK_IMPORTED_MODULE_1__.default({
      model: model
    });
  }
});
var ToolbarApp = new ToolbarAppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ToolbarApp);

/***/ }),

/***/ "./src/price_study/components/UserPreferences.js":
/*!*******************************************************!*\
  !*** ./src/price_study/components/UserPreferences.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/components/ConfigBus */ "./src/base/components/ConfigBus.js");
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math */ "./src/math.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");



var UserPreferencesClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().Object.extend({
  channelName: 'user_preferences',
  radioRequests: {
    'formatAmount': 'formatAmount'
  },
  setFormConfig: function setFormConfig(form_config) {
    console.log("Setting up User preferences");
    console.log(form_config);
    this.decimal_to_display = form_config.options.decimal_to_display;
    console.log(this.decimal_to_display);
  },
  formatAmount: function formatAmount(amount, rounded, strip) {
    var precision;

    if (!rounded) {
      precision = this.decimal_to_display;
    }

    return (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(amount, true, strip, precision);
  },
  setup: function setup(form_config_url) {
    console.log("UserPreferencesClass.setup");
  },
  start: function start() {
    console.log("UserPreferencesClass.start");
    this.setFormConfig(_base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__.default.form_config);
    var result = $.Deferred();
    result.resolve(_base_components_ConfigBus__WEBPACK_IMPORTED_MODULE_0__.default.form_config, null, null);
    return result;
  }
});
var UserPreferences = new UserPreferencesClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (UserPreferences);

/***/ }),

/***/ "./src/price_study/models/DiscountCollection.js":
/*!******************************************************!*\
  !*** ./src/price_study/models/DiscountCollection.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountModel.js */ "./src/price_study/models/DiscountModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }






var DiscountCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_4__.default.extend({
  amount_related_props: ['amount', 'percentage', 'tva_id'],
  model: _DiscountModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize(options) {
    this.on('saved', this.channelCall);
    this.on('remove', this.onDeleteCall);
  },
  onDeleteCall: function onDeleteCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    channel.trigger('changed:discount');
  },
  channelCall: function channelCall(key_or_attributes) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    var changed_keys;

    if (_typeof(key_or_attributes) == 'object') {
      changed_keys = [key_or_attributes];
    } else {
      changed_keys = _.keys(key_or_attributes);
    }

    var fire = Boolean(_.intersection(this.amount_related_props, key_or_attributes));

    if (fire) {
      channel.trigger('changed:discount');
    }
  },
  validate: function validate() {
    var result = {};
    this.each(function (model) {
      var res = model.validate();

      if (res) {
        _.extend(result, res);
      }
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollection);

/***/ }),

/***/ "./src/price_study/models/DiscountModel.js":
/*!*************************************************!*\
  !*** ./src/price_study/models/DiscountModel.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");


var DiscountModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  props: ['id', 'type_', 'amount', 'tva_id', 'description', 'total_ht', 'total_tva', 'total_ttc', 'order', 'percentage'],
  validation: {
    description: {
      required: true,
      msg: "Remise : Veuillez saisir un objet"
    },
    value: {
      required: function required(value, attr, computedState) {
        if (this.get('type_') == 'amount') {
          return true;
        }
      },
      pattern: "amount",
      msg: "Remise : Veuillez saisir un montant"
    },
    percentage: {
      required: function required(value, attr, computedState) {
        if (this.get('type_') == 'percentage') {
          return true;
        }
      },
      range: [1, 99],
      msg: "Veuillez saisir un pourcentage"
    },
    tva_id: {
      required: true,
      pattern: "number",
      msg: "Remise : Veuillez sélectionner une TVA"
    }
  },
  initialize: function initialize() {
    _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_1__.default.__super__.initialize.apply(this, arguments);

    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  is_percentage: function is_percentage() {
    return this.get('type_') == 'percentage';
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountModel);

/***/ }),

/***/ "./src/price_study/models/PriceStudyModel.js":
/*!***************************************************!*\
  !*** ./src/price_study/models/PriceStudyModel.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  PriceStudyModel
 */



var PriceStudyModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  props: ["id", "product_name", "label", "notes", "total_ht", "total_ttc", "tva_parts", "total_ht_before_discount"],
  validation: {
    label: {
      required: true,
      msg: "Ce champ est obligatoire"
    }
  },
  initialize: function initialize() {
    _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_prefs');
  },
  tva_labels: function tva_labels() {
    var values = [];
    var this_ = this;
    var tva_options = this.config.request('get:options', 'tvas');

    _.each(this.get('tva_parts'), function (item, tva_id) {
      var tva = _.findWhere(tva_options, {
        id: parseInt(tva_id)
      });

      values.push({
        'value': (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(item),
        label: tva.label
      });
    });

    return values;
  },
  ht_label: function ht_label() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ht'));
  },
  ht_before_discounts_label: function ht_before_discounts_label() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ht_before_discount'));
  },
  ttc_label: function ttc_label() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.get('total_ttc'));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyModel);

/***/ }),

/***/ "./src/price_study/models/ProductCollection.js":
/*!*****************************************************!*\
  !*** ./src/price_study/models/ProductCollection.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductModel.js */ "./src/price_study/models/ProductModel.js");
/* harmony import */ var _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkModel.js */ "./src/price_study/models/WorkModel.js");
/* harmony import */ var _tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools */ "./src/tools.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_4__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name : ProductCollection.js
 */





var ProductCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: function model(modeldict, options) {
    if (modeldict.type_ == 'price_study_work') {
      return new _WorkModel_js__WEBPACK_IMPORTED_MODULE_2__.default(modeldict, options);
    } else {
      return new _ProductModel_js__WEBPACK_IMPORTED_MODULE_1__.default(modeldict, options);
    }
  },
  initialize: function initialize(options) {
    ProductCollection.__super__.initialize.apply(this, options);

    this.on('saved', this.channelCall);
    this.on('remove', this.channelCall);
  },
  channelCall: function channelCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('facade');
    channel.trigger('changed:product');
  },
  HT: function HT() {
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  TVAParts: function TVAParts() {
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.tva_label();

      if (tva in result) {
        tva_amount += result[tva];
      }

      result[tva] = tva_amount;
    });
    return result;
  },
  TTC: function TTC() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  validateModels: function validateModels() {
    var result = {};

    if (this.models.length == 0) {
      result['products'] = "Étude: veuillez saisir au moins une prestation";
    }

    this.each(function (model) {
      var res = model.validateModel();

      if (res) {
        _.extend(result, res);
      }
    });
    return result;
  },
  load_from_catalog: function load_from_catalog(sale_product_ids) {
    var serverRequest = (0,_tools__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url + '?action=load_from_catalog', {
      sale_product_ids: sale_product_ids
    }, 'POST');
    serverRequest.then(this.fetch.bind(this));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCollection);

/***/ }),

/***/ "./src/price_study/models/ProductModel.js":
/*!************************************************!*\
  !*** ./src/price_study/models/ProductModel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name : ProductModel.js
 *
 */




var ProductModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend(_base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  props: ['id', 'type_', 'label', 'description', 'supplier_ht', 'general_overhead', 'margin_rate', "quantity", 'ht', 'total_ht', 'unity', 'tva_id', 'product_id', "base_sale_product_id", "order", 'uptodate'],
  validation: {
    'description': {
      required: true,
      msg: "Veuillez saisir une description"
    },
    ht: {
      required: false,
      pattern: "amount",
      msg: "Veuillez saisir un coût unitaire, dans la limite de 5 chiffres après la virgule"
    },
    supplier_ht: {
      required: false,
      pattern: "amount",
      msg: "Veuillez saisir un coût d'achat, dans la limite de 5 chiffres après la virgule"
    },
    general_overhead: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de frais généraux doit être un nombre, dans la limite de 5 chiffres après la virgule"
    }, {
      range: [0, 9],
      msg: "Doit être compris entre 0 et 9"
    }],
    margin_rate: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de marge doit être un nombre, dans la limite de 5 chiffres après la virgule"
    }, {
      range: [0, 9],
      msg: "Doit être compris entre 0 et 9"
    }],
    tva_id: {
      required: true,
      msg: 'Requis'
    },
    product_id: {
      required: true,
      msg: 'Requis'
    }
  },
  defaults: function defaults() {
    console.log("In Product model defaults");
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var form_defaults = config.request('get:options', 'defaults');
    return {
      type_: 'price_study_product',
      quantity: 1
    };
  },
  initialize: function initialize() {
    ProductModel.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.tva_options = this.config.request('get:options', 'tvas');
  },
  product_label: function product_label() {
    var product_options = this.config.request('get:options', 'products');
    return this.findLabelFromId('product_id', 'label', product_options);
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  validateModel: function validateModel() {
    return this.validate();
  },
  supplier_ht_mode: function supplier_ht_mode() {
    if (_.isNull(this.get('supplier_ht')) || !this.has('supplier_ht') || this.get('supplier_ht') == 0) {
      return false;
    } else {
      return true;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductModel);

/***/ }),

/***/ "./src/price_study/models/WorkItemCollection.js":
/*!******************************************************!*\
  !*** ./src/price_study/models/WorkItemCollection.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/OrderableCollection.js */ "./src/base/models/OrderableCollection.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemModel.js */ "./src/price_study/models/WorkItemModel.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * File Name :  WorkItemCollection
 */




var WorkItemCollection = _base_models_OrderableCollection_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  model: _WorkItemModel_js__WEBPACK_IMPORTED_MODULE_2__.default,
  initialize: function initialize(options) {
    WorkItemCollection.__super__.initialize.apply(this, options);
  },
  ht: function ht() {
    /* Sum the HT amounts for this collection */
    var result = 0;
    this.each(function (model) {
      result += model.ht();
    });
    return result;
  },
  tvaParts: function tvaParts() {
    /*
     * Collect tva amounts by tva  label
     */
    var result = {};
    this.each(function (model) {
      var tva_amount = model.tva();
      var tva = model.tva_label();

      if (tva != '-') {
        if (tva in result) {
          tva_amount += result[tva];
        }

        result[tva] = tva_amount;
      }
    });
    return result;
  },
  ttc: function ttc() {
    var result = 0;
    this.each(function (model) {
      result += model.ttc();
    });
    return result;
  },
  validate: function validate() {
    var result = {};
    this.each(function (model) {
      var res = model.validate();

      if (res) {
        result['items'] = "Des modèles sont incomplets, veuillez vérifier votre saisie";
      }
    });
    return result;
  },
  syncAll: function syncAll(models) {
    var _$;

    console.log("WorkItemCollection Syncing all models");
    var promises = [];
    this.each(function (model) {
      promises.push(model.fetch());
    });

    var resulting_deferred = (_$ = $).when.apply(_$, promises);

    return resulting_deferred;
  },
  refreshAll: function refreshAll(models) {
    return this.syncAll(models);
  },
  load_from_catalog: function load_from_catalog(sale_product_ids) {
    var serverRequest = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url + '?action=load_from_catalog', {
      sale_product_ids: sale_product_ids
    }, 'POST');
    serverRequest.then(this.fetch.bind(this));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollection);

/***/ }),

/***/ "./src/price_study/models/WorkItemModel.js":
/*!*************************************************!*\
  !*** ./src/price_study/models/WorkItemModel.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  WorkItemModel
 */




var WorkItemModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  props: ["id", "type_", "label", "description", "ht", "supplier_ht", "work_unit_quantity", "total_quantity", 'quantity_inherited', "unity", "work_unit_ht", "total_ht", "tva_id", "tva_id_editable", "product_id", "product_id_editable", "general_overhead", "general_overhead_editable", "margin_rate", "margin_rate_editable", 'base_sale_product_id', "sync_catalog", 'uptodate'],
  inherited_props: ['general_overhead', 'margin_rate', 'tva_id', 'product_id'],
  defaults: function defaults() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var defaults = config.request('get:options', 'defaults');
    return {
      work_unit_quantity: 1,
      quantity_inherited: true,
      general_overhead_editable: true,
      margin_rate_editable: true
    };
  },
  validation: {
    label: {
      required: true,
      msg: "Veuillez saisir un nom"
    },
    description: {
      required: true,
      msg: "Veuillez saisir une description"
    },
    tva_id: {
      required: true,
      msg: "Veuillez choisir le taux de Tva à appliquer"
    },
    product_id: {
      required: true,
      msg: "Veuillez choisir le compte produit"
    },
    work_unit_quantity: {
      required: function required(value, attr, computedState) {
        if (!this.get('locked')) {
          return true;
        }
      }
    },
    unity: {
      required: true,
      msg: "Veuillez saisir une unité"
    },
    general_overhead: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de frais généraux doit être un nombre, dans la limite de 5 chiffres après la virgule"
    }, {
      range: [0, 9],
      msg: "Doit être compris entre 0 et 9"
    }],
    margin_rate: [{
      required: false,
      pattern: 'amount',
      msg: "Le coefficient de marge doit être un nombre, dans la limite de 5 chiffres après la virgule"
    }, {
      range: [0, 9],
      msg: "Doit être compris entre 0 et 9"
    }]
  },
  initialize: function initialize() {
    _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.__super__.initialize.apply(this, arguments);

    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('user_preferences');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
  },
  tva_label: function tva_label() {
    return this.findLabelFromId('tva_id', 'label', this.tva_options);
  },
  product_label: function product_label() {
    return this.findLabelFromId('product_id', 'label', this.product_options);
  },
  loadFromCatalog: function loadFromCatalog(model) {
    var datas = model.toJSON();
    datas['base_sale_product_id'] = datas['id'];
    datas['locked'] = false;
    delete datas['id'];
    delete datas['type_'];
    console.log("ProductModel.loadFromCatalog");
    this.set(datas);
  },
  ht: function ht() {
    /* Return the ht value of this entry */
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.strToFloat)(this.get('total_ht'));
  },
  tva: function tva() {
    /*
     * Compute tva value and return a dict {tva_id: tva_amount}
     */
    var tva_value = this.findLabelFromId('tva_id', 'value', this.tva_options);
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.getTvaPart)(this.ht(), tva_value);
  },
  ttc: function ttc() {
    /* Return the ttc value of this entry */
    return this.ht() + this.tva();
  },
  isFromParent: function isFromParent(attribute) {
    /*
    * Check if the given attribute's value comes from the parent ProductWork
    */
    var result;

    if (!this.inherited_props.includes(attribute)) {
      result = false;
    } else if (!this.get(attribute + '_editable')) {
      result = true;
    } else {
      result = false;
    }

    return result;
  },
  supplier_ht_mode: function supplier_ht_mode() {
    if (_.isNull(this.get('supplier_ht')) || !this.has('supplier_ht') || this.get('supplier_ht') == 0) {
      return false;
    } else {
      return true;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemModel);

/***/ }),

/***/ "./src/price_study/models/WorkModel.js":
/*!*********************************************!*\
  !*** ./src/price_study/models/WorkModel.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WorkItemCollection.js */ "./src/price_study/models/WorkItemCollection.js");
/* harmony import */ var _ProductModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductModel.js */ "./src/price_study/models/ProductModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/*
 * File Name :  WorkModel
 */



var WorkModel = _ProductModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  /* props spécifique à ce modèle */
  props: ['title', 'items', 'sale_product_work_id', 'flat_cost', 'display_details'],
  validation: {
    items: function items(value) {
      if (this.has('items')) {
        if (value.length === 0) {
          return "Produit compos\xE9 ".concat(this.get('title'), " : Veuillez saisir au moins un produit");
        }
      }
    },
    title: {
      required: true,
      msg: "Veuillez saisir un titre"
    }
  },
  defaults: function defaults() {
    console.log("In work model defaults");
    return {
      quantity: 1,
      type_: "price_study_work"
    };
  },
  initialize: function initialize() {
    console.log("WorkItem.initialize");

    WorkModel.__super__.initialize.apply(this, arguments);

    this.populate();
    console.log("WorkItem.initialized");
  },
  setupSyncEvents: function setupSyncEvents() {
    this.stopListening(this.items);
    this.listenTo(this.items, 'saved', this.syncWithItems);
    this.listenTo(this.items, 'destroyed', this.syncWithItems);
  },
  populate: function populate() {
    console.log("WorkModel.populate");

    if (this.has('items')) {
      this.items = new _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_1__.default(this.get('items'));
    } else {
      this.items = new _WorkItemCollection_js__WEBPACK_IMPORTED_MODULE_1__.default([]);
    }

    this.items._parent = this;
    this.items.stopListening(this);
    this.items.listenTo(this, 'saved', this.items.syncAll.bind(this.items));
    var this_ = this;

    this.items.url = function () {
      return this_.url() + '/' + "work_items";
    };

    this.items.syncAll();
  },
  syncWithItems: function syncWithItems() {
    console.log("WorkModel.syncWithItems");
    this.set('items', this.items.toJSON());
    this.fetch();
  },
  supplier_ht_label: function supplier_ht_label() {
    return (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)((0,_math_js__WEBPACK_IMPORTED_MODULE_0__.round)(this.get('flat_cost'), 5));
  },
  validateModel: function validateModel() {
    var result = this.validate();
    var label = this.get('title');
    var item_validation = this.items.validate();

    _.each(item_validation, function (item, index) {
      item_validation[index] = label + item;
    });

    _.extend(result, item_validation);

    return result;
  }
});
/*
 * On complète les 'props' du ProductModel avec celle du WorkModel
 */

WorkModel.prototype.props = WorkModel.prototype.props.concat(_ProductModel_js__WEBPACK_IMPORTED_MODULE_2__.default.prototype.props);
Object.assign(WorkModel.prototype.validation, _ProductModel_js__WEBPACK_IMPORTED_MODULE_2__.default.prototype.validation);
delete WorkModel.prototype.validation.product_id;
delete WorkModel.prototype.validation.tva_id;
delete WorkModel.prototype.validation.description;
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkModel);

/***/ }),

/***/ "./src/price_study/price_study.js":
/*!****************************************!*\
  !*** ./src/price_study/price_study.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var _components_App_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/App.js */ "./src/price_study/components/App.js");
/* harmony import */ var _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Facade.js */ "./src/price_study/components/Facade.js");
/* harmony import */ var _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/ToolbarApp.js */ "./src/price_study/components/ToolbarApp.js");
/* harmony import */ var _components_UserPreferences_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/UserPreferences.js */ "./src/price_study/components/UserPreferences.js");
/* global AppOption; */







jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  console.log(_components_Facade_js__WEBPACK_IMPORTED_MODULE_4__.default.radioRequests);
  (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__.applicationStartup)(AppOption, _components_App_js__WEBPACK_IMPORTED_MODULE_3__.default, _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__.default, _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_5__.default, _components_UserPreferences_js__WEBPACK_IMPORTED_MODULE_6__.default);
});

/***/ }),

/***/ "./src/price_study/views/PriceStudyComponent.js":
/*!******************************************************!*\
  !*** ./src/price_study/views/PriceStudyComponent.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _PriceStudyView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceStudyView.js */ "./src/price_study/views/PriceStudyView.js");
/* harmony import */ var _ProductComponent_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductComponent.js */ "./src/price_study/views/ProductComponent.js");
/* harmony import */ var _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./discount/DiscountComponent.js */ "./src/price_study/views/discount/DiscountComponent.js");
/* harmony import */ var _TaskListComponent_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./TaskListComponent.js */ "./src/price_study/views/TaskListComponent.js");
/* harmony import */ var _base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/*
 * Module name : PriceStudyComponent
 */








var template = __webpack_require__(/*! ./templates/PriceStudyComponent.mustache */ "./src/price_study/views/templates/PriceStudyComponent.mustache");

var PriceStudyComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  template: template,
  regions: {
    toolbar: {
      el: '.toolbar',
      replaceElement: true
    },
    tasks: '.tasks',
    common: '.common',
    products: '.products',
    discounts: '.discounts',
    resume: '.resume',
    errors: '.errors'
  },
  ui: {
    anchors: 'a'
  },
  events: {
    'click @ui.anchors': 'onAnchorClicked'
  },
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.tasks = this.config.request('get:options', 'tasks');
  },
  onRender: function onRender() {
    if (this.tasks.length > 0) {
      this.showChildView('tasks', new _TaskListComponent_js__WEBPACK_IMPORTED_MODULE_4__.default({
        tasks: this.tasks
      }));
    }

    this.showChildView('common', new _PriceStudyView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: this.model
    }));
    var collection = this.facade.request("get:collection", 'products');
    this.showChildView('products', new _ProductComponent_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: collection
    }));
    this.showDiscounts();
  },
  showDiscounts: function showDiscounts() {
    var collection = this.facade.request("get:collection", 'discounts');
    this.showChildView('discounts', new _discount_DiscountComponent_js__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: collection
    }));
  },
  templateContext: function templateContext() {
    var editable = this.config.request('get:options', 'editable');
    return {
      editable: editable,
      has_tasks: this.tasks.length > 0
    };
  },
  formOk: function formOk() {
    var result = true;
    var errors = this.facade.request('is:valid');

    if (!_.isEmpty(errors)) {
      this.showChildView('errors', new _base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_5__.default({
        errors: errors
      }));
      result = false;
    } else {
      this.detachChildView('errors');
    }

    return result;
  },
  onAnchorClicked: function onAnchorClicked(event) {
    console.log("onAnchorClicked");
    var button = $(event.target);
    console.log(button);

    if (button.hasClass('genestimation') || button.hasClass('geninvoice')) {
      return this.formOk();
    }

    return true;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyComponent);

/***/ }),

/***/ "./src/price_study/views/PriceStudyResume.js":
/*!***************************************************!*\
  !*** ./src/price_study/views/PriceStudyResume.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : PriceStudyResume
 */



var template = __webpack_require__(/*! ./templates/PriceStudyResume.mustache */ "./src/price_study/views/templates/PriceStudyResume.mustache");

var PriceStudyResume = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'table',
  className: 'summary',
  template: template,
  modelEvents: {
    'sync': 'render'
  },
  templateContext: function templateContext() {
    return {
      show_discounts: this.model.get('total_ht_before_discount') != this.model.get('total_ht'),
      ht_before_discount_label: this.model.ht_before_discounts_label(),
      ht_label: this.model.ht_label(),
      tva_labels: this.model.tva_labels(),
      ttc_label: this.model.ttc_label()
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyResume);

/***/ }),

/***/ "./src/price_study/views/PriceStudyView.js":
/*!*************************************************!*\
  !*** ./src/price_study/views/PriceStudyView.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/*
 * Module name : PriceStudyView
 */






var template = __webpack_require__(/*! ./templates/PriceStudyView.mustache */ "./src/price_study/views/templates/PriceStudyView.mustache");

var PriceStudyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  className: 'form-section',
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  template: template,
  regions: {
    name: '.field-name',
    notes: '.field-notes'
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:persist'
  },
  onRender: function onRender() {
    this.showChildView('name', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      field_name: 'name',
      label: "Nom de l'étude",
      value: this.model.get('name')
    }));
    this.showChildView('notes', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'notes',
      label: "Notes",
      value: this.model.get('notes')
    }));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PriceStudyView);

/***/ }),

/***/ "./src/price_study/views/ProductCollectionView.js":
/*!********************************************************!*\
  !*** ./src/price_study/views/ProductCollectionView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductView.js */ "./src/price_study/views/ProductView.js");
/* harmony import */ var _ProductEmptyView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductEmptyView.js */ "./src/price_study/views/ProductEmptyView.js");
/* harmony import */ var _WorkView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./WorkView.js */ "./src/price_study/views/WorkView.js");
/*
 * Module name : ProductCollectionView
 */





var ProductCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().CollectionView.extend({
  emptyView: _ProductEmptyView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  collectionEvents: {
    'change:reorder': 'render',
    sync: 'render'
  },
  childView: function childView(model) {
    if (model.get('type_') == 'price_study_work') {
      return _WorkView_js__WEBPACK_IMPORTED_MODULE_3__.default;
    } else {
      return _ProductView_js__WEBPACK_IMPORTED_MODULE_1__.default;
    }
  },
  // Bubble up child view events
  childViewTriggers: {
    'model:edit': 'product:edit',
    'model:delete': 'product:delete',
    'model:duplicate': 'product:duplicate',
    'model:down': 'product:down',
    'model:up': 'product:up'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductCollectionView);

/***/ }),

/***/ "./src/price_study/views/ProductComponent.js":
/*!***************************************************!*\
  !*** ./src/price_study/views/ProductComponent.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ProductCollectionView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductCollectionView.js */ "./src/price_study/views/ProductCollectionView.js");
/*
 * Module name : ProductComponent
 */




var template = __webpack_require__(/*! ./templates/ProductComponent.mustache */ "./src/price_study/views/templates/ProductComponent.mustache");

var ProductComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  className: '',
  regions: {
    collection: '.collection'
  },
  // Listen to child view events
  childViewEvents: {
    "product:up": "onModelOrderUp",
    "product:down": "onModelOrderDown",
    "product:edit": "onProductEdit",
    'product:delete': "onProductDelete",
    "product:duplicate": "onProductDuplicate"
  },
  initialize: function initialize() {
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  onRender: function onRender() {
    this.showChildView('collection', new _ProductCollectionView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      collection: this.collection
    }));
  },
  onModelOrderUp: function onModelOrderUp(childView) {
    console.log("Moving up the element");
    this.collection.moveUp(childView.model);
  },
  onModelOrderDown: function onModelOrderDown(childView) {
    this.collection.moveDown(childView.model);
  },
  onProductEdit: function onProductEdit(childView) {
    var route = '/products/';

    if (childView.model.get('type_') == 'price_study_work') {
      route = '/works/';
    }

    this.app.trigger('navigate', route + childView.model.get('id'));
  },
  onProductDuplicate: function onProductDuplicate(childView) {
    console.log("Triggering App.product:duplicate");
    this.app.trigger('product:duplicate', childView);
  },
  onProductDelete: function onProductDelete(childView) {
    this.app.trigger('product:delete', childView);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductComponent);

/***/ }),

/***/ "./src/price_study/views/ProductEmptyView.js":
/*!***************************************************!*\
  !*** ./src/price_study/views/ProductEmptyView.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : ProductEmptyView
 */



var template = __webpack_require__(/*! ./templates/ProductEmptyView.mustache */ "./src/price_study/views/templates/ProductEmptyView.mustache");

var ProductEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductEmptyView);

/***/ }),

/***/ "./src/price_study/views/ProductForm.js":
/*!**********************************************!*\
  !*** ./src/price_study/views/ProductForm.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/*
 * Module name : ProductForm
 */









var template = __webpack_require__(/*! ./templates/ProductForm.mustache */ "./src/price_study/views/templates/ProductForm.mustache");

var ProductForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend(_base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_4__.default).extend({
  partial: false,
  template: template,
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_5__.default],
  regions: {
    'order': '.field-order',
    'description': '.field-description',
    'supplier_ht': '.field-supplier_ht',
    'general_overhead': '.field-general_overhead',
    'margin_rate': '.field-margin_rate',
    'ht': '.field-ht',
    'quantity': '.field-quantity',
    'unity': '.field-unity',
    'tva_id': '.field-tva_id',
    'product_id': '.field-product_id',
    'catalogContainer': '#catalog-container'
  },
  ui: {
    main_tab: "ul.nav-tabs li:first a"
  },
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': 'onCatalogInsert'
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'cancel:click': 'cancel:click'
  },
  modelEvents: {
    'set:product': 'refreshForm',
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.workunit_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  getCommonFieldOptions: function getCommonFieldOptions(attribute, label) {
    var result = {
      field_name: attribute,
      value: this.model.get(attribute),
      label: label,
      editable: true
    };
    return result;
  },
  refreshForm: function refreshForm() {
    this.showOrder();
    this.showDescription();
    this.showSupplierHt();
    this.showGeneralOverhead();
    this.showMarginRate();
    this.showHt();
    this.showQuantity();
    this.showUnity();
    this.showTva();
    this.showProduct();

    if (!this.getOption('edit')) {
      this.getUI('main_tab').tab('show');
    }
  },
  showOrder: function showOrder() {
    this.showChildView('order', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      value: this.model.get('order'),
      field_name: 'order',
      type: 'hidden'
    }));
  },
  showDescription: function showDescription() {
    var options = this.getCommonFieldOptions('description', "Description");
    options.description = "Description utilisée dans les devis/factures";
    options.tinymce = true;
    options.required = true;
    options.cid = this.model.cid;
    options.required = true;
    var view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
    this.showChildView('description', view);
  },
  showSupplierHt: function showSupplierHt() {
    var options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('supplier_ht', view);
  },
  showGeneralOverhead: function showGeneralOverhead() {
    var options = this.getCommonFieldOptions('general_overhead', 'Coefficient de frais généraux');
    options.description = "Utilisé pour calculer le 'Prix de revient' depuis le coût d'achat ou 'Déboursé sec' selon la formule 'coût d'achat * (1 + Coefficient de frais généraux)'";
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('general_overhead', view);
  },
  showMarginRate: function showMarginRate() {
    var options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
    options.description = "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'";
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('margin_rate', view);
  },
  showHt: function showHt() {
    var options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
    var supplier_value = this.model.get('supplier_ht');

    if (Boolean(supplier_value)) {
      options.description = "Calculé depuis le coût unitaire";
      options.editable = false;
    }

    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('ht', view);
  },
  showQuantity: function showQuantity() {
    var options = this.getCommonFieldOptions('quantity', "Quantité");
    options.required = true;
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('quantity', view);
  },
  showUnity: function showUnity() {
    var options = this.getCommonFieldOptions('unity', "Unité");
    options.options = this.workunit_options;
    options.placeholder = 'Choisir une unité';
    options.id_key = 'value';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView("unity", view);
  },
  showTva: function showTva() {
    // NB : ici les tva_options sont différentes de celles utilisées dans le reset de la view
    // car elles sont modifiées par le SelectWidget et on perd donc l'info de la
    // tva par défaut
    var tva_options = this.config.request('get:options', 'tvas');
    var options = this.getCommonFieldOptions('tva_id', 'TVA');
    options.id_key = 'id';
    options.options = tva_options;
    options.required = true;
    options.placeholder = 'Choisir un taux de TVA';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('tva_id', view);
  },
  showProduct: function showProduct() {
    var options = this.getCommonFieldOptions('product_id', 'Compte produit');
    this.product_options = this.getProductOptions(this.tva_options, this.all_product_options, this.model.get('tva_id'));
    options.required = true;
    options.options = this.product_options;
    options.id_key = 'id';
    options.placeholder = 'Choisir un compte produit';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('product_id', view);
  },
  templateContext: function templateContext() {
    var title = "Ajouter un produit";

    if (this.getOption('edit')) {
      title = "Modifier un produit";
    }

    return {
      add: !this.getOption('edit'),
      title: title
    };
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      this.showChildView('catalogContainer', new _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_6__.default({
        query_params: {
          type_: 'product'
        },
        url: AppOption['catalog_tree_url'],
        multiple: true
      }));
    }
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    console.log("ProductForm.onCatalogInsert");
    var req = this.facade.request('insert:from:catalog', sale_products);
    console.log(req);
    req.then(function () {
      return _this.triggerMethod('modal:close');
    });
  },
  onDestroyModal: function onDestroyModal() {
    console.log("Modal close");
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    app.trigger('navigate', 'index');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductForm);

/***/ }),

/***/ "./src/price_study/views/ProductView.js":
/*!**********************************************!*\
  !*** ./src/price_study/views/ProductView.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _base_models_ButtonCollection_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/models/ButtonCollection.js */ "./src/base/models/ButtonCollection.js");
/* harmony import */ var _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/ActionButtonsWidget.js */ "./src/widgets/ActionButtonsWidget.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/*
 * Module name : ProductView
 */








var template = __webpack_require__(/*! ./templates/ProductView.mustache */ "./src/price_study/views/templates/ProductView.mustache");

var ProductView = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  tagName: 'div',
  className: 'product border_left_block content_double_padding quotation_item',
  template: template,
  regions: {
    actions: "td.col_actions"
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.listenTo(this.facade, 'bind:validation', this.bindValidation);
    this.listenTo(this.facade, 'unbind:validation', this.unbindValidation);
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(this);
  },
  showActions: function showActions() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    var collection = new _base_models_ButtonCollection_js__WEBPACK_IMPORTED_MODULE_3__.default();
    var buttons = [{
      label: "Dupliquer",
      action: "duplicate",
      icon: "copy"
    }, {
      label: "Supprimer",
      action: "delete",
      icon: "trash-alt",
      css: 'negative'
    }];

    if (order != min_order) {
      buttons.push({
        label: "Remonter",
        action: "up",
        icon: "arrow-up"
      });
    }

    if (order != max_order) {
      buttons.push({
        label: "Descendre",
        action: "down",
        icon: "arrow-down"
      });
    }

    var primary = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
      label: "Modifier / Voir le détail",
      action: "edit",
      icon: "pen",
      showLabel: false
    });
    collection.add(buttons);
    var view = new _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      collection: collection,
      dropdownLabel: 'Actions',
      icon: "dots",
      showLabel: false,
      primary: primary
    });
    this.showChildView('actions', view);
  },
  onRender: function onRender() {
    var editable = this.config.request('get:options', "editable");

    if (editable) {
      this.showActions();
    }
  },
  templateContext: function templateContext() {
    var ht_label = this.user_prefs.request('formatAmount', this.model.get('ht'), false);
    var ht_full_label = (0,_math_js__WEBPACK_IMPORTED_MODULE_5__.formatAmount)(this.model.get('ht'));
    return {
      supplier_ht_mode: this.model.supplier_ht_mode(),
      supplier_ht_label: this.user_prefs.request('formatAmount', this.model.get('supplier_ht'), false),
      ht_label: ht_label,
      ht_full_label: ht_full_label,
      ht_rounded: ht_label != ht_full_label,
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false),
      tva_label: this.model.tva_label(),
      product_label: this.model.product_label()
    };
  },
  onActionClicked: function onActionClicked(action_name) {
    console.log("onActionClicked %s", action_name);
    this.triggerMethod('model:' + action_name, this);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductView);

/***/ }),

/***/ "./src/price_study/views/RootComponent.js":
/*!************************************************!*\
  !*** ./src/price_study/views/RootComponent.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _discount_DiscountForm_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./discount/DiscountForm.js */ "./src/price_study/views/discount/DiscountForm.js");
/* harmony import */ var _ProductForm_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProductForm.js */ "./src/price_study/views/ProductForm.js");
/* harmony import */ var _PriceStudyComponent_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PriceStudyComponent.js */ "./src/price_study/views/PriceStudyComponent.js");
/* harmony import */ var _workform_AddWorkForm_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./workform/AddWorkForm.js */ "./src/price_study/views/workform/AddWorkForm.js");
/* harmony import */ var _workform_WorkForm_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./workform/WorkForm.js */ "./src/price_study/views/workform/WorkForm.js");
/*
 * Module name : RootComponent
 */








var template = __webpack_require__(/*! ./templates/RootComponent.mustache */ "./src/price_study/views/templates/RootComponent.mustache");

var RootComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  className: '',
  template: template,
  regions: {
    main: '.main',
    modalContainer: '.modal-container'
  },
  childViewEvents: {
    'product:delete': "onModelDelete",
    'product:duplicate': 'onProductDuplicate'
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.initialized = false;
  },
  index: function index() {
    this.initialized = true;
    var model = this.facade.request('get:model', 'price_study');
    var view = new _PriceStudyComponent_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: model
    });
    this.showChildView('main', view);
  },
  showModal: function showModal(view) {
    this.showChildView('modalContainer', view);
  },
  _showProductForm: function _showProductForm(model, collection, edit) {
    this.index();
    var view = new _ProductForm_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model,
      destCollection: collection,
      edit: edit
    });
    this.showModal(view);
  },
  showAddProductForm: function showAddProductForm(model, collection) {
    this._showProductForm(model, collection, false);
  },
  showEditProductForm: function showEditProductForm(model) {
    this._showProductForm(model, model.collection, true);
  },
  _showDiscountForm: function _showDiscountForm(model, collection, edit) {
    this.index();
    var view = new _discount_DiscountForm_js__WEBPACK_IMPORTED_MODULE_1__.default({
      model: model,
      destCollection: collection,
      edit: edit
    });
    this.showModal(view);
  },
  showAddDiscountForm: function showAddDiscountForm(model, collection) {
    this._showDiscountForm(model, collection, false);
  },
  showEditDiscountForm: function showEditDiscountForm(model) {
    this._showDiscountForm(model, model.collection, true);
  },
  showAddWorkForm: function showAddWorkForm(model, collection) {
    this.index();
    var view = new _workform_AddWorkForm_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: model,
      destCollection: collection
    });
    this.showModal(view);
  },
  showEditWorkForm: function showEditWorkForm(model) {
    var view = new _workform_WorkForm_js__WEBPACK_IMPORTED_MODULE_5__.default({
      model: model
    });
    this.showChildView('main', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RootComponent);

/***/ }),

/***/ "./src/price_study/views/TaskListComponent.js":
/*!****************************************************!*\
  !*** ./src/price_study/views/TaskListComponent.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : TaskListComponent
 */



var template = __webpack_require__(/*! ./templates/TaskListComponent.mustache */ "./src/price_study/views/templates/TaskListComponent.mustache");

var TaskListComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template,
  regions: {},
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {},
  templateContext: function templateContext() {
    var tasks = this.getOption('tasks');
    var editable = this.config.request('get:options', 'editable');
    return {
      tasks: tasks,
      edit: editable
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TaskListComponent);

/***/ }),

/***/ "./src/price_study/views/WorkItemCollectionView.js":
/*!*********************************************************!*\
  !*** ./src/price_study/views/WorkItemCollectionView.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _WorkItemView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./WorkItemView.js */ "./src/price_study/views/WorkItemView.js");
/* harmony import */ var _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemEmptyView.js */ "./src/price_study/views/WorkItemEmptyView.js");
/*
 * Module name : WorkItemCollectionView
 */





var template = __webpack_require__(/*! ./templates/WorkItemCollectionView.mustache */ "./src/price_study/views/templates/WorkItemCollectionView.mustache");

var WorkItemCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  template: template,
  childView: _WorkItemView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  emptyView: _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  childViewContainer: "tbody",
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {},
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {},
  templateContext: function templateContext() {
    return {};
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollectionView);

/***/ }),

/***/ "./src/price_study/views/WorkItemEmptyView.js":
/*!****************************************************!*\
  !*** ./src/price_study/views/WorkItemEmptyView.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : WorkItemEmptyView
 */



var template = __webpack_require__(/*! ./templates/WorkItemEmptyView.mustache */ "./src/price_study/views/templates/WorkItemEmptyView.mustache");

var WorkItemEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template,
  tagName: "tr"
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemEmptyView);

/***/ }),

/***/ "./src/price_study/views/WorkItemView.js":
/*!***********************************************!*\
  !*** ./src/price_study/views/WorkItemView.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math */ "./src/math.js");
/*
 * Module name : WorkItemView
 */




var template = __webpack_require__(/*! ./templates/WorkItemView.mustache */ "./src/price_study/views/templates/WorkItemView.mustache");

var WorkItemView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  tagName: "tr",
  template: template,
  modelEvents: {
    'sync': 'render'
  },
  initialize: function initialize() {
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
  },
  templateContext: function templateContext() {
    console.log("WorkItemView Calling the templating context");
    var ht_label = this.user_prefs.request('formatAmount', this.model.get('ht'), false);
    var ht_full_label = (0,_math__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht'));
    return {
      tva_label: this.model.tva_label(),
      ht_label: ht_label,
      ht_full_label: ht_full_label,
      ht_rounded: ht_label != ht_full_label,
      supplier_ht_label: this.user_prefs.request('formatAmount', this.model.get('supplier_ht'), false),
      supplier_ht_mode: this.model.supplier_ht_mode(),
      product_label: this.model.product_label(),
      work_unit_ht_label: this.user_prefs.request('formatAmount', this.model.get('work_unit_ht'), false),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false)
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemView);

/***/ }),

/***/ "./src/price_study/views/WorkView.js":
/*!*******************************************!*\
  !*** ./src/price_study/views/WorkView.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone-validation */ "./node_modules/backbone-validation/dist/backbone-validation-amd.js");
/* harmony import */ var backbone_validation__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_validation__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemCollectionView.js */ "./src/price_study/views/WorkItemCollectionView.js");
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _base_models_ButtonCollection_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../base/models/ButtonCollection.js */ "./src/base/models/ButtonCollection.js");
/* harmony import */ var _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../widgets/ActionButtonsWidget.js */ "./src/widgets/ActionButtonsWidget.js");
/*
 * Module name : WorkView
 */








var template = __webpack_require__(/*! ./templates/WorkView.mustache */ "./src/price_study/views/templates/WorkView.mustache");

var WorkView = backbone_marionette__WEBPACK_IMPORTED_MODULE_6___default().View.extend({
  tagName: 'div',
  className: 'product border_left_block content_double_padding quotation_item',
  template: template,
  regions: {
    work_items: '.items_container',
    actions: ".actions"
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.listenTo(this.facade, 'bind:validation', this.bindValidation);
    this.listenTo(this.facade, 'unbind:validation', this.unbindValidation);
    this.listenTo(this.model, 'validated:invalid', this.showErrors);
    this.listenTo(this.model, 'validated:valid', this.hideErrors.bind(this));
  },
  showErrors: function showErrors(model, errors) {
    this.$el.addClass('error');
  },
  hideErrors: function hideErrors(model) {
    this.$el.removeClass('error');
  },
  bindValidation: function bindValidation() {
    console.log("Binding validation");
    console.log(this.model);
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().bind(this);
  },
  unbindValidation: function unbindValidation() {
    backbone_validation__WEBPACK_IMPORTED_MODULE_1___default().unbind(this);
  },
  showActions: function showActions() {
    var min_order = this.model.collection.getMinOrder();
    var max_order = this.model.collection.getMaxOrder();
    var order = this.model.get('order');
    var collection = new _base_models_ButtonCollection_js__WEBPACK_IMPORTED_MODULE_4__.default();
    var buttons = [{
      label: "Dupliquer",
      action: "duplicate",
      icon: "copy"
    }, {
      label: "Supprimer",
      action: "delete",
      icon: "trash-alt",
      css: 'negative'
    }];
    var primary = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_3__.default({
      label: "Modifier / Voir le détail",
      action: "edit",
      icon: "pen",
      showLabel: false
    });

    if (order != min_order) {
      buttons.push({
        label: "Remonter",
        action: "up",
        icon: "arrow-up"
      });
    }

    if (order != max_order) {
      buttons.push({
        label: "Descendre",
        action: "down",
        icon: "arrow-down"
      });
    }

    collection.add(buttons);
    var view = new _widgets_ActionButtonsWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      collection: collection,
      dropdownLabel: 'Actions',
      icon: "dots",
      showLabel: false,
      primary: primary
    });
    this.showChildView('actions', view);
  },
  onRender: function onRender() {
    console.log("Showing the workItems");
    console.log(this.model.items);
    this.showChildView('work_items', new _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.model.items
    }));
    var editable = this.config.request('get:options', 'editable');

    if (editable) {
      this.showActions();
    }
  },
  templateContext: function templateContext() {
    return {
      ht_label: this.user_prefs.request('formatAmount', this.model.get('ht'), false),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false),
      tva_label: this.model.tva_label(),
      product_label: this.model.product_label(),
      general_overhead: this.model.get('general_overhead') || '-',
      margin_rate: this.model.get('margin_rate') || '-',
      unity: this.model.get('unity') || '-'
    };
  },
  onActionClicked: function onActionClicked(action_name) {
    console.log("onActionClicked %s", action_name);
    this.triggerMethod('model:' + action_name, this);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkView);

/***/ }),

/***/ "./src/price_study/views/discount/DiscountCollectionView.js":
/*!******************************************************************!*\
  !*** ./src/price_study/views/discount/DiscountCollectionView.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountView.js */ "./src/price_study/views/discount/DiscountView.js");
/* harmony import */ var _DiscountEmptyView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./DiscountEmptyView.js */ "./src/price_study/views/discount/DiscountEmptyView.js");
/*
 * Module name : DiscountCollectionView
 */





var template = __webpack_require__(/*! ./templates/DiscountCollectionView.mustache */ "./src/price_study/views/discount/templates/DiscountCollectionView.mustache");

var DiscountCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  //template: template,
  tagName: 'tbody',
  childView: _DiscountView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  emptyView: _DiscountEmptyView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  collectionEvents: {
    'change:reorder': 'render',
    sync: 'render'
  },
  childViewTriggers: {
    'edit': 'edit',
    'delete': 'delete',
    'down': 'down',
    'up': 'up'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountCollectionView);

/***/ }),

/***/ "./src/price_study/views/discount/DiscountComponent.js":
/*!*************************************************************!*\
  !*** ./src/price_study/views/discount/DiscountComponent.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DiscountCollectionView.js */ "./src/price_study/views/discount/DiscountCollectionView.js");
/*
 * Module name : DiscountComponent
 */




var template = __webpack_require__(/*! ./templates/DiscountComponent.mustache */ "./src/price_study/views/discount/templates/DiscountComponent.mustache");

var DiscountComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: template,
  regions: {
    collection: {
      el: 'tbody',
      replaceElement: true
    },
    addButton: '.add',
    popin: '.popin'
  },
  // Listen to child view events
  childViewEvents: {
    "up": "onUp",
    "down": "onDown",
    "edit": "onEdit",
    'delete': "onDelete",
    'destroy:modal': 'render',
    'cancel:form': 'render',
    'action:clicked': "onActionClicked"
  },
  initialize: function initialize() {
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  onRender: function onRender() {
    this.showChildView('collection', new _DiscountCollectionView_js__WEBPACK_IMPORTED_MODULE_1__.default({
      collection: this.collection
    }));
  },
  onUp: function onUp(childView) {
    console.log("Moving up the element");
    this.collection.moveUp(childView.model);
  },
  onDown: function onDown(childView) {
    this.collection.moveDown(childView.model);
  },
  onEdit: function onEdit(childView) {
    var route = '/discounts/';
    this.app.trigger('navigate', route + childView.model.get('id'));
  },
  onDelete: function onDelete(childView) {
    this.app.trigger('discount:delete', childView);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountComponent);

/***/ }),

/***/ "./src/price_study/views/discount/DiscountEmptyView.js":
/*!*************************************************************!*\
  !*** ./src/price_study/views/discount/DiscountEmptyView.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/*
 * Module name : DiscountEmptyView
 */



var template = __webpack_require__(/*! ./templates/DiscountEmptyView.mustache */ "./src/price_study/views/discount/templates/DiscountEmptyView.mustache");

var DiscountEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  template: template
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountEmptyView);

/***/ }),

/***/ "./src/price_study/views/discount/DiscountForm.js":
/*!********************************************************!*\
  !*** ./src/price_study/views/discount/DiscountForm.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/*
 * Module name : DiscountForm
 */







var template = __webpack_require__(/*! ./templates/DiscountForm.mustache */ "./src/price_study/views/discount/templates/DiscountForm.mustache");

var DiscountForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  // Send the whole model on submit
  partial: false,
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_4__.default],
  template: template,
  regions: {
    'order': '.field-order',
    'type_': '.field-type_',
    'description': '.field-description',
    'amount': '.field-amount',
    'percentage': '.field-percentage',
    'tva_id': '.field-tva_id'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'finish': 'onDatasFinished'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    var tva_options = config.request('get:options', 'tvas');
    var price_study_model = facade.request('get:model', 'price_study');
    var tva_parts = Object.keys(price_study_model.get('tva_parts')).map(function (x) {
      return parseInt(x);
    });
    this.tva_options = tva_options.filter(function (tva) {
      return tva_parts.indexOf(tva.id) != -1;
    });
  },
  onRender: function onRender() {
    this.refreshFormFields();
  },
  refreshFormFields: function refreshFormFields() {
    this.emptyRegions();
    this.showTypeField();
    this.showOrderField();
    this.showDescriptionField();

    if (this.model.get('type_') == 'percentage') {
      this.showPercentageField();
    } else {
      this.showAmountField();
      this.showTvaField();
    }
  },
  getCommonFieldOptions: function getCommonFieldOptions(label, key, description) {
    var result = {
      field_name: key,
      label: label,
      description: description || '',
      value: this.model.get(key)
    };
    return result;
  },
  showOrderField: function showOrderField() {
    var options = this.getCommonFieldOptions('', 'order');
    options['type'] = 'hidden';
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('order', view);
  },
  showTypeField: function showTypeField() {
    var options = this.getCommonFieldOptions('Type de remise', 'type_');
    options['options'] = [{
      'id': 'amount',
      'label': 'Montant fixe'
    }, {
      'id': 'percentage',
      'label': 'Pourcentage (valeur calculée)'
    }];
    options['id_key'] = 'id';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('type_', view);
  },
  showDescriptionField: function showDescriptionField() {
    var options = this.getCommonFieldOptions('Description', 'description', 'Visible dans le document final');
    options['tinymce'] = true;
    options['cid'] = this.model.cid;
    var view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(options);
    this.showChildView('description', view);
  },
  showPercentageField: function showPercentageField() {
    var options = this.getCommonFieldOptions('Pourcentage', 'percentage', 'Le montant HT et les taux de TVA seront calculés dynamiquement depuis les valeurs des prestations');
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('percentage', view);
  },
  showAmountField: function showAmountField() {
    var options = this.getCommonFieldOptions('Montant', 'amount');
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_1__.default(options);
    this.showChildView('amount', view);
  },
  showTvaField: function showTvaField() {
    var options = this.getCommonFieldOptions('TVA', 'tva_id');
    options['options'] = this.tva_options;
    options['id_key'] = 'id';
    options['label_key'] = 'name';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default(options);
    this.showChildView('tva_id', view);
  },
  templateContext: function templateContext() {
    var title;

    if (this.getOption('edit')) {
      title = "Modifier cette remise";
    } else {
      title = "Ajouter une remise";
    }

    return {
      title: title
    };
  },
  onDatasFinished: function onDatasFinished(key, value) {
    if (key == 'type_') {
      this.model.set(key, value);
      this.refreshFormFields();
    }
  },
  onDestroyModal: function onDestroyModal() {
    console.log("Modal close");
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    app.trigger('navigate', 'index');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountForm);

/***/ }),

/***/ "./src/price_study/views/discount/DiscountView.js":
/*!********************************************************!*\
  !*** ./src/price_study/views/discount/DiscountView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/*
 * Module name : DiscountView
 */





var template = __webpack_require__(/*! ./templates/DiscountView.mustache */ "./src/price_study/views/discount/templates/DiscountView.mustache");

var DiscountView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: template,
  tagName: 'tr',
  regions: {
    editButtonContainer: {
      el: '.col_actions .edit',
      replaceElement: true
    },
    delButtonContainer: {
      el: '.col_actions .delete',
      replaceElement: true
    }
  },
  // Listen to child view events
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  modelEvents: {
    'sync': 'render'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
  },
  onRender: function onRender() {
    var editModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Modifier cet élément',
      icon: 'pen',
      showLabel: false,
      action: 'edit'
    });
    var deleteModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Supprimer cet élément',
      icon: 'trash-alt',
      showLabel: false,
      action: 'delete'
    });
    this.showChildView('editButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: editModel
    }));
    this.showChildView('delButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: deleteModel
    }));
  },
  templateContext: function templateContext() {
    return {
      tva_label: this.model.tva_label(),
      is_percentage: this.model.is_percentage(),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false)
    };
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'edit') {
      this.triggerMethod('edit', this);
    } else if (action == 'delete') {
      this.triggerMethod('delete', this);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DiscountView);

/***/ }),

/***/ "./src/price_study/views/workform/AddWorkForm.js":
/*!*******************************************************!*\
  !*** ./src/price_study/views/workform/AddWorkForm.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/*
 * Module name : AddWorkForm
 */







var template = __webpack_require__(/*! ./templates/AddWorkForm.mustache */ "./src/price_study/views/workform/templates/AddWorkForm.mustache");

var AddWorkForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  template: template,
  partial: true,
  className: 'main_content',
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  regions: {
    title: '.field-title',
    description: '.field-description',
    catalogContainer: '#catalog-container'
  },
  ui: {
    main_tab: "ul.nav-tabs li:first a"
  },
  // Listen to the current's view events
  events: {},
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': "onCatalogInsert"
  },
  // Bubble up child view events
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified',
    'cancel:click': 'cancel:click'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  refreshForm: function refreshForm() {
    console.log(this.model);
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      field_name: 'title',
      label: "Titre du produit composé",
      value: this.model.get('title'),
      required: true
    });
    this.showChildView('title', view);
    view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      value: this.model.get('description'),
      title: "Description",
      field_name: "description",
      tinymce: true,
      cid: this.model.cid
    });
    this.showChildView('description', view);
  },
  onRender: function onRender() {
    this.refreshForm();
    this.showChildView('catalogContainer', new _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_4__.default({
      query_params: {
        type_: 'work'
      },
      url: AppOption['catalog_tree_url'],
      multiple: true
    }));
  },
  onAttach: function onAttach() {
    this.getUI('main_tab').tab('show');
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    console.log("AddWorkForm.onCatalogInsert");
    var req = this.facade.request('insert:from:catalog', sale_products);
    req.then(function () {
      return _this.triggerMethod('modal:close');
    });
  },
  onSuccessSync: function onSuccessSync() {
    this.app.trigger('navigate', 'works/' + this.model.get('id'));
  },
  onDestroyModal: function onDestroyModal() {
    console.log("Modal close");
    var app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    this.app.trigger('navigate', 'index');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AddWorkForm);

/***/ }),

/***/ "./src/price_study/views/workform/ProductResume.js":
/*!*********************************************************!*\
  !*** ./src/price_study/views/workform/ProductResume.js ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../math.js */ "./src/math.js");
/*
 * Module name : ProductResume
 */



var ProductResume = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  template: __webpack_require__(/*! ./templates/ProductResume.mustache */ "./src/price_study/views/workform/templates/ProductResume.mustache"),
  modelEvents: {
    'sync': 'render'
  },
  initialize: function initialize() {
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
  },
  templateContext: function templateContext() {
    return {
      supplier_ht_label: this.user_prefs.request('formatAmount', (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.round)(this.model.get('flat_cost')), false, false, 5),
      ht_label: this.user_prefs.request('formatAmount', this.model.get('ht'), false),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false)
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ProductResume);

/***/ }),

/***/ "./src/price_study/views/workform/WorkForm.js":
/*!****************************************************!*\
  !*** ./src/price_study/views/workform/WorkForm.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ActionButtonCollection_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/models/ActionButtonCollection.js */ "./src/base/models/ActionButtonCollection.js");
/* harmony import */ var _widgets_ButtonCollectionWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/ButtonCollectionWidget.js */ "./src/widgets/ButtonCollectionWidget.js");
/* harmony import */ var _base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _WorkItemComponent_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./WorkItemComponent.js */ "./src/price_study/views/workform/WorkItemComponent.js");
/* harmony import */ var _ProductResume_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ProductResume.js */ "./src/price_study/views/workform/ProductResume.js");
/* harmony import */ var _base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../../base/views/ErrorView.js */ "./src/base/views/ErrorView.js");
/*
 * Module name : WorkForm
 */














var template = __webpack_require__(/*! ./templates/WorkForm.mustache */ "./src/price_study/views/workform/templates/WorkForm.mustache");

var WorkForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_12___default().View.extend(_base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  template: template,
  partial: true,
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_4__.default],
  regions: {
    resume: '.resume',
    messageContainer: '.message-container',
    errors: '.errors',
    title: ".field-title",
    description: '.field-description',
    quantity: '.field-quantity',
    total_ht: '.field-total_ht',
    unity: '.field-unity',
    tva_id: '.field-tva_id',
    product_id: '.field-product_id',
    margin_rate: '.field-margin_rate',
    general_overhead: '.field-general_overhead',
    display_details: '.field-display_details',
    items: '.items-container',
    other_buttons: {
      el: '.other_buttons',
      replaceElement: true
    }
  },
  ui: {
    'btn_submit': 'button[type=submit]'
  },
  // Listen to child view events
  childViewEvents: {
    'action:clicked': 'onActionClicked',
    'finish': 'onDatasChanged'
  },
  // Bubble up child view events
  childViewTriggers: {
    'finish': 'data:persist',
    'change': 'data:modified'
  },
  events: {
    'data:invalid': 'onDataInvalid'
  },
  modelEvents: {
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
    this.unity_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products'); // On active les évènements de synchro item/work

    this.model.setupSyncEvents();
  },
  showMessageView: function showMessageView() {
    var model = new Bb.Model();
    var view = new MessageView({
      model: model
    });
    this.showChildView('messageContainer', view);
  },
  showCommonFields: function showCommonFields() {
    this.showChildView('title', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      label: "Titre du produit composé",
      description: "Titre du produit composé dans le document final",
      field_name: "title",
      value: this.model.get('title'),
      required: true
    }));
    this.showChildView('description', new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_8__.default({
      title: "Description",
      field_name: 'description',
      value: this.model.get('description'),
      tinymce: true
    }));
    this.showChildView('quantity', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "Quantité",
      field_name: "quantity",
      value: this.model.get('quantity')
    }));
    this.showChildView('display_details', new _widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_6__.default({
      title: "Afficher le détail des prestations dans le document final",
      description: "En décochant cette case, le produit composé apparaîtra comme une seule ligne de prestation, sans le détail des produits ci-dessous",
      field_name: "display_details",
      value: this.model.get('display_details')
    }));
    this.showChildView('items', new _WorkItemComponent_js__WEBPACK_IMPORTED_MODULE_9__.default({
      collection: this.model.items
    }));
    this.showChildView('unity', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Unité",
      field_name: 'unity',
      options: this.unity_options,
      value: this.model.get('unity'),
      placeholder: 'Choisir une unité'
    }));
    this.showChildView('tva_id', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "TVA",
      field_name: 'tva_id',
      options: this.tva_options,
      id_key: 'id',
      value: this.model.get('tva_id'),
      placeholder: 'Choisir un taux de TVA'
    }));
    this.product_options = this.getProductOptions(this.tva_options, this.all_product_options);
    this.showChildView('product_id', new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_7__.default({
      title: "Compte produit",
      field_name: 'product_id',
      options: this.product_options,
      id_key: 'id',
      value: this.model.get('product_id'),
      placeholder: 'Choisir un compte produit'
    }));
    this.showChildView('general_overhead', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "Coefficient de frais généraux",
      field_name: 'general_overhead',
      value: this.model.get('general_overhead'),
      description: "Utilisé pour calculer le 'Prix de revient' depuis le coût d'achat ou 'Déboursé sec' selon la formule 'coût d'achat * (1 + Coefficient de frais généraux)'"
    }));
    this.showChildView('margin_rate', new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
      title: "Coefficient de marge",
      field_name: 'margin_rate',
      value: this.model.get('margin_rate'),
      description: "Utilisé pour calculer le 'Prix intermédiaire' depuis le 'Prix de revient' selon la formule 'prix de revient / (1 - Coefficient marge)'"
    }));
  },
  showOtherActionButtons: function showOtherActionButtons() {
    var collection = new _base_models_ActionButtonCollection_js__WEBPACK_IMPORTED_MODULE_1__.default();
    var buttons = [{
      label: 'Dupliquer',
      action: 'duplicate',
      icon: 'copy',
      showLabel: false
    }, {
      label: "Supprimer",
      action: "delete",
      icon: "trash-alt",
      showLabel: false,
      css: 'negative'
    }];
    collection.add(buttons);
    var view = new _widgets_ButtonCollectionWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: collection
    });
    this.showChildView('other_buttons', view);
  },
  onRender: function onRender() {
    this.showCommonFields();
    this.showChildView('resume', new _ProductResume_js__WEBPACK_IMPORTED_MODULE_10__.default({
      model: this.model
    }));
    this.showOtherActionButtons();
  },
  onCancelForm: function onCancelForm() {
    console.log("We cancel the form in WorkForm!!!");
    this.app.trigger('navigate', 'index');
  },
  templateContext: function templateContext() {
    return {};
  },
  onDataInvalid: function onDataInvalid(model, errors) {
    console.log("WorkForm.onInvalid");
    this.showChildView('errors', new _base_views_ErrorView_js__WEBPACK_IMPORTED_MODULE_11__.default({
      errors: errors
    }));
  },
  onActionClicked: function onActionClicked(actionName) {
    console.log("Action clicked : %s", actionName);
    this.app.trigger("product:" + actionName, this);
  },
  onFormSubmittedFull: function onFormSubmittedFull() {
    console.log("success sync in WorkForm!!!");
    this.app.trigger('navigate', 'index');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkForm);

/***/ }),

/***/ "./src/price_study/views/workform/WorkItemCollectionView.js":
/*!******************************************************************!*\
  !*** ./src/price_study/views/workform/WorkItemCollectionView.js ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../WorkItemEmptyView.js */ "./src/price_study/views/WorkItemEmptyView.js");
/* harmony import */ var _WorkItemView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./WorkItemView.js */ "./src/price_study/views/workform/WorkItemView.js");
/*
 * Module name : WorkItemCollectionView
 */




var WorkItemCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  emptyView: _WorkItemEmptyView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  tagName: 'tbody',
  childView: _WorkItemView_js__WEBPACK_IMPORTED_MODULE_2__.default,
  childViewTriggers: {
    'model:edit': 'model:edit',
    'model:delete': 'model:delete'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemCollectionView);

/***/ }),

/***/ "./src/price_study/views/workform/WorkItemComponent.js":
/*!*************************************************************!*\
  !*** ./src/price_study/views/workform/WorkItemComponent.js ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../tools.js */ "./src/tools.js");
/* harmony import */ var _models_WorkItemModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../models/WorkItemModel.js */ "./src/price_study/models/WorkItemModel.js");
/* harmony import */ var _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./WorkItemCollectionView.js */ "./src/price_study/views/workform/WorkItemCollectionView.js");
/* harmony import */ var _WorkItemForm_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./WorkItemForm.js */ "./src/price_study/views/workform/WorkItemForm.js");
/*
 * Module name : WorkItemComponent
 */









var template = __webpack_require__(/*! ./templates/WorkItemComponent.mustache */ "./src/price_study/views/workform/templates/WorkItemComponent.mustache");

var WorkItemComponent = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  tagName: "fieldset",
  className: "separate_block border_left_block",
  template: template,
  regions: {
    list: {
      el: 'tbody',
      replaceElement: true
    },
    addButton: '.add',
    popin: '.popin'
  },
  ui: {},
  // Listen to the current's view events
  events: {},
  // Listen to collection events
  collectionEvents: {},
  childViewEventPrefix: 'work:item',
  // Listen to child view events
  childViewEvents: {
    'action:clicked': "onActionClicked",
    'cancel:form': 'render',
    'destroy:modal': 'render',
    'model:edit': 'onModelEdit',
    'model:delete': 'onModelDelete',
    'work:item:model:delete': 'onModelDelete'
  },
  // Bubble up child view events
  childViewTriggers: {},
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.app = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('app');
  },
  showAddButton: function showAddButton() {
    var model = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      label: "Ajouter un produit",
      icon: "plus",
      action: "add"
    });
    var view = new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      model: model
    });
    this.showChildView('addButton', view);
  },
  showModelForm: function showModelForm(model, edit) {
    var view = new _WorkItemForm_js__WEBPACK_IMPORTED_MODULE_6__.default({
      model: model,
      edit: edit,
      destCollection: this.collection
    });
    this.app.trigger('show:modal', view);
  },
  onRender: function onRender() {
    var view = new _WorkItemCollectionView_js__WEBPACK_IMPORTED_MODULE_5__.default({
      collection: this.collection
    });
    this.showChildView('list', view);
    this.showAddButton();
  },
  templateContext: function templateContext() {
    return {};
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'add') {
      var model = new _models_WorkItemModel_js__WEBPACK_IMPORTED_MODULE_4__.default();
      this.showModelForm(model, false);
    }
  },
  onModelDeleteSuccess: function onModelDeleteSuccess() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('success', this, "Vos données ont bien été supprimées");
  },
  onModelDeleteError: function onModelDeleteError() {
    var messagebus = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('message');
    messagebus.trigger('error', this, "Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onModelDelete: function onModelDelete(model, childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce produit ?");

    if (result) {
      childView.model.destroy({
        success: this.onModelDeleteSuccess.bind(this),
        error: this.onModelDeleteError.bind(this),
        wait: true
      });
    }
  },
  onModelEdit: function onModelEdit(model, childView) {
    this.showModelForm(model, true);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemComponent);

/***/ }),

/***/ "./src/price_study/views/workform/WorkItemForm.js":
/*!********************************************************!*\
  !*** ./src/price_study/views/workform/WorkItemForm.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../tools.js */ "./src/tools.js");
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../widgets/TextAreaWidget.js */ "./src/widgets/TextAreaWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../base/views/TvaProductFormMixin.js */ "./src/base/views/TvaProductFormMixin.js");
/* harmony import */ var _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../../common/views/CatalogComponent.js */ "./src/common/views/CatalogComponent.js");
/*
 * Module name : WorkItemForm
 */













var template = __webpack_require__(/*! ./templates/WorkItemForm.mustache */ "./src/price_study/views/workform/templates/WorkItemForm.mustache");

var WorkItemForm = backbone_marionette__WEBPACK_IMPORTED_MODULE_11___default().View.extend(_base_views_TvaProductFormMixin_js__WEBPACK_IMPORTED_MODULE_9__.default).extend({
  template: template,
  tagName: 'section',
  id: 'workitem_form',
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_2__.default],
  partial: false,
  regions: {
    erros: ".errors",
    quantity_inherited: ".field-quantity_inherited",
    description: '.field-description',
    supplier_ht: '.field-supplier_ht',
    general_overhead: '.field-general_overhead',
    margin_rate: '.field-margin_rate',
    ht: '.field-ht',
    work_unit_quantity: '.field-work_unit_quantity',

    /*
            total_quantity: '.field-total_quantity',
    */
    unity: '.field-unity',
    tva_id: '.field-tva_id',
    product_id: '.field-product_id',
    catalogContainer: "#catalog-container"
  },
  ui: {
    main_tab: "ul.nav-tabs li:first a"
  },
  // event_prefix: "work:item",
  // Listen to the current's view events
  events: {
    'change': 'updateTotalHt'
  },
  // Listen to child view events
  childViewEvents: {
    'catalog:insert': "onCatalogInsert",
    'action:clicked': "onUnlockClicked",
    'finish': 'onDatasChanged'
  },
  // Bubble up child view events
  childViewTriggers: {
    'finish': 'data:modified'
  },
  modelEvents: {
    'change:tva_id': 'refreshTvaProductSelect'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.unity_options = this.config.request('get:options', 'workunits');
    this.tva_options = this.config.request('get:options', 'tvas');
    this.product_options = this.config.request('get:options', 'products');
    this.all_product_options = this.config.request('get:options', 'products');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
  },
  getCommonFieldOptions: function getCommonFieldOptions(attribute, label) {
    var result = {
      field_name: attribute,
      value: this.model.get(attribute),
      label: label,
      editable: true
    };

    if (this.model.isFromParent(attribute)) {
      result.label += " hérité du produit composé";
      result.editable = false;
    }

    return result;
  },
  refreshForm: function refreshForm() {
    this.showToggleLockButton();
    this.showDescription();
    this.showSupplierHt();
    this.showGeneralOverhead();
    this.showMarginRate();
    this.showHt();
    this.showWorkQuantity();
    this.showUnity();
    this.showTva();
    this.showProduct();
  },
  showToggleLockButton: function showToggleLockButton() {
    var datas = {};

    if (this.model.get('quantity_inherited')) {
      datas.label = "Modifier librement";
      datas.title = "Modifier les quantités indépendament du produit composé";
      datas.icon = 'lock';
    } else {
      datas.label = "Saisir une quantité unitaire";
      datas.title = "Les quantités seront dépendantes de la configuration du produit composé";
      datas.icon = "lock-open";
    }

    this.showChildView('quantity_inherited', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      model: new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_3__.default(datas)
    }));
  },
  showDescription: function showDescription() {
    var options = this.getCommonFieldOptions('description', "Description");
    options.description = "Description utilisée dans les devis/factures";
    options.tinymce = true;
    options.required = true;
    var view = new _widgets_TextAreaWidget_js__WEBPACK_IMPORTED_MODULE_7__.default(options);
    this.showChildView('description', view);
  },
  showSupplierHt: function showSupplierHt() {
    var options = this.getCommonFieldOptions('supplier_ht', "Coût unitaire HT");
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView('supplier_ht', view);
  },
  showGeneralOverhead: function showGeneralOverhead() {
    var options = this.getCommonFieldOptions('general_overhead', 'Coefficient de frais généraux');
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView('general_overhead', view);
  },
  showMarginRate: function showMarginRate() {
    var options = this.getCommonFieldOptions('margin_rate', 'Coefficient de marge');
    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView('margin_rate', view);
  },
  showHt: function showHt() {
    var options = this.getCommonFieldOptions('ht', 'Montant unitaire HT');
    var supplier_value = this.model.get('supplier_ht');

    if (options.editable === false) {
      options.description = "Sera calculé depuis le coût unitaire";
    } else if (Boolean(supplier_value)) {
      console.log("supplier_value %s", supplier_value);
      options.description = "Calculé depuis le coût unitaire";
      options.editable = false;
    }

    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView('ht', view);
  },
  showWorkQuantity: function showWorkQuantity() {
    var label = "Quantité par produit composé";
    var description_text = "Quantité de ce produit dans chaque unité de produit composé";

    if (!this.model.get('quantity_inherited')) {
      label = "Quantité : saisie libre";
      description_text = "";
    }

    var options = this.getCommonFieldOptions('work_unit_quantity', label);

    if (description_text != "") {
      options.description = description_text;
    }

    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_5__.default(options);
    this.showChildView('work_unit_quantity', view);
  },
  showUnity: function showUnity() {
    var options = this.getCommonFieldOptions('unity', "Unité");
    options.options = this.unity_options;
    options.placeholder = 'Choisir une unité';
    options.required = true;
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__.default(options);
    this.showChildView("unity", view);
  },
  showTva: function showTva() {
    // NB : ici les tva_options sont différentes de celles utilisées dans le reset de la view
    // car elles sont modifiées par le SelectWidget et on perd donc l'info de la
    // tva par défaut
    var tva_options = this.config.request('get:options', 'tvas');
    var options = this.getCommonFieldOptions('tva_id', 'TVA');
    options.id_key = 'id';
    options.options = tva_options;
    options.placeholder = 'Choisir un taux de TVA';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__.default(options);
    this.showChildView('tva_id', view);
  },
  showProduct: function showProduct() {
    var options = this.getCommonFieldOptions('product_id', 'Compte produit');
    this.product_options = this.getProductOptions(this.tva_options, this.all_product_options, this.model.get('tva_id'));
    options.options = this.product_options;
    options.id_key = 'id';
    options.placeholder = 'Choisir un compte produit';
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_8__.default(options);
    this.showChildView('product_id', view);
  },
  templateContext: function templateContext() {
    var title = "Ajouter un produit";

    if (this.getOption('edit')) {
      title = "Modifier";
    }

    return {
      title: title,
      add: !this.getOption('edit')
    };
  },
  onRender: function onRender() {
    this.refreshForm();

    if (!this.getOption('edit')) {
      var view = new _common_views_CatalogComponent_js__WEBPACK_IMPORTED_MODULE_10__.default({
        query_params: {
          type_: 'product'
        },
        url: AppOption['catalog_tree_url'],
        multiple: true
      });
      this.showChildView('catalogContainer', view);
    }
  },
  onUnlockClicked: function onUnlockClicked() {
    var confirm;

    if (this.model.get('quantity_inherited')) {
      confirm = window.confirm("En éditant librement ce produit les quantités ne se mettront plus à jour lorsque vous modifierez la quantité du produit composé. Confirmez-vous ?");
    } else {
      confirm = true;
    }

    if (confirm) {
      this.model.set('quantity_inherited', !this.model.get('quantity_inherited'));
      this.refreshForm();
    }
  },
  onDatasChanged: function onDatasChanged(attribute, value) {
    if (attribute == 'supplier_ht') {
      this.model.set('supplier_ht', value);
      this.showHt(true);
    }
  },
  onAttach: function onAttach() {
    this.getUI('main_tab').tab('show');
  },
  onCatalogInsert: function onCatalogInsert(sale_products) {
    var _this = this;

    console.log(this.getOption("destCollection"));
    console.log(this.facade);
    var req = this.facade.request('workitem:from:catalog', this.getOption("destCollection"), sale_products);
    console.log(req);
    req.then(function () {
      return _this.triggerMethod('modal:close');
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemForm);

/***/ }),

/***/ "./src/price_study/views/workform/WorkItemView.js":
/*!********************************************************!*\
  !*** ./src/price_study/views/workform/WorkItemView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../math.js */ "./src/math.js");
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/*
 * Module name : WorkItemView
 */






var template = __webpack_require__(/*! ./templates/WorkItemView.mustache */ "./src/price_study/views/workform/templates/WorkItemView.mustache");

var WorkItemView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: template,
  tagName: 'tr',
  regions: {
    editButtonContainer: {
      el: '.col_actions .edit',
      replaceElement: true
    },
    delButtonContainer: {
      el: '.col_actions .delete',
      replaceElement: true
    }
  },
  childViewEvents: {
    'action:clicked': 'onActionClicked'
  },
  modelEvents: {
    'sync': 'render'
  },
  initialize: function initialize() {
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.user_prefs = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('user_preferences');
    this.unity_options = this.config.request('get:options', 'unities');
  },
  onRender: function onRender() {
    var editModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Modifier cet élément',
      icon: 'pen',
      showLabel: false,
      action: 'edit'
    });
    var deleteModel = new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_1__.default({
      ariaLabel: 'Supprimer cet élément',
      icon: 'trash-alt',
      showLabel: false,
      action: 'delete',
      css: 'negative'
    });
    this.showChildView('editButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: editModel
    }));
    this.showChildView('delButtonContainer', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      model: deleteModel
    }));
  },
  templateContext: function templateContext() {
    console.log("WorkItemView Calling the templating context");
    var ht_label = this.user_prefs.request('formatAmount', this.model.get('ht'), false);
    var ht_full_label = (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.get('ht'));
    return {
      tva_label: this.model.tva_label(),
      ht_label: ht_label,
      ht_full_label: ht_full_label,
      ht_rounded: ht_label != ht_full_label,
      supplier_ht_label: this.user_prefs.request('formatAmount', this.model.get('supplier_ht'), false),
      supplier_ht_mode: this.model.supplier_ht_mode(),
      product_label: this.model.product_label(),
      work_unit_ht_label: this.user_prefs.request('formatAmount', this.model.get('work_unit_ht'), false),
      total_ht_label: this.user_prefs.request('formatAmount', this.model.get('total_ht'), false)
    };
  },
  onActionClicked: function onActionClicked(action) {
    if (action == 'edit') {
      this.triggerMethod('model:edit', this.model, this);
    } else if (action == 'delete') {
      this.triggerMethod('model:delete', this.model, this);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (WorkItemView);

/***/ }),

/***/ "./src/price_study/views/discount/templates/DiscountCollectionView.mustache":
/*!**********************************************************************************!*\
  !*** ./src/price_study/views/discount/templates/DiscountCollectionView.mustache ***!
  \**********************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div></div>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/discount/templates/DiscountComponent.mustache":
/*!*****************************************************************************!*\
  !*** ./src/price_study/views/discount/templates/DiscountComponent.mustache ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class=\"title\">Remises</h2>\n<div class=\"table_container items\">\n    <table class=\"hover_table\">\n		<caption class='screen-reader-text'>\n			Liste des remises accordées au client dans cette étude de prix\n		</caption>\n		<thead>\n			<tr>\n				<th scope=\"col\" class=\"col_text\">Description</th>\n				<th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n					<span class=\"screen-reader-text\">Pourcentage </span>%\n				</th>\n				<th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n					<span class=\"screen-reader-text\">Prix </span>HT\n				</th>\n				<th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\">\n					<span class=\"screen-reader-text\">Taux de </span>TVA\n				</th>\n				<th scope=\"col\" class=\"col_actions width_two\" title=\"Actions\">\n					<span class=\"screen-reader-text\">Actions</span>\n				</th>\n			</tr>\n		</thead>\n		<tbody>\n		</tbody>\n<!-- \n		<tfoot>\n			<tr>\n				<td class=\"col_actions\" colspan=\"5\">\n					<ul>\n						<li class='add'></li>\n					</ul>\n				</td>\n			</tr>\n		</tfoot>\n -->\n    </table>\n    <div class='popin'></div>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/discount/templates/DiscountEmptyView.mustache":
/*!*****************************************************************************!*\
  !*** ./src/price_study/views/discount/templates/DiscountEmptyView.mustache ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<em>\nAucune remise n’a encore été ajoutée\n</em>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/discount/templates/DiscountForm.mustache":
/*!************************************************************************!*\
  !*** ./src/price_study/views/discount/templates/DiscountForm.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div role=\"dialog\" id=\"product-forms\" aria-modal=\"true\" aria-labelledby=\"product-forms_title\">\n        <div class='modal_layout'>\n            <header>\n                <button class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"product-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":8,"column":45},"end":{"line":8,"column":56}}}) : helper)))
    + "</h2>\n            </header>\n            <main>\n            <form class='form discount-form'>\n                <div class='field-order'></div>\n                <div class='field-type_'></div>\n                <div class='field-description'></div>\n                <div class='field-amount required'></div>\n                <div class='field-percentage required'></div>\n                <div class='field-tva_id required'></div>\n\n                <button\n                    class='btn btn-primary'\n                    type='submit'\n                    value='submit'>\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":23,"column":20},"end":{"line":23,"column":31}}}) : helper)))
    + "\n                </button>\n                <button\n                    class='btn'\n                    type='reset'\n                    value='submit'>\n                    Annuler\n                </button>\n                </form>\n            </main>\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/discount/templates/DiscountView.mustache":
/*!************************************************************************!*\
  !*** ./src/price_study/views/discount/templates/DiscountView.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"percentage") || (depth0 != null ? lookupProperty(depth0,"percentage") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"percentage","hash":{},"data":data,"loc":{"start":{"line":4,"column":4},"end":{"line":4,"column":20}}}) : helper)))
    + " %\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    -\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":14,"column":8},"end":{"line":14,"column":23}}}) : helper)))
    + "\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":21},"end":{"line":1,"column":38}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_percentage") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":4},"end":{"line":5,"column":11}}})) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>\n    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":8,"column":4},"end":{"line":8,"column":26}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_percentage") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":11,"column":4},"end":{"line":15,"column":11}}})) != null ? stack1 : "")
    + "</td>\n<td class='col_actions width_two'>\n    <div class='edit'></div>\n    <div class='delete'></div>\n</td>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/PriceStudyComponent.mustache":
/*!**********************************************************************!*\
  !*** ./src/price_study/views/templates/PriceStudyComponent.mustache ***!
  \**********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<div class='tasks separate_block border_left_block'></div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <div class='border_left_block'>\n    	<div class='actions content_double_padding align_right'>\n            <a class=\"btn\"\n               href=\"#addproduct\"\n               title=\"Ajouter un produit\">\n                <svg>\n                    <use href=\"/static/icons/endi.svg#plus\">   </use>\n                </svg>Ajouter un produit\n            </a>\n            <a class=\"btn\"\n               href=\"#addwork\"\n               title=\"Ajouter un produit composé\">\n                <svg>\n                    <use href=\"/static/icons/endi.svg#plus\">   </use>\n                </svg>Ajouter un produit composé\n            </a>    	\n    	</div>\n    </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "	<div class='actions content_double_padding align_right'>\n		<a class=\"btn\"\n		   href=\"#adddiscount\"\n		   title=\"Ajouter une remise\">\n			<svg>\n				<use href=\"/static/icons/endi.svg#plus\">   </use>\n			</svg>Ajouter une remise\n		</a>\n	</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div id=\"actions_toolbar\" class=\"task-desktop-actions\">\n    <div class='toolbar'></div>\n    <div class='errors'></div>\n</div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_tasks") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":5,"column":0},"end":{"line":7,"column":7}}})) != null ? stack1 : "")
    + "<div class='private'>\n	<div class='common separate_block border_left_block'></div>\n</div>\n\n<div class='separate_block composite'>\n    <div class='border_left_block'>\n		<h2 class='title'>Liste des produits</h2>\n    </div>\n    <div class='products'></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"editable") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":17,"column":4},"end":{"line":36,"column":11}}})) != null ? stack1 : "")
    + "</div>\n\n<div class='separate_block separate_block border_left_block'>\n    <div class='discounts table_container'></div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"editable") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":41,"column":4},"end":{"line":51,"column":11}}})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/PriceStudyResume.mustache":
/*!*******************************************************************!*\
  !*** ./src/price_study/views/templates/PriceStudyResume.mustache ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <tr>\n<th scope=\"row\">Total HT avant remise</th>\n<td>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_before_discount_label") || (depth0 != null ? lookupProperty(depth0,"ht_before_discount_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht_before_discount_label","hash":{},"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":7,"column":34}}}) : helper))) != null ? stack1 : "")
    + "</td>\n</tr>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tr>\n<th scope='row'>\nTVA : "
    + container.escapeExpression(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + "\n</th>\n<td>\n"
    + ((stack1 = alias1((depth0 != null ? lookupProperty(depth0,"value") : depth0), depth0)) != null ? stack1 : "")
    + "\n</td>\n</tr>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<caption class='screen-reader-text'>Totaux de l'étude</caption>\n<tbody>\n\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_discounts") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":4},"end":{"line":9,"column":7}}})) != null ? stack1 : "")
    + "<tr>\n<th scope='row'>\nTotal HT\n</th>\n<td>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":15,"column":0},"end":{"line":15,"column":14}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n</tr>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"tva_labels") : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":18,"column":0},"end":{"line":27,"column":9}}})) != null ? stack1 : "")
    + "<tr>\n<th scope='row'>\nTotal TTC\n</th>\n<td>\n"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc_label") || (depth0 != null ? lookupProperty(depth0,"ttc_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ttc_label","hash":{},"data":data,"loc":{"start":{"line":33,"column":0},"end":{"line":33,"column":16}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n</tr>\n</tbody>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/PriceStudyView.mustache":
/*!*****************************************************************!*\
  !*** ./src/price_study/views/templates/PriceStudyView.mustache ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class='title'>\n	<span class=\"icon status neutral\" title=\"Ces informations internes n’apparaissent pas dans les documents finaux (Devis/Factures)\">\n		<svg><use href=\"/static/icons/endi.svg#eye-slash\"></use></svg>\n	</span>\n	Informations générales \n	<small>N’apparaissent pas dans les documents finaux (Devis/Factures)</small>\n</h2>\n<div class='layout flex content'>\n    <div class='field-name col-md-6'></div>\n    <div class='field-notes col-md-6'></div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/ProductComponent.mustache":
/*!*******************************************************************!*\
  !*** ./src/price_study/views/templates/ProductComponent.mustache ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='collection '>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/ProductEmptyView.mustache":
/*!*******************************************************************!*\
  !*** ./src/price_study/views/templates/ProductEmptyView.mustache ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"border_left_block content_double_padding\">\n<em>\nAucun produit n’a encore été ajouté\n</em>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/ProductForm.mustache":
/*!**************************************************************!*\
  !*** ./src/price_study/views/templates/ProductForm.mustache ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "            <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                <li role=\"presentation\" class=\"active\">\n                    <a href=\"#form-container\"\n                        aria-controls=\"form-container\"\n                        role=\"tab\"\n                        data-toggle=\"tab\"\n                        tabindex='-1'\n                        >\n                        Saisie libre\n                    </a>\n                </li>\n                <li role=\"presentation\">\n                    <a href=\"#catalog-container\"\n                        aria-controls=\"catalog-container\"\n                        role=\"tab\"\n                        tabindex='-1'\n                        data-toggle=\"tab\">\n                        Depuis le catalogue\n                    </a>\n                </li>\n            </ul>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane\"\n                    id=\"catalog-container\">\n                </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div role=\"dialog\" id=\"product-forms\" aria-modal=\"true\" aria-labelledby=\"product-forms_title\">\n        <div class='modal_layout'>\n            <header>\n                <button class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"product-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":8,"column":45},"end":{"line":8,"column":56}}}) : helper)))
    + "</h2>\n            </header>\n            <main>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":12},"end":{"line":33,"column":19}}})) != null ? stack1 : "")
    + "            <div class='tab-content'>\n                <div\n                    role=\"tabpanel\"\n                    class=\"tab-pane fade in active\"\n                    id=\"form-container\">\n                    <form class='form product-form'>\n                        <div class='field-order'></div>\n                        <div class='field-description required'></div>\n                        <div class='field-supplier_ht'></div>\n                        <div class='field-general_overhead'></div>\n                        <div class='field-margin_rate'></div>\n                        <div class='field-ht'></div>\n                        <div class='field-quantity required'></div>\n                        <div class='field-unity'></div>\n                        <div class='field-tva_id required'></div>\n                        <div class='field-product_id'></div>\n                        <button\n                            class='btn btn-primary'\n                            type='submit'\n                            value='submit'>\n                            "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":54,"column":28},"end":{"line":54,"column":39}}}) : helper)))
    + "\n                        </button>\n                        <button\n                            class='btn'\n                            type='reset'\n                            value='submit'>\n                            Annuler\n                        </button>\n                    </form>\n                </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":64,"column":16},"end":{"line":70,"column":23}}})) != null ? stack1 : "")
    + "            </div>\n            </main>\n<!--\n            <footer>\n            </footer>\n -->\n        </div>\n    </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/ProductView.mustache":
/*!**************************************************************!*\
  !*** ./src/price_study/views/templates/ProductView.mustache ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":23,"column":4},"end":{"line":23,"column":29}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "				-\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":30,"column":4},"end":{"line":30,"column":21}}}) : helper)))
    + "\n";
},"7":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"general_overhead") || (depth0 != null ? lookupProperty(depth0,"general_overhead") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"general_overhead","hash":{},"data":data,"loc":{"start":{"line":37,"column":4},"end":{"line":37,"column":26}}}) : helper)))
    + "\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "					<br /><em title=\"obtenu par arrondi de\">\n						<small><span class=\"screen-reader-text\">obtenu par arrondi de </span>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_full_label") || (depth0 != null ? lookupProperty(depth0,"ht_full_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht_full_label","hash":{},"data":data,"loc":{"start":{"line":46,"column":75},"end":{"line":46,"column":96}}}) : helper))) != null ? stack1 : "")
    + "</small>\n						</em>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class='table_container'>\n	<table>\n		<thead>\n			<th scope=\"col\" class=\"col_text\">Description</th>\n			<th scope=\"col\" title=\"Prix unitaire Hors Taxe\" class=\"col_number\">Coût unit<span class=\"screen-reader-text\">aire</span>. HT</th>\n			<th scope='col' class='col_number' title='Coefficient de marge'>\n				Coef<span class='screen-reader-text'>ficient de</span>. marge\n			</th>\n			<th scope=\"col\" class=\"col_number\" title=\"Coefficient de frais généraux\">Coef<span class='screen-reader-text'>ficient de</span>. frais<span class='screen-reader-text'> généraux</span></th>\n			<th scope=\"col\" title=\"Prix unitaire Hors Taxe\" class=\"col_number\">Prix unit<span class=\"screen-reader-text\">aire</span>. HT</th>\n			<th scope=\"col\" title=\"Quantité\">Q<span class=\"screen-reader-text\">anti</span>té</th>\n			<th scope=\"col\" class=\"col_text\">Unité</th>\n			<th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\"><span class=\"screen-reader-text\">Prix </span>HT</th>\n			<th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\"><span class=\"screen-reader-text\">Taux de </span>TVA</th>\n			<th scope=\"col\" class=\"col_text\" title=\"Compte produit\">Compte produit</th>\n			<th scope=\"col\" class=\"col_actions width_two\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n		</thead>\n		<tbody>\n			<tr>\n				<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":20,"column":25},"end":{"line":20,"column":44}}}) : helper))) != null ? stack1 : "")
    + "</td>\n				<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":22,"column":4},"end":{"line":26,"column":11}}})) != null ? stack1 : "")
    + "				</td>\n				<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":29,"column":4},"end":{"line":33,"column":11}}})) != null ? stack1 : "")
    + "				</td>\n				<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":36,"column":4},"end":{"line":40,"column":11}}})) != null ? stack1 : "")
    + "				</td>\n				<td class='col_number'>\n					"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":43,"column":5},"end":{"line":43,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ht_rounded") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":44,"column":5},"end":{"line":48,"column":13}}})) != null ? stack1 : "")
    + "				</td>\n				<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":50,"column":27},"end":{"line":50,"column":41}}}) : helper)))
    + "</td>\n				<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":51,"column":25},"end":{"line":51,"column":36}}}) : helper)))
    + "</td>\n				<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":52,"column":27},"end":{"line":52,"column":49}}}) : helper))) != null ? stack1 : "")
    + "</td>\n				<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":53,"column":27},"end":{"line":53,"column":42}}}) : helper)))
    + "</td>\n\n				<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":55,"column":25},"end":{"line":55,"column":44}}}) : helper)))
    + "</td>\n				<td class='col_actions width_two'></td>\n			</tr>\n		</tbody>\n	</table>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/RootComponent.mustache":
/*!****************************************************************!*\
  !*** ./src/price_study/views/templates/RootComponent.mustache ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class='modal-container'>\n</div>\n<div class='main'></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/TaskListComponent.mustache":
/*!********************************************************************!*\
  !*** ./src/price_study/views/templates/TaskListComponent.mustache ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "		<br />\n		En synchronisant l’étude de prix avec un document (pas encore validé), vous pouvez regénérer les prestations depuis les données présentées ci-dessous.\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				<tr>\n					<td class='col_text' onclick='document.location="
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"url") : depth0), depth0))
    + "' title=\"Voir le document « "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + " »\">"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"type_label") : depth0), depth0))
    + " : "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + "</td>\n					<td class='col_actions width_two'>\n					<a href='"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"url") : depth0), depth0))
    + "' class='btn icon only' title=\"Voir le document « "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + " »\" aria-label=\"Voir le document « "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + " »\"><svg><use href=\"/static/icons/endi.svg#arrow-right\"></use></svg></a>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"sync_url") : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":25,"column":5},"end":{"line":28,"column":12}}})) != null ? stack1 : "")
    + "					</td>\n				</tr>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "					<a class='btn icon only' href='"
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"sync_url") : depth0), depth0))
    + "' onclick=\"return confirm('En synchronisant les données, vous écraserez les prestations existantes du document. Continuer ?')\" title=\"Synchroniser le contenu de l’étude de prix avec le document "
    + alias2(alias1((depth0 != null ? lookupProperty(depth0,"label") : depth0), depth0))
    + "\">\n					<svg><use href=\"/static/icons/endi.svg#redo-alt\"></use></svg></a>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<h2 class=\"title\">Documents générés depuis cette étude de prix</h2>\n<div>\n	<div class='alert alert-info'>\n		<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#info-circle\"></use></svg></span>\n		Vous pouvez retrouver les documents générés depuis cette étude de prix.\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":6,"column":2},"end":{"line":9,"column":9}}})) != null ? stack1 : "")
    + "	</div>\n	<div class='table_container'>\n		<table class=\"hover_table\">\n			<thead>\n				<tr>\n					<th scope=\"col\" class=\"col_text\" title=\"Document\"><span class=\"screen-reader-text\">Document</span></th>\n					<th scope=\"col\" class=\"col_actions\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n				</tr>\n			</thead>\n			<tbody>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"tasks") : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":3},"end":{"line":31,"column":12}}})) != null ? stack1 : "")
    + "			</tbody>\n		</table>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/WorkItemCollectionView.mustache":
/*!*************************************************************************!*\
  !*** ./src/price_study/views/templates/WorkItemCollectionView.mustache ***!
  \*************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "	<table class=\"hover_table\">\n        <caption class='screen-reader-text'>\n            Liste des produits composants le produit composé\n        </caption>\n        <thead>\n            <tr>\n                <th scope=\"col\" class=\"col_text\">Description</th>\n                <th scope=\"col\" title=\"Coût unitaire Hors Taxe\" class=\"col_number\">\n                    Coût unit<span class=\"screen-reader-text\">aire</span>. HT\n                </th>\n                <th scope=\"col\" title=\"Prix unitaire Hors Taxe\" class=\"col_number\">\n                    Prix unit<span class=\"screen-reader-text\">aire</span>. HT\n                </th>\n                <th scope=\"col\" title=\"Quantité par unité de produit composé\" class=\"col_number\">\n                    Q<span class=\"screen-reader-text\">uanti</span>té par unité<span class=\"screen-reader-text\"> de produit composé</span>\n                </th>\n                <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n                    <span class=\"screen-reader-text\">Prix </span>HT par unité<span class=\"screen-reader-text\"> de produit composé</span>\n                </th>\n                <th scope=\"col\" title=\"Quantité\">\n                    Q<span class=\"screen-reader-text\">uanti</span>té\n                </th>\n                <th scope=\"col\" class=\"col_text\">Unité</th>\n                <th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\">\n                    <span class=\"screen-reader-text\">Taux de </span>TVA\n                </th>\n                <th scope=\"col\" class=\"col_text\" title=\"Compte produit\">\n                	Compte produit\n                </th>\n                <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">      <span class=\"screen-reader-text\">Prix </span>HT\n                </th>\n            </tr>\n        </thead>\n        <tbody>\n        </tbody>\n    </table>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/WorkItemEmptyView.mustache":
/*!********************************************************************!*\
  !*** ./src/price_study/views/templates/WorkItemEmptyView.mustache ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<td class='col_text' colspan='11'>\n<em>\nAucun produit n’a encore été ajouté\n</em>\n</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/WorkItemView.mustache":
/*!***************************************************************!*\
  !*** ./src/price_study/views/templates/WorkItemView.mustache ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":3,"column":25},"end":{"line":3,"column":50}}}) : helper))) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    return "-";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <br /><em title=\"obtenu par arrondi de\">\n        <small><span class=\"screen-reader-text\">obtenu par arrondi de </span>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_full_label") || (depth0 != null ? lookupProperty(depth0,"ht_full_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht_full_label","hash":{},"data":data,"loc":{"start":{"line":9,"column":77},"end":{"line":9,"column":98}}}) : helper))) != null ? stack1 : "")
    + "</small>\n        </em>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":21},"end":{"line":1,"column":40}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":3,"column":0},"end":{"line":3,"column":66}}})) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number'>\n   "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":6,"column":3},"end":{"line":6,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ht_rounded") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + "    </td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"work_unit_quantity") || (depth0 != null ? lookupProperty(depth0,"work_unit_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"work_unit_quantity","hash":{},"data":data,"loc":{"start":{"line":13,"column":23},"end":{"line":13,"column":47}}}) : helper)))
    + "</td>\n<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"work_unit_ht_label") || (depth0 != null ? lookupProperty(depth0,"work_unit_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"work_unit_ht_label","hash":{},"data":data,"loc":{"start":{"line":14,"column":23},"end":{"line":14,"column":49}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"total_quantity") || (depth0 != null ? lookupProperty(depth0,"total_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_quantity","hash":{},"data":data,"loc":{"start":{"line":15,"column":23},"end":{"line":15,"column":43}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":16,"column":21},"end":{"line":16,"column":32}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":17,"column":21},"end":{"line":17,"column":36}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":18,"column":21},"end":{"line":18,"column":40}}}) : helper)))
    + "</td>\n<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":19,"column":23},"end":{"line":19,"column":45}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/templates/WorkView.mustache":
/*!***********************************************************!*\
  !*** ./src/price_study/views/templates/WorkView.mustache ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                Le détail des prestations est affiché dans le document final\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                Le détail des prestations n'est pas affiché dans le document final\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "\n<h3>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":2,"column":15}}}) : helper))) != null ? stack1 : "")
    + "</h3>\n<div class='table_container'>\n    <table>\n        <thead>\n            <th colspan=\"2\" scope=\"col\" class=\"col_text\">Description</th>\n            <th scope=\"col\" class=\"col_number\" title=\"Coefficient de frais généraux\">Coef<span class='screen-reader-text'>ficient de</span>. frais<span class='screen-reader-text'> généraux</span></th>\n            <th scope=\"col\" class=\"col_number\" title=\"Coefficient de marge\">Coef<span class='screen-reader-text'>ficient de</span>. marge</th>\n            <th scope=\"col\" class=\"col_number\" title=\"Prix unitaire Hors Taxe\">\n                Prix unit<span class=\"screen-reader-text\">aire</span>. HT\n            </th>\n            <th scope=\"col\" class=\"col_number\" title=\"Quantité\">\n                Q<span class=\"screen-reader-text\">uanti</span>té\n            </th>\n            <th scope=\"col\" class=\"col_text\">Unité</th>\n            <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n        		<span class=\"screen-reader-text\">Prix </span>HT\n            </th>\n            <th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\">\n                <span class=\"screen-reader-text\">Taux de </span>TVA\n            </th>\n            <th scope=\"col\" class=\"col_text\" title=\"Compte produit\">\n            	Compte produit\n            </th>\n            <th scope='col' class='col_actions width_two'><span class=\"screen-reader-text\">Actions</span></th>\n        </thead>\n        <tr>\n            <td colspan=\"2\" class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":28,"column":45},"end":{"line":28,"column":62}}}) : helper))) != null ? stack1 : "")
    + "</td>\n            <td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"general_overhead") || (depth0 != null ? lookupProperty(depth0,"general_overhead") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"general_overhead","hash":{},"data":data,"loc":{"start":{"line":29,"column":35},"end":{"line":29,"column":57}}}) : helper)))
    + "</td>\n            <td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"margin_rate") || (depth0 != null ? lookupProperty(depth0,"margin_rate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"margin_rate","hash":{},"data":data,"loc":{"start":{"line":30,"column":35},"end":{"line":30,"column":52}}}) : helper)))
    + "</td>\n            <td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":31,"column":35},"end":{"line":31,"column":51}}}) : helper))) != null ? stack1 : "")
    + "</td>\n            <td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"quantity") || (depth0 != null ? lookupProperty(depth0,"quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quantity","hash":{},"data":data,"loc":{"start":{"line":32,"column":35},"end":{"line":32,"column":47}}}) : helper)))
    + "</td>\n            <td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":33,"column":33},"end":{"line":33,"column":42}}}) : helper)))
    + "</td>\n            <td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":34,"column":35},"end":{"line":34,"column":55}}}) : helper))) != null ? stack1 : "")
    + "</td>\n            <td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":35,"column":33},"end":{"line":35,"column":48}}}) : helper))) != null ? stack1 : "")
    + "</td>\n            <td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":36,"column":33},"end":{"line":36,"column":52}}}) : helper))) != null ? stack1 : "")
    + "</td>\n            <td class='col_actions actions width_two'></td>\n        </tr>\n        <tr><td colspan='11' class='col_text'>\n            <small>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"display_details") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":41,"column":16},"end":{"line":45,"column":23}}})) != null ? stack1 : "")
    + "            </small>\n            </td>\n        </tr>\n        <tr>\n            <td class=\"level_spacer\"></td>\n            <td class=\"empty\" colspan='10'><div class=\"items_container items\"></div></td>\n        </tr>\n    </table>\n</div>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/AddWorkForm.mustache":
/*!***********************************************************************!*\
  !*** ./src/price_study/views/workform/templates/AddWorkForm.mustache ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<section id=\"work_form\" class=\"size_middle\">\n    <div role=\"dialog\" id=\"work-forms\" aria-modal=\"true\" aria-labelledby=\"work-forms_title\">\n        <div class='modal_layout'>\n            <header>\n                <button class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"work-forms_title\">Ajouter un produit composé : étape 1</h2>\n            </header>\n            <main>\n                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a href=\"#form-container\"\n                           aria-controls=\"form-container\"\n                           role=\"tab\"\n                           data-toggle=\"tab\"\n                           tabindex='-1'\n                           >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a href=\"#catalog-container\"\n                           aria-controls=\"catalog-container\"\n                           role=\"tab\"\n                           tabindex='-1'\n                           data-toggle=\"tab\">\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n                <div class='tab-content'>\n                    <div\n                         role=\"tabpanel\"\n                         class=\"tab-pane fade in active\"\n                         id=\"form-container\">\n                        <form name=\"current\">\n                            <input type='hidden' name='type_' value='price_study_work' />\n                            <div class='limited_width'>\n                                <div class='message-container'></div>\n                                <div class='errors'></div>\n                                <fieldset>\n                                    <div class='field-title'></div>\n                                    <div class='field-description'></div>\n                                </fieldset>\n                            </div>\n							<div class=\"layout flex main_actions\">\n								<div role='group'>\n									<button\n											class='btn btn-primary icon'\n											type='submit'\n											value='submit'>\n										<svg><use href=\"/static/icons/endi.svg#save\"></use></svg>Continuer\n									</button>\n									<button\n											class='btn icon negative'\n											type='reset'\n											value='submit'\n											title=\"Annuler et revenir en arrière\">\n										<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n									</button>\n								</div>\n								<div class='resume'></div>\n							</div>\n                        </form>\n                   </div>\n                    <div\n                         role=\"tabpanel\"\n                         class=\"tab-pane\"\n                         id=\"catalog-container\">\n                    </div>\n                </div>\n            </main>\n        </div>\n    </div>\n</section>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/ProductResume.mustache":
/*!*************************************************************************!*\
  !*** ./src/price_study/views/workform/templates/ProductResume.mustache ***!
  \*************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<b>Total HT par unité&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":1,"column":29},"end":{"line":1,"column":43}}}) : helper))) != null ? stack1 : "")
    + "</b>\n<br/>\n<b>Coût HT&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":3,"column":18},"end":{"line":3,"column":41}}}) : helper))) != null ? stack1 : "")
    + "</b>\n<br/>\n<b>Total HT&nbsp;: "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":5,"column":19},"end":{"line":5,"column":39}}}) : helper))) != null ? stack1 : "")
    + "</b>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/WorkForm.mustache":
/*!********************************************************************!*\
  !*** ./src/price_study/views/workform/templates/WorkForm.mustache ***!
  \********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form name=\"current\">\n\n    <div class='main_toolbar'>\n        <h3>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"title","hash":{},"data":data,"loc":{"start":{"line":4,"column":12},"end":{"line":4,"column":21}}}) : helper)))
    + "</h3>\n        <div class=\"layout flex main_actions\">\n            <div role='group'>\n                <button class='btn btn-primary icon' type='submit' value='submit'>\n                    <svg><use href=\"/static/icons/endi.svg#save\"></use></svg>Enregistrer\n                </button>\n				<button class='icon' type='reset' value='submit' title=\"Annuler et revenir en arrière\" aria-label=\"Annuler et revenir en arrière\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>Annuler\n                </button>\n            </div>\n            <div class='resume' role='group'></div>\n\n            <div role='group' class='other_buttons'>\n            </div>\n        </div>\n        <div class='message-container'></div>\n        <div class='errors'></div>\n    </div>\n\n    <fieldset class='separate_block border_left_block'>\n        <h2 class='title'>Informations</h2>\n        <div class='layout flex'>\n            <div class='field-title col-md-6'></div>\n            <div class='field-description col-md-6'></div>\n        </div>\n        <div class='layout flex'>\n            <div class='field-quantity col-md-6'></div>\n            <div class='field-display_details col-md-6'></div>\n        </div>\n    </fieldset>\n    <fieldset class='separate_block border_left_block'>\n        <h2 class='title'>Détails</h2>\n        <div class='layout flex'>\n            <div class='field-general_overhead col-md-4'></div>\n            <div class='field-margin_rate col-md-4'></div>\n            <div class='field-unity col-md-4'></div>\n        </div>\n        <div class='layout flex'>\n            <div class='field-tva_id col-md-4'></div>\n            <div class='field-product_id col-md-4'></div>\n        </div>\n    </fieldset>\n    <span class='items-container'></span>\n</form>";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/WorkItemComponent.mustache":
/*!*****************************************************************************!*\
  !*** ./src/price_study/views/workform/templates/WorkItemComponent.mustache ***!
  \*****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<h2 class='title'>\n	<span class=\"icon status caution\" title=\"Il faut au moins un produit pour créer un produit composé\">\n		<svg><use href=\"/static/icons/endi.svg#danger\"></use></svg>\n	</span>\n	Liste des produits\n</h2>\n<div class=\"table_container items\">\n    <table class=\"hover_table\">\n            <caption class='screen-reader-text'>\n                Liste des produits composants le produit composé\n            </caption>\n            <thead>\n                <tr>\n                    <th scope=\"col\" class=\"col_text\">Description</th>\n                    <th scope=\"col\" title=\"Coût unitaire Hors Taxe\" class=\"col_number\">\n                        Coût unit<span class=\"screen-reader-text\">aire</span>. HT\n                    </th>\n                    <th scope=\"col\" title=\"Prix unitaire Hors Taxe\" class=\"col_number\">\n                        Prix unit<span class=\"screen-reader-text\">aire</span>. HT\n                    </th>\n                    <th scope=\"col\" title=\"Quantité par unité de produit composé\" class=\"col_number\">\n                        Q<span class=\"screen-reader-text\">uanti</span>té par unité<span class=\"screen-reader-text\"> de produit composé</span>\n                    </th>\n                    <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n                    	<span class=\"screen-reader-text\">Prix </span>HT par unité<span class=\"screen-reader-text\"> de produit composé</span>\n                    </th>\n                    <th scope=\"col\" title=\"Quantité\"  class=\"col_number\">\n                        Q<span class=\"screen-reader-text\">uanti</span>té\n                    </th>\n                    <th scope=\"col\" class=\"col_text\">Unité</th>\n                    <th scope=\"col\" class=\"col_text\" title=\"Taux de TVA\">\n                        <span class=\"screen-reader-text\">Taux de </span>TVA\n                    </th>\n                    <th scope=\"col\" class=\"col_text\" title=\"Compte produit\">\n                    	Compte produit\n                    </th>\n                    <th scope=\"col\" class=\"col_number\" title=\"Prix Hors Taxe\">\n                        <span class=\"screen-reader-text\">Prix </span>HT\n                    </th>\n                    <th scope=\"col\" class=\"col_actions width_two\" title=\"Actions\">\n                        <span class=\"screen-reader-text\">Actions</span>\n                    </th>\n                </tr>\n            </thead>\n            <tbody>\n            </tbody>\n            <tfoot>\n            	<tr>\n            		<td class=\"col_actions\" colspan=\"12\">\n						<ul>\n							<li class='add'></li>\n						</ul>\n            		</td>\n            	</tr>\n            </tfoot>\n    </table>\n<div class='popin'></div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/WorkItemForm.mustache":
/*!************************************************************************!*\
  !*** ./src/price_study/views/workform/templates/WorkItemForm.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "                <ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n                    <li role=\"presentation\" class=\"active\">\n                        <a href=\"#form-container\"\n                           aria-controls=\"form-container\"\n                           role=\"tab\"\n                           data-toggle=\"tab\"\n                           tabindex='-1'\n                           >\n                            Saisie libre\n                        </a>\n                    </li>\n                    <li role=\"presentation\">\n                        <a href=\"#catalog-container\"\n                           aria-controls=\"catalog-container\"\n                           role=\"tab\"\n                           tabindex='-1'\n                           data-toggle=\"tab\">\n                            Depuis le catalogue\n                        </a>\n                    </li>\n                </ul>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "                    <div\n                         role=\"tabpanel\"\n                         class=\"tab-pane\"\n                         id=\"catalog-container\">\n                    </div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<section class=\"size_middle\">\n    <div role=\"dialog\" id=\"product-forms\" aria-modal=\"true\" aria-labelledby=\"product-forms_title\">\n        <div class='modal_layout'>\n            <header>\n                <button class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\" >\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"product-forms_title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":8,"column":45},"end":{"line":8,"column":56}}}) : helper)))
    + "</h2>\n            </header>\n            <main>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":11,"column":16},"end":{"line":33,"column":23}}})) != null ? stack1 : "")
    + "                <div class='tab-content'>\n                    <div\n                         role=\"tabpanel\"\n                         class=\"tab-pane fade in active\"\n                         id=\"form-container\">\n                        <form class='form product-form'>\n                            <div class='field-quantity_inherited'></div>\n                            <div class='errors'></div>\n                            <fieldset>\n                                <div class='field-description'></div>\n\n                                <div class=\"layout flex\">\n                                <div class='col-md-4 field-supplier_ht'></div>\n                                    <div class='col-md-4 field-general_overhead'></div>\n                                    <div class='col-md-4 field-margin_rate'></div>\n                                </div>\n                                <div class='layout flex'>\n                                    <div class='col-md-4 field-ht'></div>\n                                    <div class='col-md-4 field-work_unit_quantity'></div>\n                                    <div class='col-md-4 field-unity'></div>\n                                </div>\n                                <div class=\"layout flex\">\n                                    <div class='col-md-6 field-tva_id'></div>\n                                    <div class='col-md-6 field-product_id'></div>\n                                </div>\n                            </fieldset>\n                            <button\n                                    class='btn btn-primary'\n                                    type='submit'\n                                    value='submit'>\n                                "
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":64,"column":32},"end":{"line":64,"column":43}}}) : helper)))
    + "\n                            </button>\n                            <button\n                                    class='btn'\n                                    type='reset'\n                                    value='submit'>\n                                Annuler\n                            </button>\n                        </form>\n                    </div>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":74,"column":20},"end":{"line":80,"column":27}}})) != null ? stack1 : "")
    + "                </div>\n            </main>\n<!-- \n            <footer>\n            </footer>\n -->\n        </div>\n    </div>\n</section>\n";
},"useData":true});

/***/ }),

/***/ "./src/price_study/views/workform/templates/WorkItemView.mustache":
/*!************************************************************************!*\
  !*** ./src/price_study/views/workform/templates/WorkItemView.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = ((helper = (helper = lookupProperty(helpers,"supplier_ht_label") || (depth0 != null ? lookupProperty(depth0,"supplier_ht_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_ht_label","hash":{},"data":data,"loc":{"start":{"line":3,"column":25},"end":{"line":3,"column":50}}}) : helper))) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    return "-";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <br /><em title=\"obtenu par arrondi de\">\n        <small><span class=\"screen-reader-text\">obtenu par arrondi de </span>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_full_label") || (depth0 != null ? lookupProperty(depth0,"ht_full_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ht_full_label","hash":{},"data":data,"loc":{"start":{"line":9,"column":77},"end":{"line":9,"column":98}}}) : helper))) != null ? stack1 : "")
    + "</small>\n        </em>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_text'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":21},"end":{"line":1,"column":40}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_ht_mode") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":3,"column":0},"end":{"line":3,"column":66}}})) != null ? stack1 : "")
    + "\n</td>\n<td class='col_number'>\n   "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":6,"column":3},"end":{"line":6,"column":19}}}) : helper))) != null ? stack1 : "")
    + "\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"ht_rounded") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"work_unit_quantity") || (depth0 != null ? lookupProperty(depth0,"work_unit_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"work_unit_quantity","hash":{},"data":data,"loc":{"start":{"line":13,"column":23},"end":{"line":13,"column":47}}}) : helper)))
    + "</td>\n<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"work_unit_ht_label") || (depth0 != null ? lookupProperty(depth0,"work_unit_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"work_unit_ht_label","hash":{},"data":data,"loc":{"start":{"line":14,"column":23},"end":{"line":14,"column":49}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class='col_number'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"total_quantity") || (depth0 != null ? lookupProperty(depth0,"total_quantity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_quantity","hash":{},"data":data,"loc":{"start":{"line":15,"column":23},"end":{"line":15,"column":43}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"unity") || (depth0 != null ? lookupProperty(depth0,"unity") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"unity","hash":{},"data":data,"loc":{"start":{"line":16,"column":21},"end":{"line":16,"column":32}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":17,"column":21},"end":{"line":17,"column":36}}}) : helper)))
    + "</td>\n<td class='col_text'>"
    + alias4(((helper = (helper = lookupProperty(helpers,"product_label") || (depth0 != null ? lookupProperty(depth0,"product_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"product_label","hash":{},"data":data,"loc":{"start":{"line":18,"column":21},"end":{"line":18,"column":40}}}) : helper)))
    + "</td>\n<td class='col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht_label") || (depth0 != null ? lookupProperty(depth0,"total_ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht_label","hash":{},"data":data,"loc":{"start":{"line":19,"column":23},"end":{"line":19,"column":45}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_actions\">\n	<ul>\n		<li class='edit'></li>\n		<li class='delete'></li>\n	</ul>\n</td>\n";
},"useData":true});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"price_study": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkenDI"] = self["webpackChunkenDI"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendor"], () => (__webpack_require__("./src/price_study/price_study.js")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=price_study.js.map