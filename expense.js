/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/base/behaviors/TabsBehavior.js":
/*!********************************************!*\
  !*** ./src/base/behaviors/TabsBehavior.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }



var TabsBehavior = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().Behavior.extend({
  /** Tabs behaviour
   *
   * Offers an abstraction layer over BootStrap 3 tabs
   * https://getbootstrap.com/docs/3.3/javascript/#tabs
   *
   * Secret plan: might be the first step for getting rid of bootstrap.
   *
   * For some reason, BS events other than show.bs.tab do not fire
   *
   * This behaviour fires following events:
   *
   *   tab:beforeSelect -> when a tab is about to be selected
   *   tab:beforeDeselect -> when a tab is about to be deselected
   */
  events: {
    'show.bs.tab': '_onBeforeTabSelect'
  },
  options: {
    tabPanes: []
  },
  onInitialize: function onInitialize() {
    var _iterator = _createForOfIteratorHelper(this.getOption('tabPanes')),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var tabPane = _step.value;
        var region = this.view.getRegion(tabPane.region);

        if (!region) {
          throw new Error('No such region: ' + tabPane.region);
        }
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  },

  /* From the event target on a tab click finds the tab pane region
   *
   * @param target the event target (DOM element)
   * @returns a Marionette Region
   */
  findTabPane: function findTabPane(target) {
    var tabPane = this.getOption('tabPanes').find(function (tabPane) {
      console.log('CMP', target.getAttribute('aria-controls'), tabPane.id);
      return target.getAttribute('aria-controls') == tabPane.id;
    });
    return this.view.getRegion(tabPane.region);
  },
  _onBeforeTabSelect: function _onBeforeTabSelect(e) {
    var oldTabRegion = this.findTabPane(e.relatedTarget);
    console.log('TabContainerBehavior: tab pane deselect :', oldTabRegion._name);
    this.view.triggerMethod('tab:beforeDeselect', oldTabRegion);
    var newTabRegion = this.findTabPane(e.target);
    console.log('TabContainerBehavior: tab pane select :', newTabRegion._name);
    this.view.triggerMethod('tab:beforeSelect', newTabRegion);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TabsBehavior);

/***/ }),

/***/ "./src/common/components/AttachmentUploadService.js":
/*!**********************************************************!*\
  !*** ./src/common/components/AttachmentUploadService.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_components_ConfigBus_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/components/ConfigBus.js */ "./src/base/components/ConfigBus.js");
/* harmony import */ var _tools__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools */ "./src/tools.js");
/* provided dependency */ var $ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");





/** A service to manage attachments for current view context
 *
 * We are not using standard collection-related functions for that, because
 * uploading binary stuff is not very efficient using JSON.
 *
 * Requirements :
 *  - AppOption.context_url is set
 *  - an AjaxFileUploadView is wired on `${AppOption.context_url}/attachments`
 *
 * API (on `facade` radio channel)
 *  - receive `uploadAttachments(file) requests
 *  - triggers `attachment:saved`
 */

var AttachmentUploadServiceClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().Object.extend({
  channelName: 'facade',
  radioRequests: {
    'uploadAttachment': 'uploadAttachment'
  },

  /**
   * @type {File} the file to be uploaded immediately
   * @return Promise resolving on JSON representation of successfuly
   *   uploaded file.
   */
  uploadAttachment: function uploadAttachment(file) {
    var formData = new FormData();
    formData.append('__formid__', 'deform');
    formData.append('__start__', 'upload:mapping');
    formData.append('upload', file);
    formData.append('__end__', 'upload:mapping');
    formData.append('description', file.name);
    formData.append('submit', 'submit');
    (0,_tools__WEBPACK_IMPORTED_MODULE_3__.showLoader)();
    return (0,_tools__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(AppOption['context_url'] + '/attachments', formData, 'POST', {
      contentType: false
    } // required for FormData with jQuery
    ).done(function (data) {
      backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade').trigger('attachment:saved', data.instance);
    }).always(_tools__WEBPACK_IMPORTED_MODULE_3__.hideLoader);
  },
  setup: function setup() {
    console.log("AttachmentUploadServiceClass.setup");
  },
  start: function start() {
    console.log("AttachmentUploadServiceClass.start");
    var result = $.Deferred();
    result.resolve(_base_components_ConfigBus_js__WEBPACK_IMPORTED_MODULE_2__.default.form_config, null, null);
    return result;
  }
});
var AttachmentUploadService = new AttachmentUploadServiceClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AttachmentUploadService);

/***/ }),

/***/ "./src/expense/components/App.js":
/*!***************************************!*\
  !*** ./src/expense/components/App.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _Router_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Router.js */ "./src/expense/components/Router.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _views_MainView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../views/MainView.js */ "./src/expense/views/MainView.js");
/* harmony import */ var _Controller_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Controller.js */ "./src/expense/components/Controller.js");
/* harmony import */ var _base_components_ConfigBus_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../base/components/ConfigBus.js */ "./src/base/components/ConfigBus.js");
/* harmony import */ var _common_components_ExpenseTypeService_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../common/components/ExpenseTypeService.js */ "./src/common/components/ExpenseTypeService.js");








var AppClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().Application.extend({
  region: '#js-main-area',
  channelName: 'app',
  onBeforeStart: function onBeforeStart(app, options) {
    console.log("AppClass.onBeforeStart");
    this.rootView = new _views_MainView_js__WEBPACK_IMPORTED_MODULE_3__.default();
    this.controller = new _Controller_js__WEBPACK_IMPORTED_MODULE_4__.default({
      rootView: this.rootView
    });
    this.router = new _Router_js__WEBPACK_IMPORTED_MODULE_1__.default({
      controller: this.controller
    });
    console.log("AppClass.onBeforeStart finished");
    _common_components_ExpenseTypeService_js__WEBPACK_IMPORTED_MODULE_6__.default.setFormConfig(_base_components_ConfigBus_js__WEBPACK_IMPORTED_MODULE_5__.default.form_config);
  },
  onStart: function onStart(app, options) {
    this.showView(this.rootView);
    console.log("Starting the history");
    (0,_tools_js__WEBPACK_IMPORTED_MODULE_2__.hideLoader)();
    backbone__WEBPACK_IMPORTED_MODULE_0___default().history.start();
  }
});
var App = new AppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (App);

/***/ }),

/***/ "./src/expense/components/Controller.js":
/*!**********************************************!*\
  !*** ./src/expense/components/Controller.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);


var Controller = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().Object.extend({
  initialize: function initialize(options) {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    console.log("Controller.initialize");
    this.rootView = options['rootView'];
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Controller);

/***/ }),

/***/ "./src/expense/components/Facade.js":
/*!******************************************!*\
  !*** ./src/expense/components/Facade.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var _base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/components/FacadeModelApiMixin.js */ "./src/base/components/FacadeModelApiMixin.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _models_TotalModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/TotalModel.js */ "./src/expense/models/TotalModel.js");
/* harmony import */ var _models_ExpenseCollection_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../models/ExpenseCollection.js */ "./src/expense/models/ExpenseCollection.js");
/* harmony import */ var _models_ExpenseKmCollection_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../models/ExpenseKmCollection.js */ "./src/expense/models/ExpenseKmCollection.js");
/* harmony import */ var _models_BookMarkCollection_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../models/BookMarkCollection.js */ "./src/expense/models/BookMarkCollection.js");
/* harmony import */ var _common_models_NodeFileCollection__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../common/models/NodeFileCollection */ "./src/common/models/NodeFileCollection.js");
/* harmony import */ var _common_models_StatusLogEntryCollection__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common/models/StatusLogEntryCollection */ "./src/common/models/StatusLogEntryCollection.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");











var FacadeClass = backbone_marionette__WEBPACK_IMPORTED_MODULE_10___default().Object.extend(_base_components_FacadeModelApiMixin_js__WEBPACK_IMPORTED_MODULE_1__.default).extend({
  radioEvents: {
    "changed:line": "computeLineTotal",
    "changed:kmline": "computeKmLineTotal",
    "attachment:saved": "onAttachmentSaved",
    "status:justified:changed": "onGlobalJustifiedChanged"
  },
  radioRequests: {
    'get:collection': 'getCollectionRequest',
    'get:model': 'getModelRequest',
    'get:totalmodel': 'getTotalModelRequest',
    'get:bookmarks': 'getBookMarks',
    'get:linkedFiles': 'getLinkedFiles',
    'get:file': 'getFile',
    'get:supplier': 'getSupplier',
    'has:expenseLines': 'hasExpenseLines'
  },
  onGlobalJustifiedChanged: function onGlobalJustifiedChanged(value) {
    this.collections.lines.each(function (l) {
      return l.justified !== null && l.set('justified', value);
    });
  },
  setup: function setup(options) {
    console.log("Facade.setup");
    console.table(options);
    this.url = options['context_url'];
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
  },
  start: function start() {
    console.log("Starting the facade");
    var deferred = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.ajax_call)(this.url);
    return deferred.then(this.setupModels.bind(this));
  },
  setupModels: function setupModels(context_datas) {
    this.datas = context_datas;
    this.models = {};
    this.collections = {};
    this.totalmodel = new _models_TotalModel_js__WEBPACK_IMPORTED_MODULE_4__.default();
    var lines = context_datas['lines'];
    this.collections['lines'] = new _models_ExpenseCollection_js__WEBPACK_IMPORTED_MODULE_5__.default(lines);
    this.collections.status_history = new _common_models_StatusLogEntryCollection__WEBPACK_IMPORTED_MODULE_9__.default(context_datas.status_history);
    this.watchLinkToFiles();
    this.watchCollectionAggregateStatus();
    var attachments = context_datas['attachments'];
    this.collections['attachments'] = new _common_models_NodeFileCollection__WEBPACK_IMPORTED_MODULE_8__.default(attachments); // FIXME

    var attachments_url = "".concat(AppOption.context_url, "/attachments");
    this.collections['attachments'].url = attachments_url;
    var kmlines = context_datas['kmlines'];
    this.collections['kmlines'] = new _models_ExpenseKmCollection_js__WEBPACK_IMPORTED_MODULE_6__.default(kmlines);
    var bookmarks = this.config.request('get:options', 'bookmarks');
    this.bookmarks = new _models_BookMarkCollection_js__WEBPACK_IMPORTED_MODULE_7__.default(bookmarks);
    this.suppliers = new (backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection)(this.config.request('get:options', 'suppliers'));
    this.computeLineTotal();
    this.computeKmLineTotal();
  },
  watchCollectionAggregateStatus: function watchCollectionAggregateStatus() {
    this.collections['lines'].on('aggregate:change:justified', function (newValue) {
      var channel = this.getChannel();
      channel.request('set:globalJustified', newValue);
    }.bind(this));
  },
  watchLinkToFiles: function watchLinkToFiles() {
    var lines = this.collections['lines'];
    lines.on('change:files', this.notifyLinkedFilesChanged.bind(this));
    lines.on('add remove', function (model) {
      if (model.get('files').length > 0) {
        this.notifyLinkedFilesChanged();
      }
    }, this);
  },
  notifyLinkedFilesChanged: function notifyLinkedFilesChanged() {
    this.getChannel().trigger('linkedFilesChanged');
  },
  getBookMarks: function getBookMarks() {
    return this.bookmarks;
  },
  hasExpenseLines: function hasExpenseLines() {
    return !this.collections['lines'].isEmpty();
  },
  getLinkedFiles: function getLinkedFiles() {
    return this.collections['lines'].collectLinkedFileIds();
  },
  getFile: function getFile(id) {
    return this.collections['attachments'].get(id);
  },
  getSupplier: function getSupplier(id) {
    return this.suppliers.findWhere(function (x) {
      return x.get('value') == id;
    });
  },
  computeLineTotal: function computeLineTotal() {
    /*
     * compute the line totals for the given category
     */
    var categories = ['1', '2'];
    var collection = this.collections['lines'];
    var datas = {};

    _.each(categories, function (category) {
      datas['ht_' + category] = collection.total_ht(category);
      datas['tva_' + category] = collection.total_tva(category);
      datas['ttc_' + category] = collection.total(category);
    });

    datas['ht'] = collection.total_ht();
    datas['tva'] = collection.total_tva();
    datas['ttc'] = collection.total();
    this.totalmodel.set(datas);
    var channel = this.getChannel();

    _.each(categories, function (category) {
      channel.trigger('change:lines_' + category);
    });
  },
  computeKmLineTotal: function computeKmLineTotal() {
    /*
     * Compute the kmline totals for the given category
     */
    var categories = ['1', '2'];
    var collection = this.collections['kmlines'];
    var datas = {};

    _.each(categories, function (category) {
      datas['km_' + category] = collection.total_km(category);
      datas['km_ttc_' + category] = collection.total(category);
    });

    datas['km_tva'] = collection.total_tva();
    datas['km_ht'] = collection.total_ht();
    datas['km'] = collection.total_km();
    datas['km_ttc'] = collection.total();
    this.totalmodel.set(datas);
    var channel = this.getChannel();

    _.each(categories, function (category) {
      var eventName = 'change:kmlines_' + category;
      console.log("Triggering %s", eventName);
      channel.trigger(eventName);
    });
  },
  getTotalModelRequest: function getTotalModelRequest() {
    return this.totalmodel;
  },
  onAttachmentSaved: function onAttachmentSaved(attachment) {
    this.collections['attachments'].add(attachment);
  }
});
var Facade = new FacadeClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Facade);

/***/ }),

/***/ "./src/expense/components/Router.js":
/*!******************************************!*\
  !*** ./src/expense/components/Router.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var marionette_approuter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! marionette.approuter */ "./node_modules/marionette.approuter/lib/marionette.approuter.esm.js");

var Router = marionette_approuter__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  appRoutes: {}
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Router);

/***/ }),

/***/ "./src/expense/components/ToolbarApp.js":
/*!**********************************************!*\
  !*** ./src/expense/components/ToolbarApp.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _common_components_ToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/components/ToolbarAppClass */ "./src/common/components/ToolbarAppClass.js");
/* harmony import */ var _common_models_ActionModel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../common/models/ActionModel */ "./src/common/models/ActionModel.js");
/* harmony import */ var _widgets_ToggleButtonWidget__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/ToggleButtonWidget */ "./src/widgets/ToggleButtonWidget.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_3__);
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");





function notifyJustifiedChanged(model) {
  var newValue = model.get('options').current_value;
  backbone_radio__WEBPACK_IMPORTED_MODULE_3___default().channel('facade').trigger('status:justified:changed', newValue);
}

var ToolbarAppClass = _common_components_ToolbarAppClass__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  channelName: 'facade',
  radioRequests: {
    'set:globalJustified': 'setGlobalJustified'
  },
  setGlobalJustified: function setGlobalJustified(value) {
    if (this.resume_view) {
      this.resume_view.model.get('options').current_value = value;
      this.resume_view.updateSelectOptions();
      this.resume_view.render();
    }
  },
  getResumeView: function getResumeView(actions) {
    var resume_view = null;

    if (!_.isUndefined(actions['justify'])) {
      var model = new _common_models_ActionModel__WEBPACK_IMPORTED_MODULE_1__.default(actions['justify']);
      resume_view = new _widgets_ToggleButtonWidget__WEBPACK_IMPORTED_MODULE_2__.default({
        model: model
      });
      resume_view.on('status:changed', notifyJustifiedChanged);
      this.resume_view = resume_view;
    }

    return resume_view;
  }
});
var ToolbarApp = new ToolbarAppClass();
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ToolbarApp);

/***/ }),

/***/ "./src/expense/compute/FieldsLinkers.js":
/*!**********************************************!*\
  !*** ./src/expense/compute/FieldsLinkers.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HtModelFieldsLinker": () => (/* binding */ HtModelFieldsLinker),
/* harmony export */   "TTCModelFieldsLinker": () => (/* binding */ TTCModelFieldsLinker)
/* harmony export */ });
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../math */ "./src/math.js");
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }


/** Reacts to edits of ExpensModel fields by recomputing other fields
 *
 * Notifies the caller which fields actually changed and needs to be re-displayed in UI.
 */

var AbstractFieldsLinker = {
  reactMap: null,
  // Inheritor must implement

  /** Recomputes other fields according to the field that has just been changed
   *
   * @param fieldName String
   * @param model ExpenseModel
   * @return String[] name of the fields having been recomputed
   */
  reactToFieldChange: function reactToFieldChange(fieldName, model) {
    if (Object.hasOwn(this.reactMap, fieldName)) {
      // First recompute (on model)
      var _iterator = _createForOfIteratorHelper(this.reactMap[fieldName]),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var rule = _step.value;
          var field = rule[0];
          var f = rule[1];
          var v = f(model);

          if (isNaN(v)) {
            // Avoid "NaN" in model data…
            v = null;
          }

          if (v) {
            // Will skip undefined/null
            v = (0,_math__WEBPACK_IMPORTED_MODULE_0__.round)(v);
          }

          model.set(field, v);
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }

      return this.reactMap[fieldName].map(function (x) {
        return x[0];
      });
    } else {
      return [];
    }
  }
};
/** Describes how to recompute linked fields in HT filling mode
 *
 * HT filling mode = user fills HT + TVA or HT + TVA-rate
 *
 */

var HtModelFieldsLinker = {
  /*
   * The field to watch
   * value : couples of field to set and the function to set it
   */
  reactMap: {
    tva: [["tva_rate", function (m) {
      return null;
    }], ["ttc_readonly", function (m) {
      return m.total();
    }]],
    tva_rate: [["tva", function (m) {
      return (0,_math__WEBPACK_IMPORTED_MODULE_0__.getTvaPart)(m.get('ht'), m.get('tva_rate'));
    }], ["ttc_readonly", function (m) {
      return m.total();
    }]],
    ht: [["tva_rate", function (m) {
      return null;
    }], ["ttc_readonly", function (m) {
      return m.total();
    }]]
  }
};
Object.setPrototypeOf(HtModelFieldsLinker, AbstractFieldsLinker);
/** Describes how to recompute linked fields in TTC filling mode
 *
 * TTC filling mode = user fills TTC + TVA or TTC + TVA-rate
 */

var TTCModelFieldsLinker = {
  /*
   * The field to watch
   * value : couples of field to set and the function to set it
   */
  reactMap: {
    tva: [["tva_rate", function (m) {
      return null;
    }], ["ht", function (m) {
      return m.get('ttc_readonly') - m.get('tva');
    }]],
    tva_rate: [["ht", function (m) {
      return (0,_math__WEBPACK_IMPORTED_MODULE_0__.htFromTtc)(m.get('ttc_readonly'), m.get('tva_rate'));
    }], ["tva", function (m) {
      return m.get('ttc_readonly') - m.get('ht');
    }]],
    ttc_readonly: [["ht", function (m) {
      return m.get('ttc_readonly') - m.get('tva');
    }], ["tva_rate", function (m) {
      return null;
    }]]
  }
};
Object.setPrototypeOf(TTCModelFieldsLinker, AbstractFieldsLinker);

/***/ }),

/***/ "./src/expense/expense.js":
/*!********************************!*\
  !*** ./src/expense/expense.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var _components_App_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/App.js */ "./src/expense/components/App.js");
/* harmony import */ var _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/Facade.js */ "./src/expense/components/Facade.js");
/* harmony import */ var _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/ToolbarApp.js */ "./src/expense/components/ToolbarApp.js");
/* harmony import */ var _common_components_ExpenseTypeService_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/components/ExpenseTypeService.js */ "./src/common/components/ExpenseTypeService.js");
/* harmony import */ var _common_components_AttachmentUploadService_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../common/components/AttachmentUploadService.js */ "./src/common/components/AttachmentUploadService.js");
/* harmony import */ var _common_components_StatusHistoryApp_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/components/StatusHistoryApp.js */ "./src/common/components/StatusHistoryApp.js");
/* harmony import */ var _common_components_PreviewService__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/components/PreviewService */ "./src/common/components/PreviewService.js");










jquery__WEBPACK_IMPORTED_MODULE_0___default()(function () {
  (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_2__.applicationStartup)(AppOption, _components_App_js__WEBPACK_IMPORTED_MODULE_3__.default, _components_Facade_js__WEBPACK_IMPORTED_MODULE_4__.default, {
    actionsApp: _components_ToolbarApp_js__WEBPACK_IMPORTED_MODULE_5__.default,
    statusHistoryApp: _common_components_StatusHistoryApp_js__WEBPACK_IMPORTED_MODULE_8__.default,
    customServices: [_common_components_ExpenseTypeService_js__WEBPACK_IMPORTED_MODULE_6__.default, _common_components_AttachmentUploadService_js__WEBPACK_IMPORTED_MODULE_7__.default, _common_components_PreviewService__WEBPACK_IMPORTED_MODULE_9__.default]
  });
});

/***/ }),

/***/ "./src/expense/models/BookMarkCollection.js":
/*!**************************************************!*\
  !*** ./src/expense/models/BookMarkCollection.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ExpenseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseModel.js */ "./src/expense/models/ExpenseModel.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");


var BookMarkCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  url: "/api/v1/bookmarks",
  model: _ExpenseModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  addBookMark: function addBookMark(model) {
    var _ref;

    var keys = ['ht', 'tva', 'km', 'start', 'end', 'description', 'type_id', 'category', 'customer_id', 'project_id', 'business_id', 'supplier_id', 'invoice_number'];

    var attributes = (_ref = _).pick.apply(_ref, [model.attributes].concat(keys));

    this.create(attributes, {
      wait: true
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BookMarkCollection);

/***/ }),

/***/ "./src/expense/models/ExpenseBaseModel.js":
/*!************************************************!*\
  !*** ./src/expense/models/ExpenseBaseModel.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/BaseModel.js */ "./src/base/models/BaseModel.js");
/* harmony import */ var _base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../base/models/DuplicableMixin.js */ "./src/base/models/DuplicableMixin.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");





var ExpenseBaseModel = _base_models_BaseModel_js__WEBPACK_IMPORTED_MODULE_2__.default.extend(_base_models_DuplicableMixin_js__WEBPACK_IMPORTED_MODULE_3__.default).extend({
  /*
   * BaseModel for expenses, provides tools to access main options
   */
  getType: function getType() {
    /*
     * return the ExpenseType object associated to the current model
     */
    return this.config.request('get:ExpenseType', this.get('type_id'));
  },
  getTypeLabel: function getTypeLabel() {
    /*
     * Return the Label of the current type
     */
    var current_type = this.getType();

    if (current_type === undefined) {
      return "";
    } else {
      return current_type.get('label');
    }
  },
  getCategoryLabel: function getCategoryLabel() {
    var _this = this;

    /*
     * Return the Label of the current type
     */
    var categories = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config').request('get:options', 'categories');

    var category = _.find(categories, function (x) {
      return x.value == _this.get('category');
    });

    if (category) {
      return category.label;
    } else {
      return 'INCONNU'; // should not happen
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseBaseModel);

/***/ }),

/***/ "./src/expense/models/ExpenseCollection.js":
/*!*************************************************!*\
  !*** ./src/expense/models/ExpenseCollection.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ExpenseModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseModel.js */ "./src/expense/models/ExpenseModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }





var ExpenseCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  /*
   *  Collection of expense lines
   *
   * Maintains a special state in this.aggregates.justified :
   *  - true when all lines have justified=true
   *  - false when all lines have justified=false
   *  - null else
   *
   * That attribute can be watched for change with aggregate:change:justified
   */
  model: _ExpenseModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize() {
    this.on('destroyed', this.channelCall);
    this.on('saved', this.channelCall);
    this.on('change:justified add remove reset', function () {
      this.updateAggregate('justified');
    }.bind(this));
    this.aggregates = {};
  },
  channelCall: function channelCall(model) {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    channel.trigger('changed:line');
  },
  updateAggregate: function updateAggregate(property) {
    var previousValue = this.aggregates[property];
    var newValue = this.computeAggregate(property);
    this.aggregates[property] = newValue;

    if (newValue !== previousValue) {
      this.trigger("aggregate:change:".concat(property), newValue);
    }
  },
  computeAggregate: function computeAggregate(property) {
    var totalCount = this.models.length;
    var matchingCount = this.models.filter(function (x) {
      return x.get(property);
    }).length;

    if (totalCount > 0) {
      if (totalCount === matchingCount) {
        return true;
      } else if (matchingCount === 0) {
        return false;
      } else {
        return null; // mixed
      }
    } else {
      return null;
    }
  },
  url: function url() {
    return AppOption['context_url'] + "/lines";
  },
  comparator: function comparator(a, b) {
    /*
     * Sort the collection and place
     *   - unjustified lines at the begining
     *   - special lines at the end
     */
    var res = 0;

    if (b.get('justified') != a.get('justified')) {
      // Unjustified goes first
      if (!b.get('justified')) {
        return 1;
      } else {
        res = -1;
      }
    } else if (a.isTelType() != b.isTelType()) {
      // Tel type goes last
      if (b.isTelType()) {
        res = -1;
      } else {
        res = 1;
      }
    } else {
      var acat = a.get('category');
      var bcat = b.get('category');

      if (acat < bcat) {
        res = -1;
      } else if (acat > bcat) {
        res = 1;
      } else {
        var adate = a.get('altdate');
        var bdate = a.get('altdate');

        if (adate < bdate) {
          res = -1;
        } else if (acat > bcat) {
          res = 1;
        }
      }
    }

    return res;
  },
  total_ht: function total_ht(category) {
    /*
     * Return the total value
     */
    var result = 0;
    this.each(function (model) {
      if (category != undefined) {
        if (model.get('category') != category) {
          return;
        }
      }

      result += (0,_math_js__WEBPACK_IMPORTED_MODULE_3__.round)(model.getHT());
    });
    return result;
  },
  total_tva: function total_tva(category) {
    /*
     * Return the total value
     */
    var result = 0;
    this.each(function (model) {
      if (category != undefined) {
        if (model.get('category') != category) {
          return;
        }
      }

      result += (0,_math_js__WEBPACK_IMPORTED_MODULE_3__.round)(model.getTva());
    });
    return result;
  },
  total: function total(category) {
    /*
     * Return the total value
     */
    var result = 0;
    this.each(function (model) {
      if (category != undefined) {
        if (model.get('category') != category) {
          return;
        }
      }

      result += (0,_math_js__WEBPACK_IMPORTED_MODULE_3__.round)(model.total());
    });
    return result;
  },

  /**  Gathers the IDs of the files that are linked a line or more
   *
   * @returns {Set[int]} file ids that are linked to at least one expense
   *   line.
   */
  collectLinkedFileIds: function collectLinkedFileIds() {
    // FIXME : et sur delete d'expense ?
    var fileIds = new Set();
    this.each(function (model) {
      var _iterator = _createForOfIteratorHelper(model.get('files')),
          _step;

      try {
        for (_iterator.s(); !(_step = _iterator.n()).done;) {
          var id = _step.value;
          // Force cast to int
          fileIds.add(parseInt(id, 10));
        }
      } catch (err) {
        _iterator.e(err);
      } finally {
        _iterator.f();
      }
    });
    return fileIds;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseCollection);

/***/ }),

/***/ "./src/expense/models/ExpenseKmCollection.js":
/*!***************************************************!*\
  !*** ./src/expense/models/ExpenseKmCollection.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ExpenseKmModel_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseKmModel.js */ "./src/expense/models/ExpenseKmModel.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");




var ExpenseKmCollection = backbone__WEBPACK_IMPORTED_MODULE_0___default().Collection.extend({
  /*
   * Collection for expenses related to km fees
   */
  model: _ExpenseKmModel_js__WEBPACK_IMPORTED_MODULE_1__.default,
  initialize: function initialize() {
    this.on('destroyed', this.channelCall);
    this.on('saved', this.channelCall);
  },
  channelCall: function channelCall() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
    channel.trigger('changed:kmline');
  },
  url: function url() {
    return AppOption['context_url'] + '/kmlines';
  },
  total_km: function total_km(category) {
    /*
     * Return the total value
     */
    var result = 0;
    this.each(function (model) {
      if (category != undefined) {
        if (model.get('category') != category) {
          return;
        }
      }

      result += model.getKm();
    });
    return result;
  },
  total_tva: function total_tva(category) {
    return 0;
  },
  total_ht: function total_ht(category) {
    return this.total(category);
  },
  total: function total(category) {
    var result = 0;
    this.each(function (model) {
      if (category != undefined) {
        if (model.get('category') != category) {
          return;
        }
      }

      result += (0,_math_js__WEBPACK_IMPORTED_MODULE_3__.round)(model.total());
    });
    return result;
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmCollection);

/***/ }),

/***/ "./src/expense/models/ExpenseKmModel.js":
/*!**********************************************!*\
  !*** ./src/expense/models/ExpenseKmModel.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ExpenseBaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpenseBaseModel.js */ "./src/expense/models/ExpenseBaseModel.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);



var ExpenseKmModel = _ExpenseBaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  defaults: {
    type: 'km',
    category: null,
    ht: null,
    start: "",
    end: "",
    description: "",
    customer_id: null,
    project_id: null,
    business_id: null
  },
  initialize: function initialize(options) {
    if (options['altdate'] === undefined && options['date'] !== undefined) {
      this.set('altdate', (0,_date_js__WEBPACK_IMPORTED_MODULE_1__.formatPaymentDate)(options['date']));
    }

    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
  },
  validation: {
    type_id: {
      required: true,
      msg: "est requis"
    },
    date: {
      required: true,
      pattern: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
      msg: "est requise"
    },
    km: {
      required: true,
      pattern: "amount2"
    }
  },
  getIndice: function getIndice() {
    /*
     *  Return the indice used for compensation of km fees
     */
    var type = this.getType();

    if (type === undefined) {
      return 0;
    } else {
      return parseFloat(type.get('amount'));
    }
  },
  getHT: function getHT() {
    return parseFloat(this.get('ht'));
  },
  getTva: function getTva() {
    return 0;
  },
  total: function total() {
    return this.getHT();
  },
  getKm: function getKm() {
    return parseFloat(this.get('km'));
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmModel);

/***/ }),

/***/ "./src/expense/models/ExpenseModel.js":
/*!********************************************!*\
  !*** ./src/expense/models/ExpenseModel.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ExpenseBaseModel_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpenseBaseModel.js */ "./src/expense/models/ExpenseBaseModel.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _math__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../math */ "./src/math.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");




var ExpenseModel = _ExpenseBaseModel_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  defaults: {
    category: null,
    description: "",
    invoice_number: "",
    ht: null,
    tva: null,
    manual_ttc: null,
    tva_rate: null,
    // not synced, client-side use only
    ttc_readonly: null,
    // not synced, client-side use only
    customer_id: null,
    project_id: null,
    business_id: null,
    supplier_id: null,
    fill_mode: 'ht' // not synced, client-side use only

  },
  // Constructor dynamically add a altdate if missing
  // (altdate is used in views for jquery datepicker)
  initialize: function initialize(options) {
    if (options['altdate'] === undefined && options['date'] !== undefined) {
      this.set('altdate', (0,_date_js__WEBPACK_IMPORTED_MODULE_1__.formatPaymentDate)(options['date']));
    } // API does not return ttc_readonly, we compute it client side


    this.set('ttc_readonly', (0,_math__WEBPACK_IMPORTED_MODULE_3__.round)(this.get('ht') + this.get('tva')));
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('config');
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_2___default().channel('facade');
  },
  // Validation rules for our model's attributes
  validation: {
    type_id: {
      required: true,
      msg: "est requis"
    },
    date: {
      required: true,
      pattern: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/,
      msg: "est requise"
    },
    ht: {
      required: true,
      pattern: "amount2"
    },
    tva: {
      required: true,
      pattern: "amount2"
    },
    manual_ttc: {
      required: true,
      pattern: 'amount2'
    },
    ttc_readonly: {
      required: true,
      pattern: "amount2"
    },
    tva_rate: {
      required: false,
      pattern: 'amount2'
    }
  },
  total: function total() {
    if (this.hasTvaOnMargin()) {
      return parseFloat(this.get('manual_ttc'));
    } else {
      return this.getHT() + this.getTva();
    }
  },
  getTva: function getTva() {
    var result = parseFloat(this.get('tva'));
    return this.getType().computeAmount(result);
  },
  getHT: function getHT() {
    var result = parseFloat(this.get('ht'));
    return this.getType().computeAmount(result);
  },
  getSupplierLabel: function getSupplierLabel() {
    var supplier = this.facade.request('get:supplier', this.get('supplier_id'));

    if (supplier) {
      return supplier.get('label');
    } else {
      return '';
    }
  },
  isTelType: function isTelType() {
    var type = this.getType();

    if (type == undefined) {
      console.warn('Should not happen');
      return false;
    } else {
      return type.get('family') == 'tel';
    }
  },
  hasDeductibleTva: function hasDeductibleTva() {
    var type = this.getType();

    if (type == undefined) {
      return true;
    } else {
      return type.get('is_tva_deductible');
    }
  },
  hasTvaOnMargin: function hasTvaOnMargin() {
    var type = this.getType();

    if (type == undefined) {
      return false;
    } else {
      return type.get('tva_on_margin');
    }
  },
  requiresTtcInput: function requiresTtcInput() {
    return this.hasTvaOnMargin() || !this.hasDeductibleTva();
  },
  loadBookMark: function loadBookMark(bookmark) {
    var attributes = _.omit(bookmark.attributes, function (value, key) {
      if (_.indexOf(['id', 'cid'], key) > -1) {
        return true;
      } else if (_.isNull(value) || _.isUndefined(value)) {
        return true;
      }

      return false;
    });

    this.set(attributes);
    this.trigger('set:bookmark');
  },
  isFileLinked: function isFileLinked(file) {
    return this.get('files').includes(file.id);
  },
  linkToFile: function linkToFile(file) {
    var files = this.get('files');

    if (!this.isFileLinked(file)) {
      files.push(file.id);
    }

    this.set('files', files, {
      silent: true
    });
  },
  unlinkFromFile: function unlinkFromFile(file) {
    var files = this.get('files');

    if (this.isFileLinked(file)) {
      files.splice(files.indexOf(file.id), 1);
    }

    this.set('files', files, {
      silent: true
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseModel);

/***/ }),

/***/ "./src/expense/models/TotalModel.js":
/*!******************************************!*\
  !*** ./src/expense/models/TotalModel.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);

var TotalModel = backbone__WEBPACK_IMPORTED_MODULE_0___default().Model.extend({});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TotalModel);

/***/ }),

/***/ "./src/expense/views/BaseExpenseFormView.js":
/*!**************************************************!*\
  !*** ./src/expense/views/BaseExpenseFormView.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tools */ "./src/tools.js");
/* harmony import */ var _base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/behaviors/FormBehavior.js */ "./src/base/behaviors/FormBehavior.js");
/* harmony import */ var _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/Select2Widget.js */ "./src/widgets/Select2Widget.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _compute_FieldsLinkers__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../compute/FieldsLinkers */ "./src/expense/compute/FieldsLinkers.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");








var BaseExpenseFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  behaviors: [_base_behaviors_FormBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  template: __webpack_require__(/*! ./templates/ExpenseFormView.mustache */ "./src/expense/views/templates/ExpenseFormView.mustache"),
  regions: {
    'category': '.category',
    'date': '.date',
    'type_id': '.type_id',
    'description': '.description',
    'invoice_number': '.invoice_number',
    'fill_mode': '.fill_mode',
    'ht': '.ht',
    'manual_ttc': '.manual_ttc',
    'tva': '.tva',
    'ttc_readonly': '.ttc_readonly',
    'tva_rate': '.tva_rate',
    'business_link': '.business_link',
    'supplier_id': '.supplier_id',
    'files': '.files'
  },
  // Bubble up child view events
  //
  childViewTriggers: {
    'change': 'data:modified'
  },
  childViewEvents: {
    'finish': 'onChildChange'
  },
  onBeforeSync: tools__WEBPACK_IMPORTED_MODULE_0__.showLoader,
  onFormSubmitted: tools__WEBPACK_IMPORTED_MODULE_0__.hideLoader,
  initialize: function initialize() {
    // Common initialization.
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_5___default().channel('config');
    this.type_options = this.getTypeOptions();
    this.suppliers_options = channel.request('get:options', 'suppliers');
    console.log('this.type_options', this.getTypeOptions());
    this.today = channel.request('get:options', 'today'); // If we have no type (eg: new expense form), adopt the first option of
    // the select list.

    if (this.model.get('type_id') === undefined) {
      this.model.set('type_id', String(this.type_options[0].id));
    }
  },
  showFilesSelect: function showFilesSelect() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_5___default().channel('facade');
    var attachments = channel.request('get:collection', 'attachments');
    var view = new _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      title: "Justificatifs",
      options: attachments.asSelectOptions(),
      field_name: "files",
      multiple: true,
      value: this.model.get('files'),
      placeholder: "Choisir un ou plusieurs justificatifs déjà téléversés"
    });
    this.showChildView('files', view);
  },

  /** Render/Update the form fields related to amounts
   *
   * Optionaly, the fields to be rendered can be restricted via fields argument.
   * Other will remain untouched.
   *
   * @param fields String
   */
  renderAmountFields: function renderAmountFields(fields) {
    var show = function (areaName, view) {
      if (fields === undefined || fields.includes(areaName)) {
        this.showChildView(areaName, view);
      }
    }.bind(this);

    var htParams = {
      value: this.model.get('ht'),
      title: 'Montant HT',
      field_name: 'ht',
      addon: "€",
      required: true
    };
    var tvaParams = {
      value: this.model.get('tva'),
      title: 'Montant TVA',
      field_name: 'tva',
      addon: "€",
      required: true
    };
    var manual_ttcParams = {
      value: 0,
      title: 'Montant TTC',
      field_name: 'manual_ttc',
      addon: "€",
      required: true
    };
    var ttc_readonlyParams = {
      title: 'Montant TTC',
      addon: "€",
      field_name: "ttc_readonly",
      value: this.model.get('ttc_readonly')
    };

    if (this.model.hasTvaOnMargin()) {
      tvaParams.value = 0;
      htParams.value = 0;
      manual_ttcParams.value = this.model.get('manual_ttc');
    } else if (!this.model.hasDeductibleTva()) {
      manual_ttcParams.value = 0;
      tvaParams.value = 0;
      htParams.title = 'Montant TTC';
    } else {
      // regular mode (no TVA/margin neither telecom-like expense)
      if (this.model.get('fill_mode') === 'ht') {
        ttc_readonlyParams.editable = false;
        ttc_readonlyParams.description = "Le montant TTC est calculé";
        htParams.editable = true;
      } else {
        // fill_mode === 'ttc'
        ttc_readonlyParams.editable = true;
        htParams.description = "Le montant HT est calculé";
        htParams.editable = false;
      }
    }

    var view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(manual_ttcParams);
    show('manual_ttc', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(htParams);
    show('ht', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(tvaParams);
    show('tva', view);

    if (!this.model.requiresTtcInput()) {
      view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
        value: this.model.get('tva_rate'),
        title: 'Taux TVA',
        placeholder: "Taux de TVA en %",
        field_name: "tva_rate",
        addon: "%",
        description: "Le montant de TVA peut être calculé à partir de ce taux"
      });
      show('tva_rate', view);
      view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default(ttc_readonlyParams);
      show('ttc_readonly', view);
    } else {
      this.getRegion('ttc_readonly').reset();
      this.getRegion('tva_rate').reset();
    }
  },
  onRender: function onRender() {
    var view;
    view = new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      date: this.model.get('date'),
      title: "Date",
      field_name: "date",
      default_value: this.today,
      required: true
    });
    this.showChildView("date", view);
    view = new _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      value: this.model.get('supplier_id'),
      title: 'Fournisseur',
      field_name: 'supplier_id',
      options: this.suppliers_options,
      placeholder: 'Choisir un fournisseur'
    });
    this.showChildView('supplier_id', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      value: this.model.get('invoice_number'),
      title: 'Numéro de la facture',
      field_name: 'invoice_number'
    });
    this.showChildView('invoice_number', view);
    var previousType = this.model.get('type_id');
    view = new _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      value: this.model.get('type_id'),
      title: 'Type de dépense',
      field_name: 'type_id',
      options: this.type_options,
      id_key: 'id',
      required: true
    });
    this.showChildView('type_id', view); // Syncs model to allow proppper rendering of the form based on wether
    // we have TVA or not.

    this.triggerMethod('data:modified', 'type_id', view.getCurrentValues()[0]);

    if (previousType != view.getCurrentValues()[0]) {
      /* re-render to get correct hide/show of amount fields
       * Handle cases where we changed tab and default type
       * option of the new tab has different field presence requirements (tva/ht/ttc)
       * than previous tab's one.
       * A bit hackish
       */
      this.render();
      return;
    }

    this.renderAmountFields();
    this.showFilesSelect();
  },
  onChildChange: function onChildChange(field_name, value) {
    this.triggerMethod('data:modified', field_name, value);

    if (field_name == 'type_id') {
      this.render();
    } else if (field_name == 'files') {
      this.triggerMethod('files:changed');
      /* Refresh totally the files widget, to workaround two select2 issues:
      * - stores in certain circonstances empty ids (unexplained)
      * - let the dropdown open but at the wrong place (when the popin gets
      * resized when preview pane is added/removed)
      */

      this.showFilesSelect();
    }

    if (field_name === 'fill_mode') {
      this.renderAmountFields();
      this.showFillModeChoice();
    }

    this.onAmountsChange(field_name, value);
  },
  onAmountsChange: function onAmountsChange(field_name, value) {
    var linker;

    if (this.model.get('fill_mode') === 'ht') {
      linker = _compute_FieldsLinkers__WEBPACK_IMPORTED_MODULE_6__.HtModelFieldsLinker;
    } else {
      linker = _compute_FieldsLinkers__WEBPACK_IMPORTED_MODULE_6__.TTCModelFieldsLinker;
    }

    var prevValue = this.model.get(field_name);

    if (prevValue !== value && value !== '') {
      this.triggerMethod('data:modified', field_name, value);
      var changedFields = linker.reactToFieldChange(field_name, this.model);
      this.renderAmountFields(changedFields);
    }
  },
  afterSerializeForm: function afterSerializeForm(datas) {
    var modifiedDatas = _.clone(datas);
    /* We also want the category to be pushed to server,
     * even if not present as form field
     */


    modifiedDatas['category'] = this.model.get('category'); // Hack to allow setting those fields to null.
    // Otherwise $.serializeForm skips <select> with no value selected

    modifiedDatas['customer_id'] = this.model.get('customer_id');
    modifiedDatas['project_id'] = this.model.get('project_id');
    modifiedDatas['business_id'] = this.model.get('business_id');
    return modifiedDatas;
  },
  templateContext: function templateContext() {
    var hasTvaOnMargin = this.model.hasTvaOnMargin();
    return {
      button_title: this.getOption('buttonTitle'),
      add: this.getOption('add'),
      hidden_ht: hasTvaOnMargin,
      hidden_tva: this.model.requiresTtcInput(),
      hidden_manual_ttc: !hasTvaOnMargin
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BaseExpenseFormView);

/***/ }),

/***/ "./src/expense/views/BaseExpenseView.js":
/*!**********************************************!*\
  !*** ./src/expense/views/BaseExpenseView.js ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_views_BusinessLinkView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../common/views/BusinessLinkView.js */ "./src/common/views/BusinessLinkView.js");


/** Abstract Common base for ExpenseView and ExpenseKmView
 */

var BaseExpenseView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  modelEvents: {
    'sync': 'render'
  },
  regions: {
    businessLink: {
      el: '.business-link'
    }
  },
  isAchat: function isAchat() {
    return this.model.get('category') == 2;
  },
  onRender: function onRender() {
    if (this.isAchat()) {
      var view = new _common_views_BusinessLinkView_js__WEBPACK_IMPORTED_MODULE_0__.default({
        customer_label: this.model.get('customer_label'),
        project_label: this.model.get('project_label'),
        business_label: this.model.get('business_label')
      });
      this.showChildView('businessLink', view);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BaseExpenseView);

/***/ }),

/***/ "./src/expense/views/BookMarkCollectionView.js":
/*!*****************************************************!*\
  !*** ./src/expense/views/BookMarkCollectionView.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");


var BookMarkView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'tr',
  className: 'bookmark-line',
  template: __webpack_require__(/*! ./templates/BookMarkView.mustache */ "./src/expense/views/templates/BookMarkView.mustache"),
  ui: {
    delete_btn: '.delete',
    insert_btn: '.insert',
    clickable_cells: '.clickable-cell'
  },
  triggers: {
    'click @ui.delete_btn': 'bookmark:delete',
    'click @ui.insert_btn': 'bookmark:insert',
    'click @ui.clickable_cells': 'bookmark:insert'
  },
  templateContext: function templateContext() {
    var typelabel = this.model.getTypeLabel();
    return {
      ht: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('ht')),
      tva: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('tva')),
      typelabel: typelabel,
      insertAttrs: "title='Sélectionner ce favori et l’ajouter' " + "aria-label='Sélectionner ce favori et l’ajouter'"
    };
  }
});
var BookMarkCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  className: "modal_content_layout",
  template: __webpack_require__(/*! ./templates/BookMarkCollectionView.mustache */ "./src/expense/views/templates/BookMarkCollectionView.mustache"),
  childViewContainer: 'tbody',
  childView: BookMarkView,
  childViewTriggers: {
    'bookmark:delete': 'bookmark:delete',
    'bookmark:insert': 'bookmark:insert'
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (BookMarkCollectionView);

/***/ }),

/***/ "./src/expense/views/ExpenseCollectionView.js":
/*!****************************************************!*\
  !*** ./src/expense/views/ExpenseCollectionView.js ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ExpenseView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpenseView.js */ "./src/expense/views/ExpenseView.js");
/* harmony import */ var _ExpenseEmptyView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseEmptyView.js */ "./src/expense/views/ExpenseEmptyView.js");



var ExpenseCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().CollectionView.extend({
  tagName: 'tbody',
  // Bubble up child view events
  childViewTriggers: {
    'edit': 'line:edit',
    'delete': 'line:delete',
    'bookmark': 'bookmark:add',
    'duplicate': 'line:duplicate'
  },
  childView: _ExpenseView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  emptyView: _ExpenseEmptyView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  isAchatView: function isAchatView() {
    return this.getOption('category').value == 2;
  },
  emptyViewOptions: function emptyViewOptions() {
    return {
      colspan: this.isAchatView() ? 8 : 7,
      edit: this.getOption('edit')
    };
  },
  childViewOptions: function childViewOptions() {
    return {
      edit: this.getOption('edit'),
      can_validate_attachments: this.getOption('can_validate_attachments')
    };
  },
  viewFilter: function viewFilter(view, index, children) {
    if (view.model.get('category') == this.getOption('category').value) {
      return true;
    } else {
      return false;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseCollectionView);

/***/ }),

/***/ "./src/expense/views/ExpenseDuplicateFormView.js":
/*!*******************************************************!*\
  !*** ./src/expense/views/ExpenseDuplicateFormView.js ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/behaviors/ModalBehavior.js */ "./src/base/behaviors/ModalBehavior.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../tools.js */ "./src/tools.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");






var ExpenseDuplicateFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  behaviors: [_base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default],
  id: 'expense-duplicate-form',
  template: __webpack_require__(/*! ./templates/ExpenseDuplicateFormView.mustache */ "./src/expense/views/templates/ExpenseDuplicateFormView.mustache"),
  regions: {
    'select': '.select'
  },
  ui: {
    cancel_btn: 'button[type=reset]',
    form: 'form'
  },
  events: {
    'submit @ui.form': 'onSubmit',
    'click @ui.cancel_btn': 'onCancelClick'
  },
  initialize: function initialize() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.options = channel.request('get:options', 'expenses');
  },
  onCancelClick: function onCancelClick() {
    this.triggerMethod('modal:close');
  },
  templateContext: function templateContext() {
    var ht = this.model.getHT();
    var tva = this.model.getTva();
    var ttc = this.model.total();
    var is_km_fee = this.model.get('type') == 'km';
    return {
      ht: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(ht),
      tva: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(tva),
      ttc: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(ttc),
      is_km_fee: is_km_fee
    };
  },
  onRender: function onRender() {
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      options: this.options,
      title: 'Note de dépenses vers laquelle dupliquer',
      id_key: 'id',
      field_name: 'sheet_id'
    });
    this.showChildView('select', view);
  },
  onSubmit: function onSubmit(event) {
    var _this = this;

    event.preventDefault();
    var datas = (0,_tools_js__WEBPACK_IMPORTED_MODULE_3__.serializeForm)(this.getUI('form'));
    var sheet_id = this.model.get('sheet_id');
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    var request = this.model.duplicate(datas, sheet_id == datas.sheet_id);
    request.done(function () {
      var eventName = 'changed:';

      if (_this.model.get('type') == 'km') {
        eventName += 'km';
      }

      eventName += 'line';
      facade.trigger(eventName);

      _this.triggerMethod('modal:close');
    });
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseDuplicateFormView);

/***/ }),

/***/ "./src/expense/views/ExpenseEmptyView.js":
/*!***********************************************!*\
  !*** ./src/expense/views/ExpenseEmptyView.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_0__);

var ExpenseEmptyView = backbone_marionette__WEBPACK_IMPORTED_MODULE_0___default().View.extend({
  tagName: 'tr',
  template: __webpack_require__(/*! ./templates/ExpenseEmptyView.mustache */ "./src/expense/views/templates/ExpenseEmptyView.mustache"),
  templateContext: function templateContext() {
    var colspan = this.getOption('colspan');

    if (this.getOption('edit')) {
      colspan += 1;
    }

    return {
      colspan: colspan
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseEmptyView);

/***/ }),

/***/ "./src/expense/views/ExpenseFileCollectionView.js":
/*!********************************************************!*\
  !*** ./src/expense/views/ExpenseFileCollectionView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");
/* harmony import */ var underscore__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(underscore__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ExpenseFileItemView__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ExpenseFileItemView */ "./src/expense/views/ExpenseFileItemView.js");





var template = __webpack_require__(/*! ./templates/ExpenseFileCollectionView.mustache */ "./src/expense/views/templates/ExpenseFileCollectionView.mustache");

var Emptyview = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  tagName: 'tr',
  template: underscore__WEBPACK_IMPORTED_MODULE_0___default().template('<td colspan=10 class=col_text><em>Aucun justificatif non lié à une dépense.</em></td>')
});
var ExpenseFileCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().CollectionView.extend({
  tagName: 'tbody',
  template: template,
  childView: _ExpenseFileItemView__WEBPACK_IMPORTED_MODULE_2__.default,
  emptyView: Emptyview,
  childViewTriggers: {
    'file:delete': 'file:delete',
    'file:link_to_expenseline': 'file:link_to_expenseline',
    'file:new_expense': 'file:new_expense'
  },
  _onCollectionUpdate: function _onCollectionUpdate() {
    // Quite hackish : bypass CollectionView optimizations on fine-grained
    // child rendering : re-render the whole thing, allowing the files
    // counter displayed by template to update.
    this.render();
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.listenTo(this.facade, 'linkedFilesChanged', function () {
      this.updateLinkedFiles();
      this.render(); // re-apply filters
    }, this);
    this.updateLinkedFiles(); // initialization
  },
  childViewOptions: function childViewOptions() {
    return {
      edit: this.getOption('edit'),
      is_achat: this.getOption('is_achat'),
      can_validate_attachments: this.getOption('can_validate_attachments')
    };
  },
  viewFilter: function viewFilter(view, index, children) {
    return !this.linkedFileIds.has(view.model.id);
  },
  updateLinkedFiles: function updateLinkedFiles() {
    this.linkedFileIds = this.facade.request('get:linkedFiles');
  },
  getItemsCount: function getItemsCount() {
    var _this = this;

    // I don't see any better way than filtering twice to count…
    return this.children.filter(function (x) {
      return _this.viewFilter(x);
    }).length;
  },
  templateContext: function templateContext() {
    var items_count = this.getItemsCount();
    return {
      items_count: items_count,
      pluralize: items_count > 1 ? 's' : ''
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFileCollectionView);

/***/ }),

/***/ "./src/expense/views/ExpenseFileItemView.js":
/*!**************************************************!*\
  !*** ./src/expense/views/ExpenseFileItemView.js ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/ButtonWidget.js */ "./src/widgets/ButtonWidget.js");
/* harmony import */ var _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../base/models/ButtonModel.js */ "./src/base/models/ButtonModel.js");





var template = __webpack_require__(/*! ./templates/ExpenseFileItemView.mustache */ "./src/expense/views/templates/ExpenseFileItemView.mustache");

var ExpenseFileItemView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  regions: {
    delete_btn: '.delete-btn',
    link_btn: '.link-btn',
    new_expense_btn: '.new-expense-btn'
  },
  ui: {
    link: 'a'
  },
  childViewEvents: {
    'action:clicked': 'onButtonClicked'
  },
  events: {
    'click @ui.link': 'onLinkClicked'
  },
  tagName: 'tr',
  template: template,
  onButtonClicked: function onButtonClicked(action) {
    this.trigger("file:".concat(action), this.model);
  },
  onLinkClicked: function onLinkClicked(event) {
    if (this.config.request('is:previewable', this.model)) {
      this.facade.trigger('show:preview', this.model);
      event.preventDefault();
    } else {
      // let the link act normaly
      return true;
    }
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
  },
  onRender: function onRender() {
    if (this.getOption('edit')) {
      this.showChildView('delete_btn', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
        model: new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
          action: 'delete',
          css: 'negative icon only delete',
          icon: 'trash-alt',
          title: 'Supprimer le justificatif'
        })
      }));
      var modelLabel = this.model.get("label");

      if (this.facade.request('has:expenseLines')) {
        this.showChildView('link_btn', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
          model: new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
            action: 'link_to_expenseline',
            css: 'icon only',
            icon: 'paperclip',
            title: "Lier le justificatif ".concat(modelLabel, " \xE0 une d\xE9pense")
          })
        }));
      }

      this.showChildView('new_expense_btn', new _widgets_ButtonWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
        model: new _base_models_ButtonModel_js__WEBPACK_IMPORTED_MODULE_2__.default({
          action: 'new_expense',
          css: 'btn-primary icon only',
          icon: 'plus',
          title: "Ajouter une d\xE9pense li\xE9e au justificatif ".concat(modelLabel)
        })
      }));
    }
  },
  templateContext: function templateContext() {
    return {
      edit: this.getOption('edit'),
      openLink: "/files/".concat(this.model.get("id"), "?action=download"),
      isPreviewable: this.config.request('is:previewable', this.model),
      is_achat: this.getOption('is_achat'),
      can_validate_attachments: this.getOption('can_validate_attachments')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFileItemView);

/***/ }),

/***/ "./src/expense/views/ExpenseFormPopupView.js":
/*!***************************************************!*\
  !*** ./src/expense/views/ExpenseFormPopupView.js ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/behaviors/ModalBehavior.js */ "./src/base/behaviors/ModalBehavior.js");
/* harmony import */ var _base_behaviors_TabsBehavior_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../base/behaviors/TabsBehavior.js */ "./src/base/behaviors/TabsBehavior.js");
/* harmony import */ var _TelExpenseFormView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TelExpenseFormView.js */ "./src/expense/views/TelExpenseFormView.js");
/* harmony import */ var _RegularExpenseFormView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./RegularExpenseFormView.js */ "./src/expense/views/RegularExpenseFormView.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _BookMarkCollectionView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./BookMarkCollectionView.js */ "./src/expense/views/BookMarkCollectionView.js");
/* harmony import */ var _ExpenseFormPreviewWrapperView__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ExpenseFormPreviewWrapperView */ "./src/expense/views/ExpenseFormPreviewWrapperView.js");








var ExpenseFormPopupView = backbone_marionette__WEBPACK_IMPORTED_MODULE_7___default().View.extend({
  behaviors: [_base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default, {
    behaviorClass: _base_behaviors_TabsBehavior_js__WEBPACK_IMPORTED_MODULE_1__.default,
    tabPanes: [{
      region: 'main',
      id: 'mainform-container'
    }, {
      region: 'tel',
      id: 'telform-container'
    }, {
      region: 'bookmark',
      id: 'bookmark-container'
    }]
  }],
  template: __webpack_require__(/*! ./templates/ExpenseFormPopupView.mustache */ "./src/expense/views/templates/ExpenseFormPopupView.mustache"),
  id: "expense-form-popup-modal",
  regions: {
    main: '#mainform-container',
    tel: '#telform-container',
    bookmark: '#bookmark-container'
  },
  ui: {
    main_tab: 'ul.nav-tabs li.main a',
    tel_tab: "ul.nav-tabs li.tel a",

    /* override ModalBehavior.ui.modalbody selector that has no match otherwise,
    because its .modal_content_layout is outside my own template (sub-sub-template)
    */
    modalbody: '.tab-content'
  },
  childViewEvents: {
    'bookmark:insert': 'onBookMarkInsert',
    'success:sync': 'onSuccessSync'
  },
  // Here we bind the child FormBehavior with our ModalBehavior
  // Like it's done in the ModalFormBehavior
  childViewTriggers: {
    'cancel:form': 'modal:close',
    'bookmark:delete': 'bookmark:delete',
    'preview:displayStatusChange': 'preview:displayStatusChange'
  },
  modelEvents: {
    'set:bookmark': 'refreshForm'
  },
  initialize: function initialize() {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_4___default().channel('facade');
    this.bookmarks = facade.request('get:bookmarks');
    this.add = this.getOption('add');
    this.tel = this.model.isTelType();
  },
  onTabBeforeSelect: function onTabBeforeSelect(region) {
    var view;

    switch (region._name) {
      case 'main':
        view = new _ExpenseFormPreviewWrapperView__WEBPACK_IMPORTED_MODULE_6__.default({
          formView: new _RegularExpenseFormView_js__WEBPACK_IMPORTED_MODULE_3__.default(this.viewParams),
          model: this.viewParams.model
        });
        break;

      case 'tel':
        view = new _ExpenseFormPreviewWrapperView__WEBPACK_IMPORTED_MODULE_6__.default({
          formView: new _TelExpenseFormView_js__WEBPACK_IMPORTED_MODULE_2__.default(this.viewParams),
          model: this.viewParams.model
        });
        break;

      case 'bookmark':
        view = new _BookMarkCollectionView_js__WEBPACK_IMPORTED_MODULE_5__.default({
          collection: this.bookmarks
        });
        break;
    }

    this.showChildView(region._name, view);
  },
  onTabBeforeDeselect: function onTabBeforeDeselect(region) {
    region.empty();
  },
  onSuccessSync: function onSuccessSync() {
    if (this.add) {
      this.triggerMethod('modal:notifySuccess');
    } else {
      this.triggerMethod('modal:close');
    }
  },
  onModalAfterNotifySuccess: function onModalAfterNotifySuccess() {
    this.triggerMethod('line:add', this, {
      category: this.model.get('category')
    });
  },
  onModalBeforeClose: function onModalBeforeClose() {
    this.model.rollback();
  },
  refreshForm: function refreshForm() {
    var area, view;
    var viewParams = {
      model: this.model,
      destCollection: this.getOption('destCollection'),
      title: this.getOption('title'),
      buttonTitle: this.getOption('buttonTitle'),
      add: this.add
    };
    this.viewParams = viewParams;
    var activeTab = this.tel ? 'tel_tab' : 'main_tab';
    var activeRegion = this.getRegion(activeTab.split('_')[0]);
    this.onTabBeforeSelect(activeRegion);
    this.getUI(activeTab).tab('show');
  },
  onBookMarkInsert: function onBookMarkInsert(childView) {
    this.model.loadBookMark(childView.model);
  },
  templateContext: function templateContext() {
    /*
     * Form can be add form : show all tabs
     * Form can be tel form : show only the tel tab
     */
    var category_is_general = this.model.get('category') == "1";
    return {
      title: this.getOption('title'),
      add: this.add,
      allow_tel_tab: category_is_general,
      show_tel_tab: this.tel,
      show_tel: this.add || this.tel,
      show_bookmarks: this.add && this.bookmarks.length > 0,
      show_main: this.add || !this.tel
    };
  },
  onRender: function onRender() {
    this.refreshForm();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFormPopupView);

/***/ }),

/***/ "./src/expense/views/ExpenseFormPopupViewWithFile.js":
/*!***********************************************************!*\
  !*** ./src/expense/views/ExpenseFormPopupViewWithFile.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ExpenseFormPopupView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseFormPopupView.js */ "./src/expense/views/ExpenseFormPopupView.js");


var ExpenseFormPopupViewWithFile = _ExpenseFormPopupView_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  getNextFile: function getNextFile() {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    var linkedFilesIds = facade.request('get:linkedFiles');
    var orphanFiles = facade.request('get:collection', 'attachments').filter(function (file) {
      return !linkedFilesIds.has(file.id);
    });
    return orphanFiles[0] || null;
  },
  onModalAfterNotifySuccess: function onModalAfterNotifySuccess() {
    var nextFile = this.getNextFile();

    if (nextFile) {
      this.triggerMethod('line:add', this, {
        category: this.model.get('category'),
        files: [nextFile.id]
      }, true);
    } else {
      this.triggerMethod('modal:close');
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFormPopupViewWithFile);

/***/ }),

/***/ "./src/expense/views/ExpenseFormPreviewWrapperView.js":
/*!************************************************************!*\
  !*** ./src/expense/views/ExpenseFormPreviewWrapperView.js ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var tools__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tools */ "./src/tools.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_components_NodeFileViewerFactory__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/components/NodeFileViewerFactory */ "./src/common/components/NodeFileViewerFactory.js");
/* harmony import */ var _widgets_LoadingWidget__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/LoadingWidget */ "./src/widgets/LoadingWidget.js");





var ExpenseFormPreviewWrapperView = backbone_marionette__WEBPACK_IMPORTED_MODULE_4___default().View.extend({
  template: __webpack_require__(/*! ./templates/ExpenseFormPreviewWrapperView.mustache */ "./src/expense/views/templates/ExpenseFormPreviewWrapperView.mustache"),
  attributes: {
    "class": "layout flex two_cols pdf_viewer"
  },
  regions: {
    'form': {
      el: '.form-component',
      replaceElement: true
    },
    'preview': {
      el: '.preview',
      replaceElement: true
    },
    'loader': {
      el: '.loader',
      replaceElement: true
    }
  },
  // Bubble up child view events
  //
  childViewTriggers: {
    'cancel:form': 'cancel:form',
    'success:sync': 'success:sync'
  },
  childViewEvents: {
    'loader:start': 'showLoader',
    'loader:stop': 'hideLoader',
    'files:changed': 'onFilesChanged'
  },
  onBeforeSync: tools__WEBPACK_IMPORTED_MODULE_0__.showLoader,
  onFormSubmitted: tools__WEBPACK_IMPORTED_MODULE_0__.hideLoader,
  initialize: function initialize() {
    this.formView = this.getOption('formView');
  },
  shouldShowPreview: function shouldShowPreview() {
    var file_ids = this.model.get('files') || [];
    var config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');

    if (file_ids.length === 1 && file_ids[0] !== "") {
      // file_ids[0] mysteriously happens to be empty string…
      var files = facade.request('get:collection', 'attachments');
      return config.request('is:previewable', files.get(file_ids[0]));
    } else {
      return false;
    }
  },
  showPreview: function showPreview() {
    var view;
    var files = this.model.get('files');

    if (files && files.length > 0) {
      var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
      var attachments = channel.request('get:collection', 'attachments');
      var file = attachments.get(files[0]);
      view = _common_components_NodeFileViewerFactory__WEBPACK_IMPORTED_MODULE_2__.default.getViewer(file, {
        title: "Justificatifs",
        footerText: "Pour les PDF originaux, le copier-coller est possible."
      });
    }

    if (view) {
      this.showChildView('preview', view);
    }
  },
  onRender: function onRender() {
    this.showChildView('form', this.formView);

    if (this.shouldShowPreview()) {
      this.showPreview();
    }
  },
  templateContext: function templateContext() {
    return {
      should_show_preview: this.shouldShowPreview()
    };
  },
  onFilesChanged: function onFilesChanged() {
    // trigger modal resize if requide
    this.triggerMethod('preview:displayStatusChange', this.shouldShowPreview()); // refresh preview

    if (this.shouldShowPreview()) {
      this.showPreview();
    } else {
      this.getRegion('preview').empty();
    }
  },
  showLoader: function showLoader() {
    var view = new _widgets_LoadingWidget__WEBPACK_IMPORTED_MODULE_3__.default();
    this.showChildView("loader", view);
  },
  hideLoader: function hideLoader() {
    this.getRegion('loader').empty();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFormPreviewWrapperView);

/***/ }),

/***/ "./src/expense/views/ExpenseKmCollectionView.js":
/*!******************************************************!*\
  !*** ./src/expense/views/ExpenseKmCollectionView.js ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ExpenseKmView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpenseKmView.js */ "./src/expense/views/ExpenseKmView.js");
/* harmony import */ var _ExpenseEmptyView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExpenseEmptyView.js */ "./src/expense/views/ExpenseEmptyView.js");



var ExpenseKmCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().CollectionView.extend({
  tagName: 'tbody',
  // Bubble up child view events
  childViewTriggers: {
    'edit': 'kmline:edit',
    'delete': 'kmline:delete',
    'duplicate': 'kmline:duplicate'
  },
  childView: _ExpenseKmView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  emptyView: _ExpenseEmptyView_js__WEBPACK_IMPORTED_MODULE_1__.default,
  emptyViewOptions: function emptyViewOptions() {
    return {
      colspan: 7,
      edit: this.getOption('edit')
    };
  },
  childViewOptions: function childViewOptions() {
    return {
      edit: this.getOption('edit')
    };
  },
  viewFilter: function viewFilter(view, index, children) {
    if (view.model.get('category') == this.getOption('category').value) {
      return true;
    } else {
      return false;
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmCollectionView);

/***/ }),

/***/ "./src/expense/views/ExpenseKmFormView.js":
/*!************************************************!*\
  !*** ./src/expense/views/ExpenseKmFormView.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/behaviors/ModalFormBehavior.js */ "./src/base/behaviors/ModalFormBehavior.js");
/* harmony import */ var _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/DateWidget.js */ "./src/widgets/DateWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/Select2Widget.js */ "./src/widgets/Select2Widget.js");
/* harmony import */ var _widgets_SelectBusinessWidget_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../widgets/SelectBusinessWidget.js */ "./src/widgets/SelectBusinessWidget.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _tools__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../tools */ "./src/tools.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");









var ExpenseKmFormView = backbone_marionette__WEBPACK_IMPORTED_MODULE_8___default().View.extend({
  behaviors: [_base_behaviors_ModalFormBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default],
  template: __webpack_require__(/*! ./templates/ExpenseKmFormView.mustache */ "./src/expense/views/templates/ExpenseKmFormView.mustache"),
  id: "expensekm-form-popup-modal",
  regions: {
    'category': '.category',
    'date': '.date',
    'type_id': '.type_id',
    'start': '.start',
    'end': '.end',
    'km': '.km',
    'description': '.description',
    'business_link': '.business_link'
  },
  ui: {
    modalbody: ".modal_content_layout"
  },
  // Bubble up child view events
  //
  childViewTriggers: {
    'change': 'data:modified',
    'finish': 'data:modified'
  },
  childViewEvents: {
    'finish': 'onChildChange'
  },
  onBeforeSync: _tools__WEBPACK_IMPORTED_MODULE_7__.showLoader,
  onFormSubmitted: _tools__WEBPACK_IMPORTED_MODULE_7__.hideLoader,
  initialize: function initialize() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_6___default().channel('config');
    this.type_options = channel.request('get:options', 'expensekm_types');
    this.today = channel.request('get:options', 'today');
    this.customers_url = channel.request('get:options', 'company_customers_url');
    this.projects_url = channel.request('get:options', 'company_projects_url');
    this.businesses_url = channel.request('get:options', 'company_businesses_url');
  },
  showCategorySelect: function showCategorySelect() {
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      options: [{
        'value': 1,
        'label': "Frais généraux"
      }, {
        'value': 2,
        'label': "Achats clients"
      }],
      title: "Catégorie",
      field_name: "category",
      value: this.model.get('category')
    });
    this.showChildView('category', view);
  },
  refreshForm: function refreshForm() {
    var view;

    if (!this.add) {
      this.showCategorySelect();
    }

    view = new _widgets_DateWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      date: this.model.get('date'),
      title: "Date",
      field_name: "date",
      current_year: true,
      default_value: this.today,
      required: true
    });
    this.showChildView("date", view);
    view = new _widgets_Select2Widget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      value: this.model.get('type_id'),
      title: 'Type de dépense',
      field_name: 'type_id',
      options: this.type_options,
      id_key: 'id',
      required: true
    });
    this.showChildView('type_id', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('start'),
      title: 'Point de départ',
      field_name: 'start',
      required: true
    });
    this.showChildView('start', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('end'),
      title: "Point d'arrivée",
      field_name: 'end',
      required: true
    });
    this.showChildView('end', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('km'),
      title: "Nombre de kilomètres",
      field_name: 'km',
      addon: "km",
      required: true
    });
    this.showChildView('km', view);
    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
      value: this.model.get('description'),
      title: 'Motif de déplacement',
      field_name: 'description'
    });
    this.showChildView('description', view);

    if (this.model.get('category') != 1) {
      view = new _widgets_SelectBusinessWidget_js__WEBPACK_IMPORTED_MODULE_5__.default({
        title: 'Rattacher la dépense à',
        customers_url: this.customers_url,
        projects_url: this.projects_url,
        businesses_url: this.businesses_url,
        customer_value: this.model.get('customer_id'),
        project_value: this.model.get('project_id'),
        business_value: this.model.get('business_id'),
        customer_label: this.model.get('customer_label'),
        project_label: this.model.get('project_label'),
        business_label: this.model.get('business_label'),
        required: false
      });
      this.showChildView('business_link', view);
    }
  },
  afterSerializeForm: function afterSerializeForm(datas) {
    var modifiedDatas = _.clone(datas);
    /* We also want the category to be pushed to server,
     * even if not present as form field
     */


    modifiedDatas['category'] = this.model.get('category'); // Hack to allow setting those fields to null.
    // Otherwise $.serializeForm skips <select> with no value selected

    modifiedDatas['customer_id'] = this.model.get('customer_id');
    modifiedDatas['project_id'] = this.model.get('project_id');
    modifiedDatas['business_id'] = this.model.get('business_id');
    return modifiedDatas;
  },
  templateContext: function templateContext() {
    return {
      title: this.getOption('title'),
      button_title: this.getOption('buttonTitle')
    };
  },
  onRender: function onRender() {
    this.refreshForm();
  },
  onSuccessSync: function onSuccessSync() {
    if (this.add) {
      var this_ = this;
      var modalbody = this.getUI('modalbody');
      modalbody.effect('highlight', {
        color: 'rgba(0,0,0,0)'
      }, 800, this_.refresh.bind(this));
      modalbody.addClass('action_feedback success');
    } else {
      this.triggerMethod('modal:close');
    }
  },
  onModalBeforeClose: function onModalBeforeClose() {
    this.model.rollback();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmFormView);

/***/ }),

/***/ "./src/expense/views/ExpenseKmTableView.js":
/*!*************************************************!*\
  !*** ./src/expense/views/ExpenseKmTableView.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ExpenseKmCollectionView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExpenseKmCollectionView.js */ "./src/expense/views/ExpenseKmCollectionView.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! math.js */ "./src/math.js");




var ExpenseKmTableView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  template: __webpack_require__(/*! ./templates/ExpenseKmTableView.mustache */ "./src/expense/views/templates/ExpenseKmTableView.mustache"),
  regions: {
    lines: {
      el: 'tbody',
      replaceElement: true
    }
  },
  ui: {
    add_btn: 'button.add'
  },
  events: {
    'click @ui.add_btn': 'onAdd'
  },
  childViewTriggers: {
    'kmline:edit': 'kmline:edit',
    'kmline:delete': 'kmline:delete',
    'kmline:duplicate': 'kmline:duplicate'
  },
  collectionEvents: {
    'change:category': 'render'
  },
  initialize: function initialize() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.totalmodel = channel.request('get:totalmodel');
    this.categoryId = this.getOption('category').value;
    this.listenTo(channel, 'change:kmlines_' + this.categoryId, this.render.bind(this));
  },
  isCollectionEmpty: function isCollectionEmpty() {
    var _this = this;

    return this.collection.find(function (x) {
      return x.get('category') == _this.categoryId;
    }) === undefined;
  },
  templateContext: function templateContext() {
    var is_achat = false;
    var is_empty = this.isCollectionEmpty();
    if (this.getOption('category').value == 2) is_achat = true;
    return {
      total_km: (0,math_js__WEBPACK_IMPORTED_MODULE_2__.round)(this.totalmodel.get('km_' + this.categoryId), 2) + "&nbsp;km",
      total_ttc: (0,math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.totalmodel.get('km_ttc_' + this.categoryId)),
      category: this.getOption('category'),
      edit: this.getOption('edit'),
      is_achat: is_achat,
      is_empty: is_empty,
      is_not_empty: !is_empty
    };
  },
  onAdd: function onAdd() {
    this.triggerMethod('kmline:add', this, {
      category: this.getOption('category').value
    });
  },
  onRender: function onRender() {
    var view = new _ExpenseKmCollectionView_js__WEBPACK_IMPORTED_MODULE_0__.default({
      collection: this.collection,
      category: this.getOption('category'),
      edit: this.getOption('edit')
    });
    this.showChildView('lines', view);
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmTableView);

/***/ }),

/***/ "./src/expense/views/ExpenseKmView.js":
/*!********************************************!*\
  !*** ./src/expense/views/ExpenseKmView.js ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./BaseExpenseView.js */ "./src/expense/views/BaseExpenseView.js");



var ExpenseKmView = _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_2__.default.extend({
  template: __webpack_require__(/*! ./templates/ExpenseKmView.mustache */ "./src/expense/views/templates/ExpenseKmView.mustache"),
  ui: {
    edit: 'button.edit',
    "delete": 'button.delete',
    duplicate: 'button.duplicate'
  },
  triggers: {
    'click @ui.edit': 'edit',
    'click @ui.delete': 'delete',
    'click @ui.duplicate': 'duplicate'
  },
  templateContext: function templateContext() {
    var total = this.model.total();
    var typelabel = this.model.getTypeLabel();
    return {
      altdate: (0,_date_js__WEBPACK_IMPORTED_MODULE_1__.formatPaymentDate)(this.model.get('date')),
      edit: this.getOption('edit'),
      customer: this.model.get('customer_label'),
      is_achat: this.isAchat(),
      typelabel: typelabel,
      total: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(total)
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseKmView);

/***/ }),

/***/ "./src/expense/views/ExpenseTableView.js":
/*!***********************************************!*\
  !*** ./src/expense/views/ExpenseTableView.js ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _widgets_DropZoneWidget__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/DropZoneWidget */ "./src/widgets/DropZoneWidget.js");
/* harmony import */ var _ExpenseCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ExpenseCollectionView.js */ "./src/expense/views/ExpenseCollectionView.js");
/* harmony import */ var _ExpenseFileCollectionView__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ExpenseFileCollectionView */ "./src/expense/views/ExpenseFileCollectionView.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");






var ExpenseTableView = backbone_marionette__WEBPACK_IMPORTED_MODULE_5___default().View.extend({
  template: __webpack_require__(/*! ./templates/ExpenseTableView.mustache */ "./src/expense/views/templates/ExpenseTableView.mustache"),
  regions: {
    lines: {
      el: 'tbody.lines',
      replaceElement: true
    },
    files: {
      el: 'tbody.files',
      replaceElement: true
    },
    dropzone: '.dropzone'
  },
  ui: {
    add_btn: 'button.add'
  },
  events: {
    'click @ui.add_btn': 'onAdd'
  },
  childViewTriggers: {
    'line:edit': 'line:edit',
    'line:delete': 'line:delete',
    'line:duplicate': 'line:duplicate',
    'bookmark:add': 'bookmark:add',
    'file:delete': 'attachment:delete',
    'file:link_to_expenseline': 'attachment:link_to_expenseline'
  },
  childViewEvents: {
    'file:new_expense': 'onAddWithAttachment'
  },
  collectionEvents: {
    'change:category': 'render'
  },
  initialize: function initialize() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');
    this.totalmodel = channel.request('get:totalmodel');
    this.categoryId = this.getOption('category').value;
    this.listenTo(channel, 'change:lines_' + this.categoryId, this.render.bind(this));
  },
  isCollectionEmpty: function isCollectionEmpty() {
    var _this = this;

    return this.collection.find(function (x) {
      return x.get('category') == _this.categoryId;
    }) === undefined;
  },
  includesTvaOnMargin: function includesTvaOnMargin() {
    var that = this;
    var result = this.collection.find(function (expense) {
      return expense.hasTvaOnMargin() && expense.get('category') == that.categoryId;
    });
    return result;
  },
  isAchat: function isAchat() {
    return this.getOption('category').value == 2;
  },
  canValidateAttachments: function canValidateAttachments() {
    return !_.isUndefined(backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config').request('get:actions').justify);
  },
  templateContext: function templateContext() {
    var is_empty = this.isCollectionEmpty();
    return {
      category: this.getOption('category'),
      edit: this.getOption('edit'),
      is_achat: this.isAchat(),
      includes_tva_on_margin: this.includesTvaOnMargin(),
      total_ht: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(this.totalmodel.get('ht_' + this.categoryId)),
      total_tva: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(this.totalmodel.get('tva_' + this.categoryId)),
      total_ttc: (0,_math_js__WEBPACK_IMPORTED_MODULE_4__.formatAmount)(this.totalmodel.get('ttc_' + this.categoryId)),
      is_empty: is_empty,
      is_not_empty: !is_empty,
      can_validate_attachments: this.canValidateAttachments()
    };
  },
  onAdd: function onAdd() {
    this.triggerMethod('line:add', this, {
      category: this.getOption('category').value
    });
  },
  onAddWithAttachment: function onAddWithAttachment(attachment) {
    this.triggerMethod('line:add', this, {
      category: this.getOption('category').value,
      files: [attachment.id]
    }, true);
  },
  onRender: function onRender() {
    var edit = this.getOption('edit');
    this.showChildView('lines', new _ExpenseCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.collection,
      category: this.getOption('category'),
      edit: this.getOption('edit'),
      can_validate_attachments: this.canValidateAttachments()
    }));
    this.showChildView('files', new _ExpenseFileCollectionView__WEBPACK_IMPORTED_MODULE_3__.default({
      collection: this.getOption('filesCollection'),
      is_achat: this.isAchat(),
      edit: edit,
      can_validate_attachments: this.canValidateAttachments()
    }));

    if (edit) {
      this.showChildView('dropzone', new _widgets_DropZoneWidget__WEBPACK_IMPORTED_MODULE_1__.default({}));
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseTableView);

/***/ }),

/***/ "./src/expense/views/ExpenseView.js":
/*!******************************************!*\
  !*** ./src/expense/views/ExpenseView.js ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");
/* harmony import */ var _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./BaseExpenseView.js */ "./src/expense/views/BaseExpenseView.js");
/* harmony import */ var _widgets_ToggleButtonWidget__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/ToggleButtonWidget */ "./src/widgets/ToggleButtonWidget.js");
/* harmony import */ var _common_models_ActionModel__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../common/models/ActionModel */ "./src/common/models/ActionModel.js");







var tel_template = __webpack_require__(/*! ./templates/ExpenseTelView.mustache */ "./src/expense/views/templates/ExpenseTelView.mustache");

var template = __webpack_require__(/*! ./templates/ExpenseView.mustache */ "./src/expense/views/templates/ExpenseView.mustache");

__webpack_require__(/*! jquery-ui/ui/effects/effect-highlight */ "./node_modules/jquery-ui/ui/effects/effect-highlight.js");

var ExpenseView = _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_3__.default.extend({
  ui: {
    edit: 'button.edit',
    "delete": 'button.delete',
    duplicate: 'button.duplicate',
    bookmark: 'button.bookmark'
  },
  regions: {
    justified: '.justified',
    viewer: '.viewer',
    businessLink: '.business-link'
  },
  triggers: {
    'click @ui.edit': 'edit',
    'click @ui.delete': 'delete',
    'click @ui.duplicate': 'duplicate',
    'click @ui.bookmark': 'bookmark'
  },
  childViewEvents: {
    'status:change': 'onStatusChange'
  },
  modelEvents: {
    'change:justified': 'render'
  },
  onStatusChange: function onStatusChange(value) {
    this.model.set('justified', value);
  },
  getTemplate: function getTemplate() {
    if (this.model.isTelType()) {
      return tel_template;
    } else {
      return template;
    }
  },
  highlightBookMark: function highlightBookMark() {
    this.getUI('bookmark').effect("highlight", {
      color: "#ceff99"
    }, "slow");
  },
  onRender: function onRender() {
    ExpenseView.__super__.onRender.apply(this);

    if (this.getOption('can_validate_attachments')) {
      var model = new _common_models_ActionModel__WEBPACK_IMPORTED_MODULE_5__.default({
        options: {
          name: "justified-line-".concat(this.model.get('id')),
          current_value: this.model.get('justified'),
          url: "".concat(AppOption.context_url, "/lines/").concat(this.model.id, "?action=justified_status"),
          buttons: [{
            value: false,
            icon: "clock",
            label: "En attente",
            title: "Le(s) justificatif(s) n'ont pas été acceptés",
            css: "btn"
          }, {
            value: true,
            icon: "check",
            label: "Accepté",
            title: "Le(s) justificatif(s) ont pas été acceptés",
            css: "btn"
          }]
        }
      });
      this.showChildView('justified', new _widgets_ToggleButtonWidget__WEBPACK_IMPORTED_MODULE_4__.default({
        model: model
      }));
    }
  },
  templateContext: function templateContext() {
    var total = this.model.total();
    var typelabel = this.model.getTypeLabel();
    return {
      altdate: (0,_date_js__WEBPACK_IMPORTED_MODULE_2__.formatPaymentDate)(this.model.get('date')),
      edit: this.getOption('edit'),
      is_achat: this.isAchat(),
      has_tva_on_margin: this.model.hasTvaOnMargin(),
      typelabel: typelabel,
      total: (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(total),
      ht_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ht')),
      tva_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('tva')),
      files: this.model.get('files').map(function (id) {
        return backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade').request('get:file', id).attributes;
      }),
      invoice_number: this.model.get('invoice_number'),
      supplier_label: this.model.getSupplierLabel(),
      invoice_number_and_supplier: this.model.get('supplier_id') && this.model.get('invoice_number'),
      can_validate_attachments: this.getOption('can_validate_attachments'),
      justified: this.model.get('justified')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseView);

/***/ }),

/***/ "./src/expense/views/FileLinkPopupView.js":
/*!************************************************!*\
  !*** ./src/expense/views/FileLinkPopupView.js ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../base/behaviors/ModalBehavior.js */ "./src/base/behaviors/ModalBehavior.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SelectableExpenseCollectionView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./SelectableExpenseCollectionView.js */ "./src/expense/views/SelectableExpenseCollectionView.js");




var ExpenseFormPopupView = backbone_marionette__WEBPACK_IMPORTED_MODULE_3___default().View.extend({
  behaviors: [_base_behaviors_ModalBehavior_js__WEBPACK_IMPORTED_MODULE_0__.default],
  ui: {
    modalbody: ".modal_content_layout",
    submit_btn: 'input[type=submit]',
    cancel_btn: 'input[type=reset]',
    form: 'form'
  },
  regions: {
    lines: {
      el: 'tbody',
      replaceElement: true
    }
  },
  events: {
    'submit @ui.form': 'onSubmit'
  },
  showExpenseLineSelect: function showExpenseLineSelect() {
    var view = new _SelectableExpenseCollectionView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      collection: this.linesCollection,
      targetFile: this.model
    });
    this.showChildView('lines', view);
  },
  template: __webpack_require__(/*! ./templates/FileLinkPopupView.mustache */ "./src/expense/views/templates/FileLinkPopupView.mustache"),
  id: "expense-file-link-form-popup-modal",
  initialize: function initialize() {
    this.model = this.getOption('model');
    this.linesCollection = this.getOption('linesCollection');
  },
  onSubmit: function onSubmit(event) {
    event.preventDefault();
    this.triggerMethod('modal:close');
  },
  templateContext: function templateContext() {
    return {
      file_label: this.model.get('label')
    };
  },
  onRender: function onRender() {
    this.showExpenseLineSelect();
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ExpenseFormPopupView);

/***/ }),

/***/ "./src/expense/views/MainView.js":
/*!***************************************!*\
  !*** ./src/expense/views/MainView.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_18___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_18__);
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone */ "./node_modules/backbone/backbone.js");
/* harmony import */ var backbone__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _common_views_StatusFormPopupView_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../common/views/StatusFormPopupView.js */ "./src/common/views/StatusFormPopupView.js");
/* harmony import */ var _models_ExpenseModel_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/ExpenseModel.js */ "./src/expense/models/ExpenseModel.js");
/* harmony import */ var _models_ExpenseKmModel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/ExpenseKmModel.js */ "./src/expense/models/ExpenseKmModel.js");
/* harmony import */ var _ExpenseTableView_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ExpenseTableView.js */ "./src/expense/views/ExpenseTableView.js");
/* harmony import */ var _ExpenseKmTableView_js__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./ExpenseKmTableView.js */ "./src/expense/views/ExpenseKmTableView.js");
/* harmony import */ var _ExpenseFormPopupView_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ExpenseFormPopupView.js */ "./src/expense/views/ExpenseFormPopupView.js");
/* harmony import */ var _ExpenseKmFormView_js__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./ExpenseKmFormView.js */ "./src/expense/views/ExpenseKmFormView.js");
/* harmony import */ var _FileLinkPopupView_js__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./FileLinkPopupView.js */ "./src/expense/views/FileLinkPopupView.js");
/* harmony import */ var _ExpenseDuplicateFormView_js__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./ExpenseDuplicateFormView.js */ "./src/expense/views/ExpenseDuplicateFormView.js");
/* harmony import */ var _TotalView_js__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./TotalView.js */ "./src/expense/views/TotalView.js");
/* harmony import */ var _TabTotalView_js__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./TabTotalView.js */ "./src/expense/views/TabTotalView.js");
/* harmony import */ var _base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../base/views/MessageView.js */ "./src/base/views/MessageView.js");
/* harmony import */ var _base_views_LoginView_js__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../base/views/LoginView.js */ "./src/base/views/LoginView.js");
/* harmony import */ var _backbone_tools_js__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../backbone-tools.js */ "./src/backbone-tools.js");
/* harmony import */ var _ExpenseFormPopupViewWithFile_js__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./ExpenseFormPopupViewWithFile.js */ "./src/expense/views/ExpenseFormPopupViewWithFile.js");
/* harmony import */ var _common_views_NodeFileViewerPopupView__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../common/views/NodeFileViewerPopupView */ "./src/common/views/NodeFileViewerPopupView.js");
/* provided dependency */ var _ = __webpack_require__(/*! underscore */ "./node_modules/underscore/underscore.js");



















/** Contruct the form popup title for a given expense
 *
 * @param expense: ExpenseBaseModel instance
 */

function makePopupTitle(expense) {
  var expenseLabel, actionLabel;

  if (expense.get('type') == 'km') {
    expenseLabel = 'dépense kilométrique';
  } else {
    expenseLabel = 'dépense';
  }

  if (expense.id) {
    actionLabel = 'Modifier';
  } else {
    actionLabel = 'Ajouter';
  }

  return "".concat(actionLabel, " une ").concat(expenseLabel, " (").concat(expense.getCategoryLabel(), ")");
}

var MainView = backbone_marionette__WEBPACK_IMPORTED_MODULE_18___default().View.extend({
  className: 'container-fluid page-content',
  template: __webpack_require__(/*! ./templates/MainView.mustache */ "./src/expense/views/templates/MainView.mustache"),
  regions: {
    modalRegion: '.modalRegion',
    internalLines: '.internal-lines',
    internalKmLines: '.internal-kmlines',
    internalTotal: '.internal-total',
    activityLines: '.activity-lines',
    activityKmLines: '.activity-kmlines',
    activityTotal: '.activity-total',
    files: '.files',
    totals: '.totals',
    messages: {
      el: '.messages-container',
      replaceElement: true
    }
  },
  ui: {
    internal: '#internal-container',
    activity: '#activity-container',
    modal: '.modalRegion'
  },
  childViewEvents: {
    'line:add': 'onLineAdd',
    'line:edit': 'onLineEdit',
    'line:delete': 'onLineDelete',
    'kmline:add': 'onKmLineAdd',
    'kmline:edit': 'onKmLineEdit',
    'kmline:delete': 'onLineDelete',
    'line:duplicate': 'onLineDuplicate',
    'kmline:duplicate': 'onLineDuplicate',
    'bookmark:add': 'onBookMarkAdd',
    'bookmark:delete': 'onBookMarkDelete',
    "status:change": 'onStatusChange',
    "attachment:delete": 'onAttachmentDelete',
    "attachment:link_to_expenseline": 'onAttachmentLink',
    "preview:displayStatusChange": 'onPreviewDisplayStatusChange'
  },
  templateContext: function templateContext() {
    return {
      internalDescription: this.categories[0].description,
      externalDescription: this.categories[1].description,
      internalTabLabel: this.categories[0].label,
      externalTabLabel: this.categories[1].label
    };
  },
  initialize: function initialize() {
    this.facade = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('facade');
    this.config = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    this.categories = this.config.request('get:options', 'categories');
    this.edit = this.config.request('get:form_section', 'general')['edit'];
    this.listenTo(this.facade, 'status:change', this.onStatusChange);
    this.listenTo(this.facade, 'show:preview', this.onShowPreview);
  },
  onLineAdd: function onLineAdd(childView, modelAttributes, withAttachment) {
    /*
     * Launch when a line should be added
     *
     * :param attributes (optional) : initial Line model attributes
     */
    var model = new _models_ExpenseModel_js__WEBPACK_IMPORTED_MODULE_3__.default(modelAttributes || {});
    this.showLineForm(model, true, withAttachment);
  },
  onKmLineAdd: function onKmLineAdd(childView, modelAttributes) {
    var model = new _models_ExpenseKmModel_js__WEBPACK_IMPORTED_MODULE_4__.default(modelAttributes || {});
    this.showKmLineForm(model, true);
  },
  showModal: function showModal(view, size) {
    if (size === undefined) {
      size = 'middle';
    }

    this.resizeModal(size);
    this.showChildView('modalRegion', view);
  },
  onShowPreview: function onShowPreview(nodeFile) {
    var view = new _common_views_NodeFileViewerPopupView__WEBPACK_IMPORTED_MODULE_17__.default({
      file: nodeFile,
      popupTitle: "Justificatif"
    });
    this.showModal(view, 'large');
  },
  resizeModal: function resizeModal(size) {
    var sizes = 'size_small size_middle size_extralarge size_large size_full';
    this.ui.modal.removeClass(sizes).addClass("size_".concat(size));
  },
  onLineEdit: function onLineEdit(childView) {
    this.showLineForm(childView.model, false);
  },
  onKmLineEdit: function onKmLineEdit(childView) {
    this.showKmLineForm(childView.model, false);
  },
  onAttachmentLink: function onAttachmentLink(model) {
    this.showFileLinkForm(model);
  },
  showFileLinkForm: function showFileLinkForm(model) {
    var view = new _FileLinkPopupView_js__WEBPACK_IMPORTED_MODULE_9__.default({
      model: model,
      linesCollection: this.facade.request('get:collection', 'lines')
    });
    this.showModal(view);
  },
  showLineForm: function showLineForm(model, add, withAttachment) {
    var params = {
      title: makePopupTitle(model),
      buttonTitle: add ? 'Ajouter' : 'Modifier',
      add: add,
      model: model,
      destCollection: this.facade.request('get:collection', 'lines')
    };
    var view;

    if (withAttachment) {
      view = new _ExpenseFormPopupViewWithFile_js__WEBPACK_IMPORTED_MODULE_16__.default(params);
    } else {
      view = new _ExpenseFormPopupView_js__WEBPACK_IMPORTED_MODULE_7__.default(params);
    } // File viewer is displayed only if we have one and only one attachment


    if ((model.get('files') || []).length === 1) {
      this.showModal(view, 'full');
    } else {
      this.showModal(view, 'middle');
    }
  },
  showKmLineForm: function showKmLineForm(model, add) {
    var view = new _ExpenseKmFormView_js__WEBPACK_IMPORTED_MODULE_8__.default({
      title: makePopupTitle(model),
      buttonTitle: add ? 'Ajouter' : 'Modifier',
      add: add,
      model: model,
      destCollection: this.facade.request('get:collection', 'kmlines')
    });
    this.showModal(view);
  },
  showDuplicateForm: function showDuplicateForm(model) {
    var view = new _ExpenseDuplicateFormView_js__WEBPACK_IMPORTED_MODULE_10__.default({
      model: model
    });
    this.showModal(view);
  },
  onLineDuplicate: function onLineDuplicate(childView) {
    this.showDuplicateForm(childView.model);
  },
  onDeleteSuccess: function onDeleteSuccess() {
    (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_15__.displayServerSuccess)("Vos données ont bien été supprimées");
  },
  onDeleteError: function onDeleteError() {
    (0,_backbone_tools_js__WEBPACK_IMPORTED_MODULE_15__.displayServerError)("Une erreur a été rencontrée lors de la " + "suppression de cet élément");
  },
  onLineDelete: function onLineDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer cette dépense ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  onPreviewDisplayStatusChange: function onPreviewDisplayStatusChange(previewDisplayed) {
    if (previewDisplayed) {
      this.resizeModal('full');
    } else {
      this.resizeModal('middle');
    }
  },
  onAttachmentDelete: function onAttachmentDelete(model) {
    var confirmed = window.confirm('Êtes-vous sûr de vouloir supprimer ce justificatif ?');

    if (confirmed) {
      model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  onBookMarkAdd: function onBookMarkAdd(childView) {
    var collection = this.facade.request('get:bookmarks');
    collection.addBookMark(childView.model);
    childView.highlightBookMark();
  },
  onBookMarkDelete: function onBookMarkDelete(childView) {
    var result = window.confirm("Êtes-vous sûr de vouloir supprimer ce favoris ?");

    if (result) {
      childView.model.destroy({
        success: this.onDeleteSuccess,
        error: this.onDeleteError
      });
    }
  },
  showTab: function showTab(categoryId, regularRegion, kmsRegion) {
    var linesCollection = this.facade.request('get:collection', 'lines');
    var filesCollection = this.facade.request('get:collection', 'attachments');
    var view = new _ExpenseTableView_js__WEBPACK_IMPORTED_MODULE_5__.default({
      collection: linesCollection,
      filesCollection: filesCollection,
      category: categoryId,
      edit: this.edit
    });
    this.showChildView(regularRegion, view);
    var km_type_options = this.config.request('get:options', 'expensekm_types');

    if (!_.isEmpty(km_type_options)) {
      var collection = this.facade.request('get:collection', 'kmlines');
      view = new _ExpenseKmTableView_js__WEBPACK_IMPORTED_MODULE_6__.default({
        collection: collection,
        category: categoryId,
        edit: this.edit
      });
      this.showChildView(kmsRegion, view);
    }
  },
  showMessages: function showMessages() {
    var model = new (backbone__WEBPACK_IMPORTED_MODULE_0___default().Model)();
    var view = new _base_views_MessageView_js__WEBPACK_IMPORTED_MODULE_13__.default({
      model: model
    });
    this.showChildView('messages', view);
  },
  showTotals: function showTotals() {
    var model = this.facade.request('get:totalmodel');
    var view = new _TotalView_js__WEBPACK_IMPORTED_MODULE_11__.default({
      model: model
    });
    this.showChildView('totals', view);
    view = new _TabTotalView_js__WEBPACK_IMPORTED_MODULE_12__.default({
      model: model,
      category: 1
    });
    this.showChildView('internalTotal', view);
    view = new _TabTotalView_js__WEBPACK_IMPORTED_MODULE_12__.default({
      model: model,
      category: 2
    });
    this.showChildView('activityTotal', view);
  },
  showLogin: function showLogin() {
    var view = new _base_views_LoginView_js__WEBPACK_IMPORTED_MODULE_14__.default({});
    this.showModal(view, 'small');
  },
  onRender: function onRender() {
    this.showTab(this.categories[0], 'internalLines', 'internalKmLines');
    this.showTab(this.categories[1], 'activityLines', 'activityKmLines');
    this.showTotals();
    this.showMessages();
  },
  onStatusChange: function onStatusChange(model) {
    console.log("Status change");
    var view = new _common_views_StatusFormPopupView_js__WEBPACK_IMPORTED_MODULE_2__.default({
      action: model
    });
    this.showModal(view, 'small');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MainView);

/***/ }),

/***/ "./src/expense/views/RegularExpenseFormView.js":
/*!*****************************************************!*\
  !*** ./src/expense/views/RegularExpenseFormView.js ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./BaseExpenseFormView.js */ "./src/expense/views/BaseExpenseFormView.js");
/* harmony import */ var _widgets_SelectBusinessWidget_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../widgets/SelectBusinessWidget.js */ "./src/widgets/SelectBusinessWidget.js");
/* harmony import */ var _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../widgets/SelectWidget.js */ "./src/widgets/SelectWidget.js");
/* harmony import */ var _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../widgets/InputWidget.js */ "./src/widgets/InputWidget.js");
/* harmony import */ var _widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../widgets/RadioChoiceButtonWidget */ "./src/widgets/RadioChoiceButtonWidget.js");
/* harmony import */ var _string__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../string */ "./src/string.js");







var RegularExpenseFormView = _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_1__.default.extend({
  childViewEvents: {
    'finish': 'onChildChange',
    'change': 'onAmountsChange',
    'labelChange': 'onChildLabelChange'
  },
  onChildLabelChange: function onChildLabelChange(field_name, label) {
    if (['customer_id', 'business_id', 'project_id'].includes(field_name)) {
      var labelFieldName = field_name.replace('_id', '_label');
      this.model.set(labelFieldName, label);
    }
  },
  initialize: function initialize() {
    _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_1__.default.prototype.initialize.apply(this);
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    this.customers_url = channel.request('get:options', 'company_customers_url');
    this.projects_url = channel.request('get:options', 'company_projects_url');
    this.businesses_url = channel.request('get:options', 'company_businesses_url');
  },
  getTypeOptions: function getTypeOptions() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('config');
    return channel.request('get:typeOptions', 'regular');
  },
  showCategorySelect: function showCategorySelect() {
    var view = new _widgets_SelectWidget_js__WEBPACK_IMPORTED_MODULE_3__.default({
      options: [{
        'value': 1,
        'label': "Frais généraux"
      }, {
        'value': 2,
        'label': "Achats clients"
      }],
      title: "Catégorie",
      field_name: "category",
      value: this.model.get('category')
    });
    this.showChildView('category', view);
  },
  onRender: function onRender() {
    _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_1__.default.prototype.onRender.apply(this);
    var view;

    if (!this.getOption('add')) {
      this.showCategorySelect();
    }

    if (this.model.get('category') != 1) {
      view = new _widgets_SelectBusinessWidget_js__WEBPACK_IMPORTED_MODULE_2__.default({
        title: 'Rattacher la dépense à',
        customers_url: this.customers_url,
        projects_url: this.projects_url,
        businesses_url: this.businesses_url,
        customer_value: this.model.get('customer_id'),
        project_value: this.model.get('project_id'),
        business_value: this.model.get('business_id'),
        customer_label: this.model.get('customer_label'),
        project_label: this.model.get('project_label'),
        business_label: this.model.get('business_label'),
        required: false
      });
      this.showChildView('business_link', view);
    }

    view = new _widgets_InputWidget_js__WEBPACK_IMPORTED_MODULE_4__.default({
      value: this.model.get('description'),
      title: 'Description',
      field_name: 'description'
    });
    this.showChildView('description', view);
    this.showFillModeChoice();
  },
  showFillModeChoice: function showFillModeChoice() {
    var msgs = {
      ht: "Vous entrez le <strong>montant HT</strong> et la TVA (montant ou taux)," + " le montant TTC est calculé ",
      ttc: "Vous entrez le <strong>montant TTC</strong> et la TVA (montant ou taux)," + " le montant HT est calculé "
    };

    if (!this.model.requiresTtcInput()) {
      var view = new _widgets_RadioChoiceButtonWidget__WEBPACK_IMPORTED_MODULE_5__.default({
        field_name: "fill_mode",
        title: "Mode de saisie",
        description: new _string__WEBPACK_IMPORTED_MODULE_6__.SafeString(msgs[this.model.get('fill_mode')]),
        value: this.model.get('fill_mode'),
        "options": [{
          'label': 'HT',
          'value': 'ht'
        }, {
          'label': 'TTC',
          'value': 'ttc'
        }]
      });
      this.showChildView('fill_mode', view);
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (RegularExpenseFormView);

/***/ }),

/***/ "./src/expense/views/SelectableExpenseCollectionView.js":
/*!**************************************************************!*\
  !*** ./src/expense/views/SelectableExpenseCollectionView.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _SelectableExpenseItemView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SelectableExpenseItemView.js */ "./src/expense/views/SelectableExpenseItemView.js");

 // import ExpenseEmptyView from './ExpenseEmptyView.js';

var SelectableExpenseCollectionView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().CollectionView.extend({
  tagName: 'tbody',
  childView: _SelectableExpenseItemView_js__WEBPACK_IMPORTED_MODULE_0__.default,
  childViewOptions: function childViewOptions() {
    return {
      targetFile: this.getOption('targetFile')
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SelectableExpenseCollectionView);

/***/ }),

/***/ "./src/expense/views/SelectableExpenseItemView.js":
/*!********************************************************!*\
  !*** ./src/expense/views/SelectableExpenseItemView.js ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseExpenseView.js */ "./src/expense/views/BaseExpenseView.js");
/* harmony import */ var _widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../widgets/CheckboxWidget.js */ "./src/widgets/CheckboxWidget.js");
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");
/* harmony import */ var _date_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../date.js */ "./src/date.js");






var template = __webpack_require__(/*! ./templates/SelectableExpenseItemView.mustache */ "./src/expense/views/templates/SelectableExpenseItemView.mustache");

var SelectableExpenseItemView = _BaseExpenseView_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  tagName: 'tr',
  template: template,
  regions: {
    td_action: 'td.action',
    businessLink: '.business-link'
  },
  modelEvents: {
    'change:selected': 'render' // ??

  },
  childViewEvents: {
    'finish': "onChange"
  },
  initialize: function initialize() {
    this.targetFile = this.getOption('targetFile');
  },
  onRender: function onRender() {
    if (this.model.get('customer_id')) {
      // Avoid displaying « rien » if there is no linked customer
      SelectableExpenseItemView.__super__.onRender.apply(this);
    }

    var widget = new _widgets_CheckboxWidget_js__WEBPACK_IMPORTED_MODULE_1__.default({
      field_name: 'check',
      toggle: false,
      ariaLabel: 'Sélectionner cette ligne',
      value: this.model.isFileLinked(this.targetFile),
      true_val: true,
      false_val: false
    });
    this.showChildView('td_action', widget);
  },
  onChange: function onChange(field_name, value) {
    if (value === true) {
      this.model.linkToFile(this.targetFile);
    } else {
      this.model.unlinkFromFile(this.targetFile);
    }

    this.model.save({
      files: this.model.get('files')
    }, {
      patch: true
    });
  },
  templateContext: function templateContext() {
    return {
      date: (0,_date_js__WEBPACK_IMPORTED_MODULE_3__.formatPaymentDate)(this.model.get('date')),
      is_achat: this.isAchat(),
      has_tva_on_margin: this.model.hasTvaOnMargin(),
      type_label: this.model.getTypeLabel(),
      ttc_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.total()),
      ht_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.get('ht')),
      tva_label: (0,_math_js__WEBPACK_IMPORTED_MODULE_2__.formatAmount)(this.model.get('tva'))
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SelectableExpenseItemView);

/***/ }),

/***/ "./src/expense/views/TabTotalView.js":
/*!*******************************************!*\
  !*** ./src/expense/views/TabTotalView.js ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");



var TabTotalView = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  tagName: 'div',
  template: __webpack_require__(/*! ./templates/TabTotalView.mustache */ "./src/expense/views/templates/TabTotalView.mustache"),
  modelEvents: {
    'change:ttc': 'render',
    'change:km_ttc': 'render'
  },
  templateContext: function templateContext() {
    var category = this.getOption('category');
    return {
      ttc: (0,_math_js__WEBPACK_IMPORTED_MODULE_1__.formatAmount)(this.model.get('ttc_' + category) + this.model.get('km_ttc_' + category))
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TabTotalView);

/***/ }),

/***/ "./src/expense/views/TelExpenseFormView.js":
/*!*************************************************!*\
  !*** ./src/expense/views/TelExpenseFormView.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./BaseExpenseFormView.js */ "./src/expense/views/BaseExpenseFormView.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_1__);


var TelExpenseFormView = _BaseExpenseFormView_js__WEBPACK_IMPORTED_MODULE_0__.default.extend({
  getTypeOptions: function getTypeOptions() {
    var channel = backbone_radio__WEBPACK_IMPORTED_MODULE_1___default().channel('config');
    return channel.request('get:typeOptions', 'tel');
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TelExpenseFormView);

/***/ }),

/***/ "./src/expense/views/TotalView.js":
/*!****************************************!*\
  !*** ./src/expense/views/TotalView.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _math_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../math.js */ "./src/math.js");


var TotalView = backbone_marionette__WEBPACK_IMPORTED_MODULE_1___default().View.extend({
  tagName: 'div',
  template: __webpack_require__(/*! ./templates/TotalView.mustache */ "./src/expense/views/templates/TotalView.mustache"),
  modelEvents: {
    'change:ttc': 'render',
    'change:ht': 'render',
    'change:tva': 'render',
    'change:km_ht': 'render',
    'change:km_tva': 'render',
    'change:km_ttc': 'render',
    'change:km': 'render'
  },
  templateContext: function templateContext() {
    return {
      ht: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('ht') + this.model.get('km_ht')),
      tva: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('tva') + this.model.get('km_tva')),
      ttc: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatAmount)(this.model.get('ttc') + this.model.get('km_ttc')),
      km: (0,_math_js__WEBPACK_IMPORTED_MODULE_0__.formatPrice)(this.model.get('km'))
    };
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TotalView);

/***/ }),

/***/ "./src/widgets/DropZoneWidget.js":
/*!***************************************!*\
  !*** ./src/widgets/DropZoneWidget.js ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! backbone.marionette */ "./node_modules/backbone.marionette/lib/backbone.marionette.js");
/* harmony import */ var backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(backbone_marionette__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! backbone.radio */ "./node_modules/backbone.radio/build/backbone.radio.js");
/* harmony import */ var backbone_radio__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(backbone_radio__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tools_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../tools.js */ "./src/tools.js");
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }




/** Combined input[type=file] and drag'n'drop zone
 *
 * intended for immediate upload (not part of a larger form)
 *
 * Requirements:
 * - `uploadAttachment(file)` radio request responding on `facade` channel
 */

var DropZoneWidget = backbone_marionette__WEBPACK_IMPORTED_MODULE_2___default().View.extend({
  tagName: 'div',
  template: __webpack_require__(/*! ./templates/DropZoneWidget.mustache */ "./src/widgets/templates/DropZoneWidget.mustache"),
  ui: {
    input: 'input[type=file]',
    dropzone: '.drop_files_here',
    form: 'form'
  },
  events: {
    // Mark zone as drag'n'drop target for browser
    'dragover @ui.dropzone': function dragoverUiDropzone(e) {
      return e.preventDefault();
    },
    'dragenter @ui.dropzone': function dragenterUiDropzone(e) {
      return e.preventDefault();
    },
    'drop @ui.dropzone': 'onFilesDropped',
    'change @ui.input': 'onFilesSelected'
  },
  onFilesDropped: function onFilesDropped(event) {
    event.preventDefault();
    this.onFilesAdded(event.originalEvent.dataTransfer.files);
  },
  onFilesSelected: function onFilesSelected(event) {
    this.onFilesAdded(event.target.files);
  },
  onFilesAdded: function onFilesAdded(files) {
    var facade = backbone_radio__WEBPACK_IMPORTED_MODULE_0___default().channel('facade');

    var _iterator = _createForOfIteratorHelper(files),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var file = _step.value;
        facade.request('uploadAttachment', file);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  }
});
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DropZoneWidget);

/***/ }),

/***/ "./src/expense/views/templates/BookMarkCollectionView.mustache":
/*!*********************************************************************!*\
  !*** ./src/expense/views/templates/BookMarkCollectionView.mustache ***!
  \*********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "							<div class='modal_content with_table'>\n								<div class='table_container'>\n									<table class=\"hover_table top_align_table\">\n										<thead>\n											<tr>\n												<th scope=\"col\" class=\"col_text\" title=\"Type de dépense et description\">Type<span class=\"screen-reader-text\"> de dépense</span> et description</th>\n												<th scope=\"col\" class=\"col_number\" title=\"Montant Hors Taxes\"><span class=\"screen-reader-text\">Montant </span>H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span></th>\n												<th scope=\"col\" class=\"col_number\" title=\"Montant de la TVA\"><span class=\"screen-reader-text\">Montant de la </span>TVA</th>\n												<th scope=\"col\" class=\"col_actions width_two\" title=\"Actions\"><span class=\"screen-reader-text\">Actions</span></th>\n											</tr>\n										</thead>\n										<tbody id=\"bookmark-list-container\">\n										</tbody>\n									</table>\n								</div>\n							</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/BookMarkView.mustache":
/*!***********************************************************!*\
  !*** ./src/expense/views/templates/BookMarkView.mustache ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "													<br />\n													"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":5,"column":13},"end":{"line":5,"column":30}}}) : helper)))
    + "\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "                                            <td class=\"col_text clickable-cell\" "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"insertAttrs") || (depth0 != null ? lookupProperty(depth0,"insertAttrs") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insertAttrs","hash":{},"data":data,"loc":{"start":{"line":1,"column":80},"end":{"line":1,"column":99}}}) : helper))) != null ? stack1 : "")
    + ">\n                                                <strong>"
    + alias4(((helper = (helper = lookupProperty(helpers,"typelabel") || (depth0 != null ? lookupProperty(depth0,"typelabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typelabel","hash":{},"data":data,"loc":{"start":{"line":2,"column":56},"end":{"line":2,"column":71}}}) : helper)))
    + "</strong>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":48},"end":{"line":6,"column":19}}})) != null ? stack1 : "")
    + "											</td>\n											<td class=\"col_number clickable-cell\" "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"insertAttrs") || (depth0 != null ? lookupProperty(depth0,"insertAttrs") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insertAttrs","hash":{},"data":data,"loc":{"start":{"line":8,"column":49},"end":{"line":8,"column":68}}}) : helper))) != null ? stack1 : "")
    + ">\n                                                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":9,"column":48},"end":{"line":9,"column":58}}}) : helper))) != null ? stack1 : "")
    + "\n                                            </td>\n											<td class=\"col_number clickable-cell\" "
    + alias4(((helper = (helper = lookupProperty(helpers,"insertAttrs") || (depth0 != null ? lookupProperty(depth0,"insertAttrs") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insertAttrs","hash":{},"data":data,"loc":{"start":{"line":11,"column":49},"end":{"line":11,"column":66}}}) : helper)))
    + ">\n                                                "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva") || (depth0 != null ? lookupProperty(depth0,"tva") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva","hash":{},"data":data,"loc":{"start":{"line":12,"column":48},"end":{"line":12,"column":59}}}) : helper))) != null ? stack1 : "")
    + "\n                                            </td>\n											<td class=\"col_actions width_two\">\n												<ul>\n													<li>\n														<button class='btn icon only main btn-success insert' "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"insertAttrs") || (depth0 != null ? lookupProperty(depth0,"insertAttrs") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"insertAttrs","hash":{},"data":data,"loc":{"start":{"line":17,"column":68},"end":{"line":17,"column":88}}}) : helper))) != null ? stack1 : "")
    + "><svg><use href=\"/static/icons/endi.svg#check\"></use></svg></button>\n													</li>\n													<li>\n														<button class='btn icon only negative btn-danger delete' title='Supprimer ce favori' aria-label='Supprimer ce favori'><svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg></button>\n													</li>\n												</ul>\n											</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseDuplicateFormView.mustache":
/*!***********************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseDuplicateFormView.mustache ***!
  \***********************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<h3>Note de dépenses kilométriques</h3>\n							De "
    + alias4(((helper = (helper = lookupProperty(helpers,"start") || (depth0 != null ? lookupProperty(depth0,"start") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"start","hash":{},"data":data,"loc":{"start":{"line":15,"column":10},"end":{"line":15,"column":19}}}) : helper)))
    + " à "
    + alias4(((helper = (helper = lookupProperty(helpers,"end") || (depth0 != null ? lookupProperty(depth0,"end") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"end","hash":{},"data":data,"loc":{"start":{"line":15,"column":22},"end":{"line":15,"column":29}}}) : helper)))
    + " ("
    + alias4(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":15,"column":31},"end":{"line":15,"column":46}}}) : helper)))
    + ")<br />\n							Nombre de kilomètres : "
    + alias4(((helper = (helper = lookupProperty(helpers,"km") || (depth0 != null ? lookupProperty(depth0,"km") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"km","hash":{},"data":data,"loc":{"start":{"line":16,"column":30},"end":{"line":16,"column":36}}}) : helper)))
    + " <br />\n							Montant remboursé : "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":17,"column":27},"end":{"line":17,"column":36}}}) : helper))) != null ? stack1 : "")
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "							<h3>Note de dépenses à dupliquer</h3>\n							"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data,"loc":{"start":{"line":20,"column":7},"end":{"line":20,"column":22}}}) : helper)))
    + "<br />\n							<div class='expense_totals'>\n								<div>\n									<div class=\"layout flex two_cols\">\n										<div>\n											<p>HT&nbsp;: </p>\n										</div>\n										<div>\n											<p><strong>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":28,"column":22},"end":{"line":28,"column":32}}}) : helper))) != null ? stack1 : "")
    + "</strong></p>\n										</div>\n									</div>\n								</div>\n								<div>\n									<div class=\"layout flex two_cols\">\n										<div>\n											<p>TVA&nbsp;: </p>\n										</div>\n										<div>\n											<p><strong>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva") || (depth0 != null ? lookupProperty(depth0,"tva") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva","hash":{},"data":data,"loc":{"start":{"line":38,"column":22},"end":{"line":38,"column":33}}}) : helper))) != null ? stack1 : "")
    + "</strong></p>\n										</div>\n									</div>\n								</div>\n							</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div role=\"dialog\" id=\"expense-forms\" aria-modal=\"true\" aria-labelledby=\"expense-forms_title\">\n        <form>\n			<div class=\"modal_layout\">\n				<header>\n					<button tabindex='-1' type=\"button\" class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n						<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n					</button>\n					<h2 id=\"expense-forms_title\">Dupliquer une note de dépenses</h2>\n				</header>\n				<div class=\"modal_content_layout\">\n					<div class=\"modal_content\">\n						<div class='separate_bottom'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_km_fee") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":13,"column":7},"end":{"line":43,"column":14}}})) != null ? stack1 : "")
    + "						</div>\n						<div class='select layout'></div>\n					</div>\n					<footer>\n						<button\n							class='btn btn-primary'\n							type='submit'\n							value='submit'>\n							Dupliquer\n						</button>\n						<button\n							class='btn'\n							type='reset'\n							value='submit'>\n							Fermer\n						</button>\n					</footer>\n				</div>\n			</div>\n		</form>\n	</div><!-- /#expense-forms -->\n\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseEmptyView.mustache":
/*!***************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseEmptyView.mustache ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td colspan='"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"colspan") || (depth0 != null ? lookupProperty(depth0,"colspan") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"colspan","hash":{},"data":data,"loc":{"start":{"line":1,"column":13},"end":{"line":1,"column":26}}}) : helper)))
    + "' class='col_text'><em>Aucune dépense n’a été ajoutée.</em></td>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseFileCollectionView.mustache":
/*!************************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseFileCollectionView.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tr>\n	<td class=\"col_text empty\" colspan=\"10\">\n		<h3>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"items_count") || (depth0 != null ? lookupProperty(depth0,"items_count") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"items_count","hash":{},"data":data,"loc":{"start":{"line":3,"column":6},"end":{"line":3,"column":23}}}) : helper))) != null ? stack1 : "")
    + " justificatif"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"pluralize") || (depth0 != null ? lookupProperty(depth0,"pluralize") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pluralize","hash":{},"data":data,"loc":{"start":{"line":3,"column":36},"end":{"line":3,"column":51}}}) : helper))) != null ? stack1 : "")
    + " non lié"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"pluralize") || (depth0 != null ? lookupProperty(depth0,"pluralize") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"pluralize","hash":{},"data":data,"loc":{"start":{"line":3,"column":59},"end":{"line":3,"column":74}}}) : helper))) != null ? stack1 : "")
    + " à une dépense</h3>\n	</td>\n</tr>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseFileItemView.mustache":
/*!******************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseFileItemView.mustache ***!
  \******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\" colspan=\"2\"></td>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\" colspan=\"3\"></td>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "      colspan=\"";
  stack1 = ((helper = (helper = lookupProperty(helpers,"is_achat") || (depth0 != null ? lookupProperty(depth0,"is_achat") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"is_achat","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data,"loc":{"start":{"line":8,"column":15},"end":{"line":8,"column":51}}}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!lookupProperty(helpers,"is_achat")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\"\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "6";
},"8":function(container,depth0,helpers,partials,data) {
    return "5";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "      colspan=\"";
  stack1 = ((helper = (helper = lookupProperty(helpers,"is_achat") || (depth0 != null ? lookupProperty(depth0,"is_achat") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"is_achat","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(11, data, 0),"data":data,"loc":{"start":{"line":10,"column":15},"end":{"line":10,"column":51}}}),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),options) : helper));
  if (!lookupProperty(helpers,"is_achat")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "\"\n";
},"11":function(container,depth0,helpers,partials,data) {
    return "4";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "         <a href=\"#\"\n           title=\"Visualiser le justificatif "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":15,"column":45},"end":{"line":15,"column":56}}}) : helper))) != null ? stack1 : "")
    + "\"\n           aria-label=\"Visualiser le justificatif "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":16,"column":50},"end":{"line":16,"column":61}}}) : helper))) != null ? stack1 : "")
    + "\"\n        >\n           "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":18,"column":11},"end":{"line":18,"column":22}}}) : helper))) != null ? stack1 : "")
    + "\n        </a>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <a href=\""
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"openLink") || (depth0 != null ? lookupProperty(depth0,"openLink") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"openLink","hash":{},"data":data,"loc":{"start":{"line":21,"column":17},"end":{"line":21,"column":31}}}) : helper))) != null ? stack1 : "")
    + "\"\n           title=\"Télécharger le justificatif "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":22,"column":46},"end":{"line":22,"column":57}}}) : helper))) != null ? stack1 : "")
    + "\"\n           aria-label=\"Télécharger le justificatif "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":23,"column":51},"end":{"line":23,"column":62}}}) : helper))) != null ? stack1 : "")
    + "\"\n        >\n            "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":25,"column":12},"end":{"line":25,"column":23}}}) : helper))) != null ? stack1 : "")
    + "\n        </a>\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_actions width_four\">\n        <span class=\"new-expense-btn\"></span>\n        <span class=\"link-btn\"></span>\n        <span class=\"delete-btn\"></span>\n    </td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":5,"column":7}}})) != null ? stack1 : "")
    + "<td class=\"col_text\"\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"if","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":7,"column":4},"end":{"line":11,"column":11}}})) != null ? stack1 : "")
    + ">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"isPreviewable") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.program(15, data, 0),"data":data,"loc":{"start":{"line":13,"column":4},"end":{"line":27,"column":11}}})) != null ? stack1 : "")
    + "\n\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"edit") : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":0},"end":{"line":37,"column":7}}})) != null ? stack1 : "");
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseFormPopupView.mustache":
/*!*******************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseFormPopupView.mustache ***!
  \*******************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "			<nav>\n				<ul class=\"nav nav-tabs modal-tabs\" role=\"tablist\">\n					<li role=\"presentation\" class=\"active main\">\n						<a href=\"#mainform-container\"\n							aria-controls=\"mainform-container\"\n							role=\"tab\"\n							data-toggle=\"tab\"\n							tabindex='-1'\n							id=\"mainform-tabtitle\"\n							>\n							<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#shopping-cart\"></use></svg></span>\n							<span>Dépense</span>\n						</a>\n					</li>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"allow_tel_tab") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":20},"end":{"line":37,"column":27}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_bookmarks") : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":38,"column":5},"end":{"line":51,"column":12}}})) != null ? stack1 : "")
    + "				</ul>\n			</nav>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "					<li role=\"presentation\" class='tel'>\n						<a href=\"#telform-container\"\n							aria-controls=\"telform-container\"\n							role=\"tab\"\n							data-toggle=\"tab\"\n							tabindex='-1'\n							id=\"telform-tabtitle\"\n							>\n							<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#phone\"></use></svg></span>\n							<span>Frais télécom</span>\n						</a>\n					</li>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "						<li role=\"presentation\">\n							<a href=\"#bookmark-container\"\n								aria-controls=\"bookmark-container\"\n								role=\"tab\"\n								tabindex='-1'\n								data-toggle=\"tab\"\n								id=\"bookmark-tabtitle\"\n								>\n								<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#star-empty\"></use></svg></span>\n								Favoris\n							</a>\n						</li>\n";
},"6":function(container,depth0,helpers,partials,data) {
    return "				<div\n					role=\"tabpanel\"\n					class=\"tab-pane fade in active layout\"\n					id=\"mainform-container\"\n					aria-labelledby=\"mainform-tabtitle\">\n				</div>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				<div\n					role=\"tabpanel\"\n					class=\"tab-pane fade in "
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"show_tel_tab") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":67,"column":29},"end":{"line":67,"column":62}}})) != null ? stack1 : "")
    + " layout\"\n					id=\"telform-container\"\n					aria-labelledby=\"telform-tabtitle\">\n				</div>\n";
},"9":function(container,depth0,helpers,partials,data) {
    return "active";
},"11":function(container,depth0,helpers,partials,data) {
    return "				<div\n					role=\"tabpanel\"\n					class=\"tab-pane fade in layout\"\n					id=\"bookmark-container\"\n					aria-labelledby=\"bookmark-tabtitle\">\n				</div>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    <div role=\"dialog\" id=\"expense-forms\" aria-modal=\"true\" aria-labelledby=\"expense-forms_title\">\n		<div class=\"modal_layout\">\n			<header>\n				<button tabindex='-1' type=\"button\" class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n					<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n				</button>\n				<h2 id=\"expense-forms_title\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":7,"column":33},"end":{"line":7,"column":44}}}) : helper)))
    + "</h2>\n			</header>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":3},"end":{"line":54,"column":10}}})) != null ? stack1 : "")
    + "			<div class='tab-content'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_main") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":56,"column":4},"end":{"line":63,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_tel") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":64,"column":4},"end":{"line":71,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"show_bookmarks") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":72,"column":4},"end":{"line":79,"column":10}}})) != null ? stack1 : "")
    + "			</div>\n<!-- \n			<div class=\"modal-footer\">\n			</div>\n -->\n  		</div>\n	</div><!-- /#expense-forms -->\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseFormPreviewWrapperView.mustache":
/*!****************************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseFormPreviewWrapperView.mustache ***!
  \****************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"preview\" style=\"display: none\"></div>\n<div class=\"form-component\"></div>\n<div class=\"loader\" style=\"display: none\"></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseFormView.mustache":
/*!**************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseFormView.mustache ***!
  \**************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "        <div class='row form-row'>\n            <div class='category required col-md-12'></div>\n        </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "display: none";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<form class='modal_content_layout layout'>\n    <div class=\"modal_content\">\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"add") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":3,"column":8},"end":{"line":7,"column":19}}})) != null ? stack1 : "")
    + "        <div class='row form-row'>\n            <div class='date required col-md-6'></div>\n            <div class='type_id required col-md-6'></div>\n        </div>\n        <div class='row form-row'>\n            <div class='description required col-md-12'></div>\n        </div>\n        <div class=\"row form-row\">\n            <div class=\"fill_mode col-md-12\"></div>\n        </div>\n        <div class='row form-row'>\n            <div class='ht col-md-3' style=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden_ht") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":44},"end":{"line":19,"column":81}}})) != null ? stack1 : "")
    + "\"></div>\n            <div class='manual_ttc col-md-3' style=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden_manual_ttc") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":52},"end":{"line":20,"column":97}}})) != null ? stack1 : "")
    + "\"></div>\n            <div class='tva col-md-3' style=\""
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"hidden_tva") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":21,"column":45},"end":{"line":21,"column":83}}})) != null ? stack1 : "")
    + "\"></div>\n            <div class='tva_rate col-md-3'></div>\n            <div class='ttc_readonly col-md-3'></div>\n        </div>\n        <div class=\"row form-row\">\n            <div class=\"supplier_id col-md-6\"></div>\n            <div class=\"invoice_number col-md-6\"></div>\n        </div>\n        <div class='row form-row'>\n            <div class='files col-md-12'></div>\n        </div>\n        <div class='row form-row'>\n            <div class='business_link col-md-12'></div>\n        </div>\n    </div>\n    <footer>\n        <button\n            class='btn btn-primary'\n            type='submit'\n            value='submit'>\n            "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"button_title") || (depth0 != null ? lookupProperty(depth0,"button_title") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"button_title","hash":{},"data":data,"loc":{"start":{"line":41,"column":12},"end":{"line":41,"column":30}}}) : helper)))
    + "\n        </button>\n        <button\n            class='btn'\n            type='reset'\n            value='submit'>\n            Fermer\n        </button>\n    </footer>\n</form>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseKmFormView.mustache":
/*!****************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseKmFormView.mustache ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div role=\"dialog\" id=\"expense-forms\" aria-modal=\"true\" aria-labelledby=\"expense-forms_title\">\n    <form class='form layout'>\n        <div class=\"modal_layout\">\n            <header>\n                <button tabindex='-1' type=\"button\" class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n                    <svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n                </button>\n                <h2 id=\"expense-forms_title\" class=\"modal-title\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"title") || (depth0 != null ? lookupProperty(depth0,"title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data,"loc":{"start":{"line":8,"column":65},"end":{"line":8,"column":76}}}) : helper)))
    + "</h2>\n            </header>\n			<div class=\"modal_content_layout layout\">\n                <div class=\"modal_content add_km_expense\">\n                    <div class='row form-row'>\n                        <div class='category required col-md-12'></div>\n                    </div>\n                    <div class='row form-row'>\n                        <div class='date required col-md-6'></div>\n                        <div class='type_id required col-md-6'></div>\n                    </div>\n                    <div class='row form-row'>\n                        <div class='description required col-md-12'></div>\n                    </div>\n                    <div class='row form-row'>\n                        <div class='start required col-md-6'></div>\n                        <div class='end required col-md-6'></div>\n                    </div>\n                    <div class='row form-row'>\n                        <div class='km col-md-6'></div>\n                        <div class='business_link col-md-6'></div>\n                    </div>\n                </div>\n                <footer>\n                    <button\n                        class='btn btn-primary'\n                        type='submit'\n                        value='submit'>\n                        "
    + alias4(((helper = (helper = lookupProperty(helpers,"button_title") || (depth0 != null ? lookupProperty(depth0,"button_title") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"button_title","hash":{},"data":data,"loc":{"start":{"line":36,"column":24},"end":{"line":36,"column":42}}}) : helper)))
    + "\n                    </button>\n                    <button\n                        class='btn'\n                        type='reset'\n                        value='submit'>\n                        Fermer\n                    </button>\n                </footer>\n            </div>\n        </div><!-- /.modal_layout -->\n    </form>\n</div><!-- /#expense-forms -->\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseKmTableView.mustache":
/*!*****************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseKmTableView.mustache ***!
  \*****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<div class=\"layout flex two_cols content_vertical_padding separate_top\">\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "<div class=\"layout flex content_vertical_padding separate_top\">\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "	<table class=\"opa hover_table top_align_table\">\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "	<table class=\"opa top_align_table\">\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "			<tr>\n			    <th scope=\"col\" class=\"col_date\">Date</th>\n			    <th scope=\"col\" class=\"col_text\" title=\"Type de dépense et motif de déplacement\">Type<span class=\"screen-reader-text\"> de dépense</span> et motif de déplacement</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":24,"column":7},"end":{"line":26,"column":14}}})) != null ? stack1 : "")
    + "			    <th scope=\"col\" class=\"col_text\">Départ</th>\n			    <th scope=\"col\" class=\"col_text\">Arrivée</th>\n			    <th scope=\"col\" class=\"col_number\">Kms</th>\n			    <th scope=\"col\" class=\"col_number\">Indemnités</th>\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"edit","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":31,"column":7},"end":{"line":37,"column":16}}}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</tr>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "				<th scope=\"col\" class=\"col_text\">Rattaché à…</th>\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "				<th scope=\"col\" class=\"col_actions width_three\"\n				    title=\"Actions\"\n				>\n				    <span class=\"screen-reader-text\">Actions</span>\n				</th>\n";
},"14":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "			    <td colspan='"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.program(17, data, 0),"data":data,"loc":{"start":{"line":42,"column":20},"end":{"line":42,"column":53}}})) != null ? stack1 : "")
    + "'></td>\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "7";
},"17":function(container,depth0,helpers,partials,data) {
    return "6";
},"19":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(20, data, 0),"inverse":container.program(22, data, 0),"data":data,"loc":{"start":{"line":44,"column":7},"end":{"line":48,"column":14}}})) != null ? stack1 : "")
    + "			    <td class='total_km col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_km") || (depth0 != null ? lookupProperty(depth0,"total_km") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_km","hash":{},"data":data,"loc":{"start":{"line":49,"column":39},"end":{"line":49,"column":55}}}) : helper))) != null ? stack1 : "")
    + "</td>\n			    <td class='total_ttc col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc") || (depth0 != null ? lookupProperty(depth0,"total_ttc") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc","hash":{},"data":data,"loc":{"start":{"line":50,"column":40},"end":{"line":50,"column":57}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
},"20":function(container,depth0,helpers,partials,data) {
    return "				<th scope='row' colspan='5' class='col_text'>Total</th>\n";
},"22":function(container,depth0,helpers,partials,data) {
    return "				<th scope='row' colspan='4' class='col_text'>Total</th>\n";
},"24":function(container,depth0,helpers,partials,data) {
    return "			    <td class='col_actions width_three'>\n				<button class='btn btn-primary icon add'\n					title='Ajouter une dépense kilométrique'\n					aria-label='Ajouter une dépense kilométrique'\n				>\n				    <svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n				    Ajouter\n				</button>\n			    </td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.hooks.blockHelperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = "";

  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":5,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "	<div class=\"vertical_align_center\">\n		<h2>\n			<span class=\"icon status neutral\"><svg><use href=\"/static/icons/endi.svg#car-side\"></use></svg></span>\n            Dépenses kilométriques\n        </h2>\n	</div>\n</div>\n<div class=\"table_container\">\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":14,"column":0},"end":{"line":18,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<thead>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_empty") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":6},"end":{"line":39,"column":13}}})) != null ? stack1 : "")
    + "		    <tr class=\"row_recap\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_empty") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.program(19, data, 0),"data":data,"loc":{"start":{"line":41,"column":3},"end":{"line":51,"column":10}}})) != null ? stack1 : "");
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(24, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":52,"column":3},"end":{"line":62,"column":12}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "		    </tr>\n		</thead>\n		<tbody class='lines'>\n		</tbody>\n	</table>\n</div>\n\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseKmView.mustache":
/*!************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseKmView.mustache ***!
  \************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "		<br />\n		"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":6,"column":2},"end":{"line":6,"column":19}}}) : helper)))
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\"><span class=\"business-link\"></span></td>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<td class='col_actions width_three'>\n	<ul>\n		<li>\n			<button class='btn icon only edit' title='Modifier' aria-label='Modifier'><svg><use href=\"/static/icons/endi.svg#pen\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only duplicate' title='Dupliquer' aria-label='Dupliquer'><svg><use href=\"/static/icons/endi.svg#copy\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only negative delete' title='Supprimer' aria-label='Supprimer'><svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg></button>\n		</li>\n	</ul>	\n</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "<td class=\"col_date\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"altdate") || (depth0 != null ? lookupProperty(depth0,"altdate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"altdate","hash":{},"data":data,"loc":{"start":{"line":1,"column":21},"end":{"line":1,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n	<strong>"
    + alias4(((helper = (helper = lookupProperty(helpers,"typelabel") || (depth0 != null ? lookupProperty(depth0,"typelabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typelabel","hash":{},"data":data,"loc":{"start":{"line":3,"column":9},"end":{"line":3,"column":24}}}) : helper)))
    + "</strong>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":4,"column":1},"end":{"line":7,"column":8}}})) != null ? stack1 : "")
    + "</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":9,"column":0},"end":{"line":11,"column":7}}})) != null ? stack1 : "")
    + "<td class=\"col_text\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"start") || (depth0 != null ? lookupProperty(depth0,"start") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"start","hash":{},"data":data,"loc":{"start":{"line":12,"column":21},"end":{"line":12,"column":32}}}) : helper)))
    + "</td>\n<td class=\"col_text\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"end") || (depth0 != null ? lookupProperty(depth0,"end") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"end","hash":{},"data":data,"loc":{"start":{"line":13,"column":21},"end":{"line":13,"column":30}}}) : helper)))
    + "</td>\n<td class=\"col_number\">"
    + alias4(((helper = (helper = lookupProperty(helpers,"km") || (depth0 != null ? lookupProperty(depth0,"km") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"km","hash":{},"data":data,"loc":{"start":{"line":14,"column":23},"end":{"line":14,"column":31}}}) : helper)))
    + "&nbsp;km</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total") || (depth0 != null ? lookupProperty(depth0,"total") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data,"loc":{"start":{"line":15,"column":23},"end":{"line":15,"column":34}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(5, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":16,"column":0},"end":{"line":30,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseTableView.mustache":
/*!***************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseTableView.mustache ***!
  \***************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    return "<div class=\"layout flex two_cols\">\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "<div class=\"layout flex\">\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "	<table class=\"opa hover_table top_align_table\">\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "	<table class=\"opa top_align_table\">\n";
},"9":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  "			<tr>\n"
    + ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"unless","hash":{},"fn":container.program(10, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":22,"column":16},"end":{"line":26,"column":27}}})) != null ? stack1 : "")
    + "				<th scope=\"col\" class=\"col_date\">Date</th>\n				<th scope=\"col\" class=\"col_text\" title=\"Type de dépense et description\">Type<span class=\"screen-reader-text\"> de dépense</span> et description</th>\n                <th scope=\"col\" class=\"col_text\">Justificatif</th>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"if","hash":{},"fn":container.program(12, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":30,"column":16},"end":{"line":35,"column":23}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(14, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":36,"column":4},"end":{"line":38,"column":11}}})) != null ? stack1 : "")
    + "				<th scope=\"col\" class=\"col_number\" title=\"Montant Hors Taxes\">\n                    <span class=\"screen-reader-text\">Montant </span>H<span class=\"screen-reader-text\">ors </span>T<span class=\"screen-reader-text\">axes</span>\n                </th>\n				<th scope=\"col\" class=\"col_number\">\n                    <span class=\"screen-reader-text\">Montant de la </span>TVA\n                </th>\n				<th scope=\"col\" class=\"col_number\" title=\"Montant Toutes Taxes Comprises\">\n                    <span class=\"screen-reader-text\">Montant </span>TTC\n                </th>\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : container.hooks.helperMissing),(options={"name":"edit","hash":{},"fn":container.program(16, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":48,"column":4},"end":{"line":52,"column":13}}}),(typeof helper === "function" ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</tr>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "                    <th scope=\"col\"  class=\"col_status\"title=\"Statut des justificatifs\">\n                        <span class=\"screen-reader-text\">Statut des justificatifs</span>\n                    </th>\n";
},"12":function(container,depth0,helpers,partials,data) {
    return "                    <th class=\"col_text\" scope=\"col\"\n                        title=\"Validation des justificatifs\">\n                        <span class=\"screen-reader-text\">Validation des justificatifs</span>\n                    </th>\n";
},"14":function(container,depth0,helpers,partials,data) {
    return "					<th scope=\"col\" class=\"col_text\">Rattaché à…</th>\n";
},"16":function(container,depth0,helpers,partials,data) {
    return "					<th scope=\"col\" class=\"col_actions width_four\" title=\"Actions\">\n						<span class=\"screen-reader-text\">Actions</span>\n					</th>\n";
},"18":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "					<td colspan='"
    + ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.program(21, data, 0),"data":data,"loc":{"start":{"line":57,"column":18},"end":{"line":57,"column":51}}})) != null ? stack1 : "")
    + "'></td>\n";
},"19":function(container,depth0,helpers,partials,data) {
    return "8";
},"21":function(container,depth0,helpers,partials,data) {
    return "7";
},"23":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(24, data, 0),"inverse":container.program(26, data, 0),"data":data,"loc":{"start":{"line":59,"column":5},"end":{"line":63,"column":12}}})) != null ? stack1 : "")
    + "					<td class='total_ht col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ht") || (depth0 != null ? lookupProperty(depth0,"total_ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ht","hash":{},"data":data,"loc":{"start":{"line":64,"column":37},"end":{"line":64,"column":53}}}) : helper))) != null ? stack1 : "")
    + "</td>\n					<td class='col_number'>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"includes_tva_on_margin") : depth0),{"name":"if","hash":{},"fn":container.program(28, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":66,"column":6},"end":{"line":72,"column":13}}})) != null ? stack1 : "")
    + "						<span class=\"total_tva\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_tva") || (depth0 != null ? lookupProperty(depth0,"total_tva") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_tva","hash":{},"data":data,"loc":{"start":{"line":73,"column":30},"end":{"line":73,"column":47}}}) : helper))) != null ? stack1 : "")
    + "</span>\n					</td>\n					<td class='total_ttc col_number'>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total_ttc") || (depth0 != null ? lookupProperty(depth0,"total_ttc") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total_ttc","hash":{},"data":data,"loc":{"start":{"line":75,"column":38},"end":{"line":75,"column":55}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
},"24":function(container,depth0,helpers,partials,data) {
    return "						<th scope='row' colspan='5' class='col_text'>Total</th>\n";
},"26":function(container,depth0,helpers,partials,data) {
    return "						<th scope='row' colspan='4' class='col_text'>Total</th>\n";
},"28":function(container,depth0,helpers,partials,data) {
    return "							<span class=\"icon small\"\n								  title=\"Inclut des dépenses en mode TVA sur marge : il s'agit d'une TVA estimée, le TTC fait foi.\"\n							>\n								<svg><use href=\"/static/icons/endi.svg#exclamation-circle\"></use></svg>\n							</span>\n";
},"30":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "				    <td class='col_actions width_four'>\n                        <button class='btn btn-primary add icon'\n                                title='Ajouter une dépense dans les "
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"category") : depth0)) != null ? lookupProperty(stack1,"label") : stack1), depth0))
    + "'\n                                aria-label='Ajouter une dépense dans les "
    + alias2(alias1(((stack1 = (depth0 != null ? lookupProperty(depth0,"category") : depth0)) != null ? lookupProperty(stack1,"label") : stack1), depth0))
    + "'\n                        >\n                            <svg><use href=\"/static/icons/endi.svg#plus\"></use></svg>\n                            Ajouter\n                        </button>\n                    </td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.hooks.blockHelperMissing, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = "";

  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":5,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "	<div class=\"vertical_align_center\">\n		<h2>\n			<span class=\"icon status neutral\"><svg><use href=\"/static/icons/endi.svg#shopping-cart\"></use></svg></span>\n			Dépenses\n		</h2>\n	</div>\n</div>\n<div class=\"table_container\">\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data,"loc":{"start":{"line":14,"column":0},"end":{"line":18,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  buffer += "		<thead>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_not_empty") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":20,"column":3},"end":{"line":54,"column":10}}})) != null ? stack1 : "")
    + "			<tr class=\"row_recap\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_empty") : depth0),{"name":"if","hash":{},"fn":container.program(18, data, 0),"inverse":container.program(23, data, 0),"data":data,"loc":{"start":{"line":56,"column":4},"end":{"line":76,"column":11}}})) != null ? stack1 : "");
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(30, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":77,"column":4},"end":{"line":87,"column":13}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = alias4.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer + "			</tr>\n		</thead>\n		<tbody class='lines'></tbody>\n        <tbody class='files'></tbody>\n	</table>\n</div>\n<div class=\"dropzone\"></div>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseTelView.mustache":
/*!*************************************************************!*\
  !*** ./src/expense/views/templates/ExpenseTelView.mustache ***!
  \*************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"justified") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":14,"column":11}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "        <td class=\"col_status\"\n            title=\"Justificatif(s) accepté(s)\"\n            aria-label=\"Justificatif(s) accepté(s)\"\n        >\n    <span class=\"icon status valid\">\n        <svg><use href=\"../static/icons/endi.svg#check\"></use></svg>\n    </span>\n        </td>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "        <td class=\"col_status\" title=\"Justificatif(s) en attente\" aria-label=\"Justificatif(s) en attente\">\n		</td>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        <br />\n        (Facture "
    + alias4(((helper = (helper = lookupProperty(helpers,"invoice_number") || (depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"invoice_number","hash":{},"data":data,"loc":{"start":{"line":21,"column":17},"end":{"line":21,"column":37}}}) : helper)))
    + " / "
    + alias4(((helper = (helper = lookupProperty(helpers,"supplier_label") || (depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_label","hash":{},"data":data,"loc":{"start":{"line":21,"column":40},"end":{"line":21,"column":60}}}) : helper)))
    + ")\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0),{"name":"if","hash":{},"fn":container.program(9, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":23,"column":8},"end":{"line":26,"column":15}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":27,"column":8},"end":{"line":30,"column":15}}})) != null ? stack1 : "");
},"9":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <br />\n            "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"invoice_number") || (depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"invoice_number","hash":{},"data":data,"loc":{"start":{"line":25,"column":12},"end":{"line":25,"column":30}}}) : helper)))
    + "\n";
},"11":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <br />\n            "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"supplier_label") || (depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_label","hash":{},"data":data,"loc":{"start":{"line":29,"column":12},"end":{"line":29,"column":32}}}) : helper)))
    + "\n";
},"13":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <li>\n                <a href=\"/files/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":40,"column":32},"end":{"line":40,"column":38}}}) : helper)))
    + "?action=download\"\n                   title=\"Ouvrir le justificatif "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":41,"column":49},"end":{"line":41,"column":60}}}) : helper)))
    + " dans une nouvelle fenêtre\"\n                   aria-label=\"Ouvrir le justificatif "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":42,"column":54},"end":{"line":42,"column":65}}}) : helper)))
    + " dans une nouvelle fenêtre\"\n                   target=\"_blank\"\n                >\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":45,"column":20},"end":{"line":45,"column":31}}}) : helper)))
    + "\n                </a>\n            </li>\n";
},"15":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\">\n        <div class=\"justified\"></div>\n    </td>\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "<td class='col_actions width_four'>\n	<ul>\n		<li>\n			<button class='btn icon only edit' title='Modifier' aria-label='Modifier'><svg><use href=\"/static/icons/endi.svg#pen\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only duplicate' title='Dupliquer' aria-label='Dupliquer'><svg><use href=\"/static/icons/endi.svg#copy\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only bookmark' title='Mettre en favori' aria-label='Mettre en favori'><svg><use href=\"/static/icons/endi.svg#star-empty\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only negative delete' title='Supprimer' aria-label='Supprimer'><svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg></button>\n		</li>\n	</ul>	\n</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":15,"column":11}}})) != null ? stack1 : "")
    + "<td class=\"col_date\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"altdate") || (depth0 != null ? lookupProperty(depth0,"altdate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"altdate","hash":{},"data":data,"loc":{"start":{"line":16,"column":21},"end":{"line":16,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n    <strong>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"typelabel") || (depth0 != null ? lookupProperty(depth0,"typelabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typelabel","hash":{},"data":data,"loc":{"start":{"line":18,"column":12},"end":{"line":18,"column":27}}}) : helper)))
    + "</strong>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"invoice_number_and_supplier") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.program(8, data, 0),"data":data,"loc":{"start":{"line":19,"column":4},"end":{"line":31,"column":11}}})) != null ? stack1 : "")
    + "\n    <!-- not actually used but next line prevents crash: -->\n    <span class=\"business-link\"></span>\n</td>\n<td class=\"col_text\">\n    <ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"files") : depth0),{"name":"each","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":38,"column":8},"end":{"line":48,"column":17}}})) != null ? stack1 : "")
    + "    </ul>\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"if","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":51,"column":0},"end":{"line":55,"column":7}}})) != null ? stack1 : "")
    + "<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":56,"column":23},"end":{"line":56,"column":39}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":57,"column":23},"end":{"line":57,"column":40}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total") || (depth0 != null ? lookupProperty(depth0,"total") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data,"loc":{"start":{"line":58,"column":23},"end":{"line":58,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":59,"column":0},"end":{"line":76,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/ExpenseView.mustache":
/*!**********************************************************!*\
  !*** ./src/expense/views/templates/ExpenseView.mustache ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"justified") : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data,"loc":{"start":{"line":2,"column":4},"end":{"line":14,"column":11}}})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    return "        <td class=\"col_status\"\n            title=\"Justificatif(s) accepté(s)\"\n            aria-label=\"Justificatif(s) accepté(s)\"\n        >\n    <span class=\"icon status valid\">\n        <svg><use href=\"../static/icons/endi.svg#check\"></use></svg>\n    </span>\n        </td>\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "        <td class=\"col_status\" title=\"Justificatif(s) en attente\" aria-label=\"Justificatif(s) en attente\">\n		</td>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "		<br />\n		"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":21,"column":2},"end":{"line":21,"column":19}}}) : helper)))
    + "\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "    (Facture "
    + alias4(((helper = (helper = lookupProperty(helpers,"invoice_number") || (depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"invoice_number","hash":{},"data":data,"loc":{"start":{"line":24,"column":13},"end":{"line":24,"column":33}}}) : helper)))
    + " / "
    + alias4(((helper = (helper = lookupProperty(helpers,"supplier_label") || (depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"supplier_label","hash":{},"data":data,"loc":{"start":{"line":24,"column":36},"end":{"line":24,"column":56}}}) : helper)))
    + ")\n";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {}), lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0),{"name":"if","hash":{},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":26,"column":4},"end":{"line":28,"column":11}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0),{"name":"if","hash":{},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":29,"column":4},"end":{"line":31,"column":11}}})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        ("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"invoice_number") || (depth0 != null ? lookupProperty(depth0,"invoice_number") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"invoice_number","hash":{},"data":data,"loc":{"start":{"line":27,"column":9},"end":{"line":27,"column":27}}}) : helper)))
    + ")\n";
},"13":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "        ("
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"supplier_label") || (depth0 != null ? lookupProperty(depth0,"supplier_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"supplier_label","hash":{},"data":data,"loc":{"start":{"line":30,"column":9},"end":{"line":30,"column":29}}}) : helper)))
    + ")\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "            <li>\n                <a href=\"/files/"
    + alias4(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data,"loc":{"start":{"line":38,"column":32},"end":{"line":38,"column":38}}}) : helper)))
    + "?action=download\"\n                   title=\"Ouvrir le justificatif "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":39,"column":49},"end":{"line":39,"column":60}}}) : helper)))
    + " dans une nouvelle fenêtre\"\n                   aria-label=\"Ouvrir le justificatif "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":40,"column":54},"end":{"line":40,"column":65}}}) : helper)))
    + " dans une nouvelle fenêtre\"\n                   target=\"_blank\"\n                >\n                    "
    + alias4(((helper = (helper = lookupProperty(helpers,"label") || (depth0 != null ? lookupProperty(depth0,"label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"label","hash":{},"data":data,"loc":{"start":{"line":43,"column":20},"end":{"line":43,"column":31}}}) : helper)))
    + "\n                </a>\n            </li>\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\">\n        <div class=\"justified\"></div>\n    </td>\n";
},"19":function(container,depth0,helpers,partials,data) {
    return "    <td class=\"col_text\"><span class=\"business-link\"></span></td>\n";
},"21":function(container,depth0,helpers,partials,data) {
    return "        <span class=\"icon small\"\n              title=\"Dépense en mode TVA sur marge : il s'agit d'une TVA estimée, le TTC fait foi.\"\n        >\n            <svg><use href=\"/static/icons/endi.svg#exclamation-circle\"></use></svg>\n        </span>\n";
},"23":function(container,depth0,helpers,partials,data) {
    return "<td class='col_actions width_four'>\n	<ul>\n		<li>\n			<button class='btn icon only edit' title='Modifier' aria-label='Modifier'><svg><use href=\"/static/icons/endi.svg#pen\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only duplicate' title='Dupliquer' aria-label='Dupliquer'><svg><use href=\"/static/icons/endi.svg#copy\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only bookmark' title='Mettre en favori' aria-label='Mettre en favori'><svg><use href=\"/static/icons/endi.svg#star-empty\"></use></svg></button>\n		</li>\n		<li>\n			<button class='btn icon only negative delete' title='Supprimer' aria-label='Supprimer'><svg><use href=\"/static/icons/endi.svg#trash-alt\"></use></svg></button>\n		</li>\n	</ul>	\n</td>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, options, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    }, buffer = 
  ((stack1 = lookupProperty(helpers,"unless").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"unless","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":1,"column":0},"end":{"line":15,"column":11}}})) != null ? stack1 : "")
    + "<td class=\"col_date\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"altdate") || (depth0 != null ? lookupProperty(depth0,"altdate") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"altdate","hash":{},"data":data,"loc":{"start":{"line":16,"column":21},"end":{"line":16,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n	<strong>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"typelabel") || (depth0 != null ? lookupProperty(depth0,"typelabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"typelabel","hash":{},"data":data,"loc":{"start":{"line":18,"column":9},"end":{"line":18,"column":24}}}) : helper)))
    + "</strong>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":19,"column":1},"end":{"line":22,"column":8}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"invoice_number_and_supplier") : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data,"loc":{"start":{"line":23,"column":4},"end":{"line":32,"column":7}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n    <ul>\n"
    + ((stack1 = lookupProperty(helpers,"each").call(alias1,(depth0 != null ? lookupProperty(depth0,"files") : depth0),{"name":"each","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":36,"column":8},"end":{"line":46,"column":17}}})) != null ? stack1 : "")
    + "    </ul>\n</td>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"can_validate_attachments") : depth0),{"name":"if","hash":{},"fn":container.program(17, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":49,"column":0},"end":{"line":53,"column":7}}})) != null ? stack1 : "")
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(19, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":54,"column":0},"end":{"line":56,"column":7}}})) != null ? stack1 : "")
    + "<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":57,"column":23},"end":{"line":57,"column":39}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_tva_on_margin") : depth0),{"name":"if","hash":{},"fn":container.program(21, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":59,"column":4},"end":{"line":65,"column":11}}})) != null ? stack1 : "")
    + "    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":66,"column":4},"end":{"line":66,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"total") || (depth0 != null ? lookupProperty(depth0,"total") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"total","hash":{},"data":data,"loc":{"start":{"line":68,"column":23},"end":{"line":68,"column":36}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
  stack1 = ((helper = (helper = lookupProperty(helpers,"edit") || (depth0 != null ? lookupProperty(depth0,"edit") : depth0)) != null ? helper : alias2),(options={"name":"edit","hash":{},"fn":container.program(23, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":69,"column":0},"end":{"line":86,"column":9}}}),(typeof helper === alias3 ? helper.call(alias1,options) : helper));
  if (!lookupProperty(helpers,"edit")) { stack1 = container.hooks.blockHelperMissing.call(depth0,stack1,options)}
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/FileLinkPopupView.mustache":
/*!****************************************************************!*\
  !*** ./src/expense/views/templates/FileLinkPopupView.mustache ***!
  \****************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div role=\"dialog\" id=\"file-link-forms\" aria-modal=\"true\" aria-labelledby=\"file-link-forms_title\">\n	<div class=\"modal_layout\">\n		<header>\n			<button tabindex='-1' type=\"button\" class=\"icon only unstyled close\" title=\"Fermer cette fenêtre\" aria-label=\"Fermer cette fenêtre\">\n				<svg><use href=\"/static/icons/endi.svg#times\"></use></svg>\n			</button>\n			<h2 id=\"file-link-forms_title\">\n                Lier un justificatif à une ou plusieurs dépenses\n            </h2>\n		</header>\n        <form class=\"modal_content_layout\">\n			<div class=\"modal_content\">\n				<p class=\"content_vertical_padding separate_bottom\">\n					Sélectionnez une ou plusieurs dépenses correspondant au justificatif "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"file_label") || (depth0 != null ? lookupProperty(depth0,"file_label") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"file_label","hash":{},"data":data,"loc":{"start":{"line":14,"column":74},"end":{"line":14,"column":90}}}) : helper))) != null ? stack1 : "")
    + ".\n				</p>\n				<div class=\"table_container\">\n					<table class=\"opa hover_table top_align_table\">\n						<thead>\n							<tr>\n								<th scope=\"col\" class=\"col_select\" title=\"Sélection\">\n                                    <span class=\"screen-reader-text\">Sélection</span>\n                                </th>\n								<th scope=\"col\" class=\"col_date\">\n                                    Date\n                                </th>\n								<th scope=\"col\" class=\"col_text\" title=\"Type de dépense et description\">\n                                    Type<span class=\"screen-reader-text\"> de dépense</span> et description\n                                </th>\n                                <th scope=\"col\" class=\"col_text\" title=\"Frais généraux / achat client\">\n                                    Catégorie\n                                </th>\n								<th scope=\"col\" class=\"col_number\" title=\"Montant Hors Taxes\">\n                                    <span class=\"screen-reader-text\">Montant </span>H<span class=\"screen-reader-text\">hors </span>T<span class=\"screen-reader-text\">axes</span>\n                                </th>\n								<th scope=\"col\" class=\"col_number\">\n                                    TVA\n                                </th>\n								<th scope=\"col\" class=\"col_number\" title=\"Montant Toutes Taxes Comprises\">\n                                    <span class=\"screen-reader-text\">Montant </span>T<span class=\"screen-reader-text\">outes </span>T<span class=\"screen-reader-text\">axes </span>C<span class=\"screen-reader-text\">omprises</span>\n                                </th>\n							</tr>\n						</thead>\n						<tbody></tbody>\n					</table>\n				</div>\n			</div>\n			<footer>\n				<button\n					class='btn btn-primary'\n					type='submit'\n					value='submit'>\n					Fermer\n				</button>\n			</footer>\n        </form>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/MainView.mustache":
/*!*******************************************************!*\
  !*** ./src/expense/views/templates/MainView.mustache ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", alias4=container.escapeExpression, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class='separate_top'>\n	<div class='messages-container'></div>\n	<div class='totals grand-total'></div>\n	<div class='form-section'>\n		<div class='content'>\n			<ul class=\"nav nav-tabs\" role=\"tablist\">\n				<li role=\"presentation\" class=\"active\">\n					<a href=\"#internal-container\" aria-controls=\"internal-container\" id=\"internal-tabtitle\" role=\"tab\" data-toggle=\"tab\">\n						<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#building\"></use></svg></span>\n						<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"internalTabLabel") || (depth0 != null ? lookupProperty(depth0,"internalTabLabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"internalTabLabel","hash":{},"data":data,"loc":{"start":{"line":10,"column":12},"end":{"line":10,"column":34}}}) : helper)))
    + "</span>\n					</a>\n				</li>\n				<li role=\"presentation\">\n					<a href=\"#activity-container\" aria-controls=\"activity-container\" id=\"activity-tabtitle\" role=\"tab\" data-toggle=\"tab\" title=\"Achats rattachés à un client\" aria-label=\"Achats rattachés à un client\">\n						<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#address-card\"></use></svg></span>\n						<span>"
    + alias4(((helper = (helper = lookupProperty(helpers,"externalTabLabel") || (depth0 != null ? lookupProperty(depth0,"externalTabLabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"externalTabLabel","hash":{},"data":data,"loc":{"start":{"line":16,"column":12},"end":{"line":16,"column":34}}}) : helper)))
    + "</span>\n					</a>\n				</li>\n			</ul>\n			<div class='tab-content content'>\n				<div id=\"internal-container\" class=\"tab-pane fade in active\" role=\"tabpanel\" aria-labelledby=\"internal-tabtitle\">\n					<div class=\"content_vertical_padding\">\n						<h3 class=\"print-only\">\n							"
    + alias4(((helper = (helper = lookupProperty(helpers,"internalTabLabel") || (depth0 != null ? lookupProperty(depth0,"internalTabLabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"internalTabLabel","hash":{},"data":data,"loc":{"start":{"line":24,"column":7},"end":{"line":24,"column":29}}}) : helper)))
    + "\n						</h3>\n						<p> \n							"
    + alias4(((helper = (helper = lookupProperty(helpers,"internalDescription") || (depth0 != null ? lookupProperty(depth0,"internalDescription") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"internalDescription","hash":{},"data":data,"loc":{"start":{"line":27,"column":7},"end":{"line":27,"column":32}}}) : helper)))
    + "\n						</p>\n					</div>\n					<div class='internal-total'></div>\n					<div class='internal-lines content_vertical_padding'></div>\n					<div class='internal-kmlines content_vertical_padding'>\n						<div class='alert alert-warning'>\n							<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#exclamation-circle\"></use></svg></span>\n							Il n’est plus possible de configurer des frais kilométriques sur cette année\n						</div>\n					</div>\n				</div>\n				<div id=\"activity-container\" class=\"tab-pane fade\" role=\"tabpanel\" aria-labelledby=\"activity-tabtitle\">\n					<div class=\"content_vertical_padding\">\n						<h3 class=\"print-only\">\n							"
    + alias4(((helper = (helper = lookupProperty(helpers,"externalTabLabel") || (depth0 != null ? lookupProperty(depth0,"externalTabLabel") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"externalTabLabel","hash":{},"data":data,"loc":{"start":{"line":42,"column":7},"end":{"line":42,"column":29}}}) : helper)))
    + "\n						</h3>\n						<p>\n							"
    + alias4(((helper = (helper = lookupProperty(helpers,"externalDescription") || (depth0 != null ? lookupProperty(depth0,"externalDescription") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"externalDescription","hash":{},"data":data,"loc":{"start":{"line":45,"column":7},"end":{"line":45,"column":32}}}) : helper)))
    + "\n						</p>\n					</div>\n					<div class='activity-total'></div>\n					<div class='activity-lines content_vertical_padding'></div>\n					<div class='activity-kmlines content_vertical_padding'>\n						<div class='alert alert-warning'>\n							<span class=\"icon\"><svg><use href=\"/static/icons/endi.svg#exclamation-circle\"></use></svg></span>\n							Il n’est plus possible de configurer des frais kilométriques sur cette année\n						</div>\n					</div>\n				</div>\n			</div>\n		</div>\n	</div>\n</div>\n<section id=\"expense_form\" class=\"modal_view size_large modalRegion\"></section>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/SelectableExpenseItemView.mustache":
/*!************************************************************************!*\
  !*** ./src/expense/views/templates/SelectableExpenseItemView.mustache ***!
  \************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"1":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "		<br />\n		"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":9,"column":2},"end":{"line":9,"column":19}}}) : helper)))
    + "\n";
},"3":function(container,depth0,helpers,partials,data) {
    return "        Achats client <span class=\"business-link\"></span>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "        Frais généraux\n";
},"7":function(container,depth0,helpers,partials,data) {
    return "        <span class=\"icon small\"\n              title=\"Dépense en mode TVA sur marge : il s'agit d'une TVA estimée, le TTC fait foi.\"\n        >\n            <svg><use href=\"/static/icons/endi.svg#exclamation-circle\"></use></svg>\n        </span>\n";
},"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<td class='col_select action'></td>\n<td class=\"col_date\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"date") || (depth0 != null ? lookupProperty(depth0,"date") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"date","hash":{},"data":data,"loc":{"start":{"line":2,"column":21},"end":{"line":2,"column":33}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\" title=\"Sélectionner cette dépense\">\n\n    <!-- <label><input type=\"checkbox\"><span class=\"screen-reader-text\">Sélectionner cette dépense</span></label> -->\n	<strong>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"type_label") || (depth0 != null ? lookupProperty(depth0,"type_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"type_label","hash":{},"data":data,"loc":{"start":{"line":6,"column":9},"end":{"line":6,"column":25}}}) : helper)))
    + "</strong>\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"description") : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":7,"column":1},"end":{"line":10,"column":8}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_text\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"is_achat") : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data,"loc":{"start":{"line":13,"column":4},"end":{"line":17,"column":11}}})) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht_label") || (depth0 != null ? lookupProperty(depth0,"ht_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht_label","hash":{},"data":data,"loc":{"start":{"line":19,"column":23},"end":{"line":19,"column":39}}}) : helper))) != null ? stack1 : "")
    + "</td>\n<td class=\"col_number\">\n"
    + ((stack1 = lookupProperty(helpers,"if").call(alias1,(depth0 != null ? lookupProperty(depth0,"has_tva_on_margin") : depth0),{"name":"if","hash":{},"fn":container.program(7, data, 0),"inverse":container.noop,"data":data,"loc":{"start":{"line":21,"column":4},"end":{"line":27,"column":11}}})) != null ? stack1 : "")
    + "    "
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva_label") || (depth0 != null ? lookupProperty(depth0,"tva_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva_label","hash":{},"data":data,"loc":{"start":{"line":28,"column":4},"end":{"line":28,"column":21}}}) : helper))) != null ? stack1 : "")
    + "\n</td>\n<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc_label") || (depth0 != null ? lookupProperty(depth0,"ttc_label") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ttc_label","hash":{},"data":data,"loc":{"start":{"line":30,"column":23},"end":{"line":30,"column":40}}}) : helper))) != null ? stack1 : "")
    + "</td>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/TabTotalView.mustache":
/*!***********************************************************!*\
  !*** ./src/expense/views/templates/TabTotalView.mustache ***!
  \***********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class='totals form-section'>\n	<div class=\"layout flex two_cols\">\n		<div>\n			<p>Total TTC&nbsp;: </p>\n		</div>\n		<div>\n			<p><strong>"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":7,"column":14},"end":{"line":7,"column":25}}}) : helper))) != null ? stack1 : "")
    + "</strong></p>\n		</div>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/expense/views/templates/TotalView.mustache":
/*!********************************************************!*\
  !*** ./src/expense/views/templates/TotalView.mustache ***!
  \********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=container.hooks.helperMissing, alias3="function", lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div class=\"layout flex\">\n	<div></div>\n	<div>\n    	<h4 class=\"content_vertical_padding\">Totaux</h4>\n    	<table class=\"top_align_table\">\n    		<tbody>\n    			<tr>\n    				<th scope=\"row\">Total HT</th>\n    				<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ht") || (depth0 != null ? lookupProperty(depth0,"ht") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ht","hash":{},"data":data,"loc":{"start":{"line":9,"column":31},"end":{"line":9,"column":41}}}) : helper))) != null ? stack1 : "")
    + "</td>\n    			</tr>\n    			<tr>\n    				<th scope=\"row\">Total TVA</th>\n    				<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"tva") || (depth0 != null ? lookupProperty(depth0,"tva") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"tva","hash":{},"data":data,"loc":{"start":{"line":13,"column":31},"end":{"line":13,"column":42}}}) : helper))) != null ? stack1 : "")
    + "</td>\n    			</tr>\n    			<tr>\n    				<th scope=\"row\">Total TTC</th>\n    				<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"ttc") || (depth0 != null ? lookupProperty(depth0,"ttc") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"ttc","hash":{},"data":data,"loc":{"start":{"line":17,"column":31},"end":{"line":17,"column":42}}}) : helper))) != null ? stack1 : "")
    + "</td>\n    			</tr>\n    			<tr>\n    				<th scope=\"row\">Total Km</th>\n    				<td class=\"col_number\">"
    + ((stack1 = ((helper = (helper = lookupProperty(helpers,"km") || (depth0 != null ? lookupProperty(depth0,"km") : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"km","hash":{},"data":data,"loc":{"start":{"line":21,"column":31},"end":{"line":21,"column":41}}}) : helper))) != null ? stack1 : "")
    + "&nbsp;km</td>\n    			</tr>\n    		</tbody>\n    	</table>\n	</div>\n</div>\n";
},"useData":true});

/***/ }),

/***/ "./src/widgets/templates/DropZoneWidget.mustache":
/*!*******************************************************!*\
  !*** ./src/widgets/templates/DropZoneWidget.mustache ***!
  \*******************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var Handlebars = __webpack_require__(/*! ../../../node_modules/handlebars/runtime.js */ "./node_modules/handlebars/runtime.js");
function __default(obj) { return obj && (obj.__esModule ? obj["default"] : obj); }
module.exports = (Handlebars["default"] || Handlebars).template({"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"drop_files_here\">\n    <form>\n        <label style=\"cursor: pointer;font-size: unset; display: block\">\n            <input type=\"file\"  style=\"display: none\" multiple />\n	        <span class=\"icon\">\n                <svg><use href=\"../static/icons/endi.svg#upload\"></use></svg>\n            </span>\n	        <p>\n                Cliquez ou déposez vos justificatifs ici pour les téléverser\n            </p>\n        </label>\n    </form>\n</div>\n";
},"useData":true});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			id: moduleId,
/******/ 			loaded: false,
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/global */
/******/ 	(() => {
/******/ 		__webpack_require__.g = (function() {
/******/ 			if (typeof globalThis === 'object') return globalThis;
/******/ 			try {
/******/ 				return this || new Function('return this')();
/******/ 			} catch (e) {
/******/ 				if (typeof window === 'object') return window;
/******/ 			}
/******/ 		})();
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/node module decorator */
/******/ 	(() => {
/******/ 		__webpack_require__.nmd = (module) => {
/******/ 			module.paths = [];
/******/ 			if (!module.children) module.children = [];
/******/ 			return module;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/publicPath */
/******/ 	(() => {
/******/ 		var scriptUrl;
/******/ 		if (__webpack_require__.g.importScripts) scriptUrl = __webpack_require__.g.location + "";
/******/ 		var document = __webpack_require__.g.document;
/******/ 		if (!scriptUrl && document) {
/******/ 			if (document.currentScript)
/******/ 				scriptUrl = document.currentScript.src
/******/ 			if (!scriptUrl) {
/******/ 				var scripts = document.getElementsByTagName("script");
/******/ 				if(scripts.length) scriptUrl = scripts[scripts.length - 1].src
/******/ 			}
/******/ 		}
/******/ 		// When supporting browsers where an automatic publicPath is not supported you must specify an output.publicPath manually via configuration
/******/ 		// or pass an empty string ("") and set the __webpack_public_path__ variable from your code to use your own logic.
/******/ 		if (!scriptUrl) throw new Error("Automatic publicPath is not supported in this browser");
/******/ 		scriptUrl = scriptUrl.replace(/#.*$/, "").replace(/\?.*$/, "").replace(/\/[^\/]+$/, "/");
/******/ 		__webpack_require__.p = scriptUrl;
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"expense": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkenDI"] = self["webpackChunkenDI"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["vendor"], () => (__webpack_require__("./src/expense/expense.js")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
//# sourceMappingURL=expense.js.map